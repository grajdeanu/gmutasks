﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMUTasks.Classes;

namespace GMUTasks
{
    public partial class MainLogin : Form
    {
        private CUser[] testUsers;
        private MainWindow mainWindow;

        public MainLogin()
        {
            LoadUsers();
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LastNameTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateLoginButton();
        }

        private void TeacherIdTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateLoginButton();
        }

        private void UpdateLoginButton()
        {
            LoginButton.Enabled = (LastNameTextBox.Text.Length > 0 && TeacherIdTextBox.Text.Length > 0);
        }
        private bool IsLoginValid()
        {
            return false;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            // Disable fields
            LastNameTextBox.Enabled = false;
            TeacherIdTextBox.Enabled = false;

            // Validate Login
            CUser teacher    = new CUser();
            teacher.LastName = LastNameTextBox.Text;
            teacher.UUID     = TeacherIdTextBox.Text;
            //GetUserByNameAndId(LastNameTextBox.Text, TeacherIdTextBox.Text);
            
            //if (teacher == null || teacher.AccessLevel < AccessLevel.Teacher)
            //{
            //    // Invalid Login
            //    MessageBox.Show("The Login details you provided were invalid. Please enter valid login details to continue.", "Invalid Login");
            //    LastNameTextBox.Enabled = true;
            //    TeacherIdTextBox.Enabled = true;
            //}
            //else
            //{
                mainWindow = new MainWindow(this);
                mainWindow.Teacher = teacher;
                mainWindow.Show();
                this.Hide();
            //}
            
        }

        public void LogoutUser()
        {
            LastNameTextBox.Text = "";
            LastNameTextBox.Enabled = true;
            TeacherIdTextBox.Text = "";
            TeacherIdTextBox.Enabled = true;
            this.Show();
        }

        public ArrayList GetStudentsForTeacherID(String sId)
        {
            ArrayList aStudentIds = new ArrayList();
            
            for (int i = 0; i < testUsers.Count(); i++)
            {
                if (testUsers[i].UUID.Substring(0, 5).Equals(sId) && testUsers[i].AccessLevel == AccessLevel.Student)
                {
                    aStudentIds.Add(testUsers[i].UUID);
                }
            }

            return aStudentIds;

        }

        // Load Test Users
        private void LoadUsers()
        {
            Console.WriteLine("MainLogin::LoadUsers()");

            testUsers = new CUser[12];

            // Test Users for Development
            testUsers[0] = new CUser("John", "Doe", "001010001", Gender.Male, Ethnicity.White, AccessLevel.Student);
            testUsers[1] = new CUser("John", "Smith", "001010002", Gender.Male, Ethnicity.White, AccessLevel.Student);
            testUsers[2] = new CUser("Kim", "Johnson", "001010003", Gender.Female, Ethnicity.Black, AccessLevel.Student);
            testUsers[3] = new CUser("Cinder", "Wang", "001010004", Gender.Female, Ethnicity.Asian, AccessLevel.Student);
            testUsers[4] = new CUser("Juan", "Lopez", "001020001", Gender.Male, Ethnicity.Hispanic, AccessLevel.Student);
            testUsers[5] = new CUser("Celia", "Dolemite", "001020002", Gender.Female, Ethnicity.Hispanic, AccessLevel.Student);
            testUsers[6] = new CUser("Joshua", "Barnes", "002010001", Gender.Male, Ethnicity.Black, AccessLevel.Student);
            testUsers[7] = new CUser("Rebecca", "Kline", "003020001", Gender.Female, Ethnicity.White, AccessLevel.Student);

            // Test Teachers
            testUsers[8] = new CUser("Wanda", "Jones", "00101", Gender.Female, Ethnicity.Black, AccessLevel.Teacher);
            testUsers[9] = new CUser("Jacob", "McDonald", "00102", Gender.Male, Ethnicity.White, AccessLevel.Teacher);
            testUsers[10] = new CUser("Juanita", "Juarez", "00201", Gender.Female, Ethnicity.Hispanic, AccessLevel.Teacher);
            testUsers[11] = new CUser("George", "Jackson", "00302", Gender.Male, Ethnicity.White, AccessLevel.Teacher);
        }

        public CUser GetUserById(String sId)
        {
            //for (int i=0; i < testUsers.Count(); i++)
            //{
            //    if (testUsers[i].UUID.Equals(sId))
            //    {
            //        return testUsers[i];
            //    }
            //}
            //return null;
            CUser user = new CUser();
            user.UUID = sId;
            return user;
        }

        private CUser GetUserByNameAndId(String sLastName, String sId)
        {
            for (int i=0; i < testUsers.Count(); i++)
            {
                if (testUsers[i].LastName.Equals(sLastName) && testUsers[i].UUID.Equals(sId))
                {
                    return testUsers[i];
                }
            }
            return null;
        }

        private void LastNameLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
