﻿namespace GMUTasks.Forms.AKT
{
    partial class AKTMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AKTMainWindow));
            this.ButtonClose = new System.Windows.Forms.Button();
            this.cellPictureBoxD = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxC = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxB = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxA = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxFrame = new System.Windows.Forms.PictureBox();
            this.axWMPVideo = new AxWMPLib.AxWindowsMediaPlayer();
            this.introBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWMPVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.introBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonClose
            // 
            this.ButtonClose.BackColor = System.Drawing.Color.Transparent;
            this.ButtonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonClose.FlatAppearance.BorderSize = 0;
            this.ButtonClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ButtonClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonClose.Image = global::GMUTasks.Properties.Resources.close_32x32;
            this.ButtonClose.Location = new System.Drawing.Point(1236, 12);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(32, 32);
            this.ButtonClose.TabIndex = 2;
            this.ButtonClose.UseVisualStyleBackColor = false;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // cellPictureBoxD
            // 
            this.cellPictureBoxD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cellPictureBoxD.Location = new System.Drawing.Point(687, 442);
            this.cellPictureBoxD.Name = "cellPictureBoxD";
            this.cellPictureBoxD.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxD.TabIndex = 10;
            this.cellPictureBoxD.TabStop = false;
            this.cellPictureBoxD.Tag = "5";
            this.cellPictureBoxD.Visible = false;
            this.cellPictureBoxD.Click += new System.EventHandler(this.cellPictureBoxD_Click);
            // 
            // cellPictureBoxC
            // 
            this.cellPictureBoxC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cellPictureBoxC.Location = new System.Drawing.Point(66, 442);
            this.cellPictureBoxC.Name = "cellPictureBoxC";
            this.cellPictureBoxC.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxC.TabIndex = 9;
            this.cellPictureBoxC.TabStop = false;
            this.cellPictureBoxC.Tag = "4";
            this.cellPictureBoxC.Visible = false;
            this.cellPictureBoxC.Click += new System.EventHandler(this.cellPictureBoxC_Click);
            // 
            // cellPictureBoxB
            // 
            this.cellPictureBoxB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cellPictureBoxB.Location = new System.Drawing.Point(687, 83);
            this.cellPictureBoxB.Name = "cellPictureBoxB";
            this.cellPictureBoxB.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxB.TabIndex = 8;
            this.cellPictureBoxB.TabStop = false;
            this.cellPictureBoxB.Tag = "3";
            this.cellPictureBoxB.Visible = false;
            this.cellPictureBoxB.Click += new System.EventHandler(this.cellPictureBoxB_Click);
            // 
            // cellPictureBoxA
            // 
            this.cellPictureBoxA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cellPictureBoxA.Location = new System.Drawing.Point(66, 83);
            this.cellPictureBoxA.Name = "cellPictureBoxA";
            this.cellPictureBoxA.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxA.TabIndex = 7;
            this.cellPictureBoxA.TabStop = false;
            this.cellPictureBoxA.Tag = "2";
            this.cellPictureBoxA.Visible = false;
            this.cellPictureBoxA.Click += new System.EventHandler(this.cellPictureBoxA_Click);
            // 
            // cellPictureBoxFrame
            // 
            this.cellPictureBoxFrame.Location = new System.Drawing.Point(588, 27);
            this.cellPictureBoxFrame.Name = "cellPictureBoxFrame";
            this.cellPictureBoxFrame.Size = new System.Drawing.Size(100, 50);
            this.cellPictureBoxFrame.TabIndex = 11;
            this.cellPictureBoxFrame.TabStop = false;
            this.cellPictureBoxFrame.Visible = false;
            // 
            // axWMPVideo
            // 
            this.axWMPVideo.Enabled = true;
            this.axWMPVideo.Location = new System.Drawing.Point(66, 83);
            this.axWMPVideo.Name = "axWMPVideo";
            this.axWMPVideo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWMPVideo.OcxState")));
            this.axWMPVideo.Size = new System.Drawing.Size(1149, 635);
            this.axWMPVideo.TabIndex = 3;
            // 
            // introBox
            // 
            this.introBox.Location = new System.Drawing.Point(349, 229);
            this.introBox.Name = "introBox";
            this.introBox.Size = new System.Drawing.Size(575, 335);
            this.introBox.TabIndex = 12;
            this.introBox.TabStop = false;
            this.introBox.Visible = false;
            this.introBox.Click += new System.EventHandler(this.introBox_Click);
            // 
            // AKTMainWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.ControlBox = false;
            this.Controls.Add(this.introBox);
            this.Controls.Add(this.cellPictureBoxFrame);
            this.Controls.Add(this.cellPictureBoxD);
            this.Controls.Add(this.cellPictureBoxC);
            this.Controls.Add(this.cellPictureBoxB);
            this.Controls.Add(this.cellPictureBoxA);
            this.Controls.Add(this.axWMPVideo);
            this.Controls.Add(this.ButtonClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AKTMainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AKTMainWindow";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWMPVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.introBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.PictureBox cellPictureBoxD;
        private System.Windows.Forms.PictureBox cellPictureBoxC;
        private System.Windows.Forms.PictureBox cellPictureBoxB;
        private System.Windows.Forms.PictureBox cellPictureBoxA;
        private AxWMPLib.AxWindowsMediaPlayer axWMPVideo;
        private System.Windows.Forms.PictureBox cellPictureBoxFrame;
        private System.Windows.Forms.PictureBox introBox;

    }
}