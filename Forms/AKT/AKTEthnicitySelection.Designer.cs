﻿namespace GMUTasks.Forms.AKT
{
    partial class AKTEthnicitySelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCaucasian = new System.Windows.Forms.Button();
            this.buttonAsian = new System.Windows.Forms.Button();
            this.buttonAfricanAmerican = new System.Windows.Forms.Button();
            this.buttonHispanic = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(338, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please choose the proper ethnicity for the child";
            // 
            // buttonCaucasian
            // 
            this.buttonCaucasian.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCaucasian.Location = new System.Drawing.Point(17, 51);
            this.buttonCaucasian.Name = "buttonCaucasian";
            this.buttonCaucasian.Size = new System.Drawing.Size(170, 45);
            this.buttonCaucasian.TabIndex = 1;
            this.buttonCaucasian.Text = "Caucasian";
            this.buttonCaucasian.UseVisualStyleBackColor = true;
            this.buttonCaucasian.Click += new System.EventHandler(this.buttonCaucasian_Click);
            // 
            // buttonAsian
            // 
            this.buttonAsian.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAsian.Location = new System.Drawing.Point(208, 51);
            this.buttonAsian.Name = "buttonAsian";
            this.buttonAsian.Size = new System.Drawing.Size(170, 45);
            this.buttonAsian.TabIndex = 2;
            this.buttonAsian.Text = "Asian";
            this.buttonAsian.UseVisualStyleBackColor = true;
            this.buttonAsian.Click += new System.EventHandler(this.buttonAsian_Click);
            // 
            // buttonAfricanAmerican
            // 
            this.buttonAfricanAmerican.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAfricanAmerican.Location = new System.Drawing.Point(208, 111);
            this.buttonAfricanAmerican.Name = "buttonAfricanAmerican";
            this.buttonAfricanAmerican.Size = new System.Drawing.Size(170, 45);
            this.buttonAfricanAmerican.TabIndex = 4;
            this.buttonAfricanAmerican.Text = "African American";
            this.buttonAfricanAmerican.UseVisualStyleBackColor = true;
            this.buttonAfricanAmerican.Click += new System.EventHandler(this.buttonAfricanAmerican_Click);
            // 
            // buttonHispanic
            // 
            this.buttonHispanic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonHispanic.Location = new System.Drawing.Point(17, 111);
            this.buttonHispanic.Name = "buttonHispanic";
            this.buttonHispanic.Size = new System.Drawing.Size(170, 45);
            this.buttonHispanic.TabIndex = 3;
            this.buttonHispanic.Text = "Hispanic";
            this.buttonHispanic.UseVisualStyleBackColor = true;
            this.buttonHispanic.Click += new System.EventHandler(this.buttonHispanic_Click);
            // 
            // AKTEthnicitySelection
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(392, 168);
            this.Controls.Add(this.buttonAfricanAmerican);
            this.Controls.Add(this.buttonHispanic);
            this.Controls.Add(this.buttonAsian);
            this.Controls.Add(this.buttonCaucasian);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AKTEthnicitySelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose an Ethnicity";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCaucasian;
        private System.Windows.Forms.Button buttonAsian;
        private System.Windows.Forms.Button buttonAfricanAmerican;
        private System.Windows.Forms.Button buttonHispanic;
    }
}