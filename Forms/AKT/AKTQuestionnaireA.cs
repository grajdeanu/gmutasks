﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Forms.AKT
{
    public partial class AKTQuestionnaireA : Form
    {

        public Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices> m_dStereotypes =
           new Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices>();

        public AKTQuestionnaireA()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();

            this.SetBounds(0, 0, Constants.DeviceResolutionWidth, Constants.DeviceResolutionHeight);
            labelDescription.Text = "Please select the emotion you think your child would be most likely to display in the following situations:" + InsertNewLines(1) + "(If you have not seen a situation, try to predict what your child would feel.)";
            labelQuestions.Text = "1.   Coming to preschool." + InsertNewLines(3)
                                + InsertNewLines(3)
                                + "2.   Does s/he like pizza?" + InsertNewLines(3)
                                + InsertNewLines(3)
                                + "3.   Seeing a clown walk into the classroom." + InsertNewLines(3)
                                + InsertNewLines(3)
                                + "4.   Some other kids would not let him/her play." + InsertNewLines(3)
                                + InsertNewLines(3)
                                + "5.   A classmate hits him/her, and says that if he/she tells teacher, they will hit him/her again." + InsertNewLines(3)
                                + InsertNewLines(3)
                                + "6.   After doing something against the class rules, teacher says if they do it again, they will have to be disciplined." + InsertNewLines(3)
                                ;

        }

        private string InsertNewLines(int nCount)
        {
            string returnString = "";
            for (int i = 0; i < nCount; i++)
            {
                returnString += Environment.NewLine;
            }
            return returnString;
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            // Verify that the selections were all made
            bool isCompleted = ((radioOneHappy.Checked     || radioOneSad.Checked)     &&
                                //(radioTwoHappy.Checked     || radioTwoSad.Checked)     &&
                                (radioThreeYes.Checked     || radioThreeNo.Checked)    &&
                                //(radioFourHappy.Checked    || radioFourAngry.Checked)  &&
                                (radioFiveHappy.Checked    || radioFiveAfraid.Checked) &&
                                //(radioSixHappy.Checked     || radioSixAfraid.Checked)  &&
                                (radioSevenAngry.Checked   || radioSevenSad.Checked)   &&
                                //(radioEightAngry.Checked   || radioEightSad.Checked)   &&
                                (radioNineAfraid.Checked   || radioNineAngry.Checked)  &&
                                //(radioTenAfraid.Checked    || radioTenAngry.Checked)   &&
                                (radioElevenAfraid.Checked || radioElevenSad.Checked)  //&&
                                //(radioTwelveAfraid.Checked || radioTwelveSad.Checked)
                               );

            if (isCompleted)
            {
                m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.GoSchool, (radioOneHappy.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                //m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.ClassTrip, (radioTwoHappy.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Pizza, (radioThreeYes.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                //m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Swing, (radioFourHappy.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Clown, (radioFiveHappy.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                //m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.JungleGym, (radioSixHappy.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Block, (radioSevenAngry.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                //m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.StayInside, (radioEightAngry.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Hit, (radioNineAngry.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                //m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Disappointed, (radioTenAngry.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.CleanToy, (radioElevenSad.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));
                //m_dStereotypes.Add(Constants.AKTNonStereotypicalQuestions.Hamster, (radioTwelveSad.Checked ? Constants.AKTQuestionnaireChoices.ChoiceA : Constants.AKTQuestionnaireChoices.ChoiceB));

                Cursor.Show();
                this.Close();
            }
            else
            {
                DialogResult dr = MessageBox.Show("Please make a selection for each situation listed in the questionnaire.", "AKT Questionnaire Incomplete");
            }

        }

    }
}
