﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMUTasks.Classes;

namespace GMUTasks.Forms.AKT
{
    public partial class AKTGenderSelection : Form
    {
        public Gender m_eGenderSelected;

        public AKTGenderSelection()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();

            m_eGenderSelected = Gender.Male;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_eGenderSelected = Gender.Male;
            Dismiss();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            m_eGenderSelected = Gender.Female;
            Dismiss();
        }
        
        private void Dismiss()
        {
            Cursor.Show();
            this.Close();
        }

    }
}
