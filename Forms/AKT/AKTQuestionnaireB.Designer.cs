﻿namespace GMUTasks.Forms.AKT
{
    partial class AKTQuestionnaireB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelQuestions = new System.Windows.Forms.Label();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.radioOneHappy = new System.Windows.Forms.RadioButton();
            this.radioOneSad = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioTwoSad = new System.Windows.Forms.RadioButton();
            this.radioTwoHappy = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioThreeNo = new System.Windows.Forms.RadioButton();
            this.radioThreeYes = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioFourAngry = new System.Windows.Forms.RadioButton();
            this.radioFourHappy = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioFiveAfraid = new System.Windows.Forms.RadioButton();
            this.radioFiveHappy = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioSixAfraid = new System.Windows.Forms.RadioButton();
            this.radioSixHappy = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radioSevenSad = new System.Windows.Forms.RadioButton();
            this.radioSevenAngry = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioEightSad = new System.Windows.Forms.RadioButton();
            this.radioEightAngry = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.radioTenAfraid = new System.Windows.Forms.RadioButton();
            this.radioTenAngry = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radioNineAfraid = new System.Windows.Forms.RadioButton();
            this.radioNineAngry = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioTwelveAfraid = new System.Windows.Forms.RadioButton();
            this.radioTwelveSad = new System.Windows.Forms.RadioButton();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.radioElevenAfraid = new System.Windows.Forms.RadioButton();
            this.radioElevenSad = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescription.Location = new System.Drawing.Point(12, 28);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(1606, 22);
            this.labelDescription.TabIndex = 0;
            this.labelDescription.Text = "Please select the emotion you think your child would be most likely to display in" +
                " the following situations: (If you have not seen a situation, try to predict wha" +
                "t your child would feel.)";
            // 
            // labelQuestions
            // 
            this.labelQuestions.AutoSize = true;
            this.labelQuestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuestions.Location = new System.Drawing.Point(46, 114);
            this.labelQuestions.Name = "labelQuestions";
            this.labelQuestions.Size = new System.Drawing.Size(141, 17);
            this.labelQuestions.TabIndex = 1;
            this.labelQuestions.Text = "Coming to preschool.";
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSubmit.Location = new System.Drawing.Point(1053, 736);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(199, 39);
            this.buttonSubmit.TabIndex = 2;
            this.buttonSubmit.Text = "Confirm";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // radioOneHappy
            // 
            this.radioOneHappy.AutoSize = true;
            this.radioOneHappy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioOneHappy.Location = new System.Drawing.Point(13, 16);
            this.radioOneHappy.Name = "radioOneHappy";
            this.radioOneHappy.Size = new System.Drawing.Size(72, 21);
            this.radioOneHappy.TabIndex = 3;
            this.radioOneHappy.TabStop = true;
            this.radioOneHappy.Text = "Happy";
            this.radioOneHappy.UseVisualStyleBackColor = true;
            // 
            // radioOneSad
            // 
            this.radioOneSad.AutoSize = true;
            this.radioOneSad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioOneSad.Location = new System.Drawing.Point(261, 16);
            this.radioOneSad.Name = "radioOneSad";
            this.radioOneSad.Size = new System.Drawing.Size(54, 21);
            this.radioOneSad.TabIndex = 4;
            this.radioOneSad.TabStop = true;
            this.radioOneSad.Text = "Sad";
            this.radioOneSad.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioOneSad);
            this.groupBox1.Controls.Add(this.radioOneHappy);
            this.groupBox1.Location = new System.Drawing.Point(743, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 47);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioTwoSad);
            this.groupBox2.Controls.Add(this.radioTwoHappy);
            this.groupBox2.Location = new System.Drawing.Point(743, 146);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(509, 47);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // radioTwoSad
            // 
            this.radioTwoSad.AutoSize = true;
            this.radioTwoSad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTwoSad.Location = new System.Drawing.Point(261, 16);
            this.radioTwoSad.Name = "radioTwoSad";
            this.radioTwoSad.Size = new System.Drawing.Size(54, 21);
            this.radioTwoSad.TabIndex = 4;
            this.radioTwoSad.TabStop = true;
            this.radioTwoSad.Text = "Sad";
            this.radioTwoSad.UseVisualStyleBackColor = true;
            // 
            // radioTwoHappy
            // 
            this.radioTwoHappy.AutoSize = true;
            this.radioTwoHappy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTwoHappy.Location = new System.Drawing.Point(13, 16);
            this.radioTwoHappy.Name = "radioTwoHappy";
            this.radioTwoHappy.Size = new System.Drawing.Size(72, 21);
            this.radioTwoHappy.TabIndex = 3;
            this.radioTwoHappy.TabStop = true;
            this.radioTwoHappy.Text = "Happy";
            this.radioTwoHappy.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioThreeNo);
            this.groupBox3.Controls.Add(this.radioThreeYes);
            this.groupBox3.Location = new System.Drawing.Point(743, 197);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(509, 47);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Visible = false;
            // 
            // radioThreeNo
            // 
            this.radioThreeNo.AutoSize = true;
            this.radioThreeNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioThreeNo.Location = new System.Drawing.Point(261, 16);
            this.radioThreeNo.Name = "radioThreeNo";
            this.radioThreeNo.Size = new System.Drawing.Size(46, 21);
            this.radioThreeNo.TabIndex = 4;
            this.radioThreeNo.TabStop = true;
            this.radioThreeNo.Text = "No";
            this.radioThreeNo.UseVisualStyleBackColor = true;
            // 
            // radioThreeYes
            // 
            this.radioThreeYes.AutoSize = true;
            this.radioThreeYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioThreeYes.Location = new System.Drawing.Point(13, 16);
            this.radioThreeYes.Name = "radioThreeYes";
            this.radioThreeYes.Size = new System.Drawing.Size(53, 21);
            this.radioThreeYes.TabIndex = 3;
            this.radioThreeYes.TabStop = true;
            this.radioThreeYes.Text = "Yes";
            this.radioThreeYes.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioFourAngry);
            this.groupBox4.Controls.Add(this.radioFourHappy);
            this.groupBox4.Location = new System.Drawing.Point(743, 248);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(509, 47);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            // 
            // radioFourAngry
            // 
            this.radioFourAngry.AutoSize = true;
            this.radioFourAngry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioFourAngry.Location = new System.Drawing.Point(261, 16);
            this.radioFourAngry.Name = "radioFourAngry";
            this.radioFourAngry.Size = new System.Drawing.Size(218, 21);
            this.radioFourAngry.TabIndex = 4;
            this.radioFourAngry.TabStop = true;
            this.radioFourAngry.Text = "Angry to leave the old one";
            this.radioFourAngry.UseVisualStyleBackColor = true;
            // 
            // radioFourHappy
            // 
            this.radioFourHappy.AutoSize = true;
            this.radioFourHappy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioFourHappy.Location = new System.Drawing.Point(13, 16);
            this.radioFourHappy.Name = "radioFourHappy";
            this.radioFourHappy.Size = new System.Drawing.Size(235, 21);
            this.radioFourHappy.TabIndex = 3;
            this.radioFourHappy.TabStop = true;
            this.radioFourHappy.Text = "Happy about the new activity";
            this.radioFourHappy.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioFiveAfraid);
            this.groupBox5.Controls.Add(this.radioFiveHappy);
            this.groupBox5.Location = new System.Drawing.Point(743, 299);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(509, 47);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Visible = false;
            // 
            // radioFiveAfraid
            // 
            this.radioFiveAfraid.AutoSize = true;
            this.radioFiveAfraid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioFiveAfraid.Location = new System.Drawing.Point(261, 16);
            this.radioFiveAfraid.Name = "radioFiveAfraid";
            this.radioFiveAfraid.Size = new System.Drawing.Size(69, 21);
            this.radioFiveAfraid.TabIndex = 4;
            this.radioFiveAfraid.TabStop = true;
            this.radioFiveAfraid.Text = "Afraid";
            this.radioFiveAfraid.UseVisualStyleBackColor = true;
            // 
            // radioFiveHappy
            // 
            this.radioFiveHappy.AutoSize = true;
            this.radioFiveHappy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioFiveHappy.Location = new System.Drawing.Point(13, 16);
            this.radioFiveHappy.Name = "radioFiveHappy";
            this.radioFiveHappy.Size = new System.Drawing.Size(72, 21);
            this.radioFiveHappy.TabIndex = 3;
            this.radioFiveHappy.TabStop = true;
            this.radioFiveHappy.Text = "Happy";
            this.radioFiveHappy.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioSixAfraid);
            this.groupBox6.Controls.Add(this.radioSixHappy);
            this.groupBox6.Location = new System.Drawing.Point(743, 350);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(509, 47);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            // 
            // radioSixAfraid
            // 
            this.radioSixAfraid.AutoSize = true;
            this.radioSixAfraid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSixAfraid.Location = new System.Drawing.Point(261, 16);
            this.radioSixAfraid.Name = "radioSixAfraid";
            this.radioSixAfraid.Size = new System.Drawing.Size(69, 21);
            this.radioSixAfraid.TabIndex = 4;
            this.radioSixAfraid.TabStop = true;
            this.radioSixAfraid.Text = "Afraid";
            this.radioSixAfraid.UseVisualStyleBackColor = true;
            // 
            // radioSixHappy
            // 
            this.radioSixHappy.AutoSize = true;
            this.radioSixHappy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSixHappy.Location = new System.Drawing.Point(13, 16);
            this.radioSixHappy.Name = "radioSixHappy";
            this.radioSixHappy.Size = new System.Drawing.Size(72, 21);
            this.radioSixHappy.TabIndex = 3;
            this.radioSixHappy.TabStop = true;
            this.radioSixHappy.Text = "Happy";
            this.radioSixHappy.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.radioSevenSad);
            this.groupBox7.Controls.Add(this.radioSevenAngry);
            this.groupBox7.Location = new System.Drawing.Point(743, 401);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(509, 47);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Visible = false;
            // 
            // radioSevenSad
            // 
            this.radioSevenSad.AutoSize = true;
            this.radioSevenSad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSevenSad.Location = new System.Drawing.Point(261, 16);
            this.radioSevenSad.Name = "radioSevenSad";
            this.radioSevenSad.Size = new System.Drawing.Size(54, 21);
            this.radioSevenSad.TabIndex = 4;
            this.radioSevenSad.TabStop = true;
            this.radioSevenSad.Text = "Sad";
            this.radioSevenSad.UseVisualStyleBackColor = true;
            // 
            // radioSevenAngry
            // 
            this.radioSevenAngry.AutoSize = true;
            this.radioSevenAngry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSevenAngry.Location = new System.Drawing.Point(13, 16);
            this.radioSevenAngry.Name = "radioSevenAngry";
            this.radioSevenAngry.Size = new System.Drawing.Size(68, 21);
            this.radioSevenAngry.TabIndex = 3;
            this.radioSevenAngry.TabStop = true;
            this.radioSevenAngry.Text = "Angry";
            this.radioSevenAngry.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioEightSad);
            this.groupBox8.Controls.Add(this.radioEightAngry);
            this.groupBox8.Location = new System.Drawing.Point(743, 452);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(509, 47);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            // 
            // radioEightSad
            // 
            this.radioEightSad.AutoSize = true;
            this.radioEightSad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioEightSad.Location = new System.Drawing.Point(261, 16);
            this.radioEightSad.Name = "radioEightSad";
            this.radioEightSad.Size = new System.Drawing.Size(54, 21);
            this.radioEightSad.TabIndex = 4;
            this.radioEightSad.TabStop = true;
            this.radioEightSad.Text = "Sad";
            this.radioEightSad.UseVisualStyleBackColor = true;
            // 
            // radioEightAngry
            // 
            this.radioEightAngry.AutoSize = true;
            this.radioEightAngry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioEightAngry.Location = new System.Drawing.Point(13, 16);
            this.radioEightAngry.Name = "radioEightAngry";
            this.radioEightAngry.Size = new System.Drawing.Size(68, 21);
            this.radioEightAngry.TabIndex = 3;
            this.radioEightAngry.TabStop = true;
            this.radioEightAngry.Text = "Angry";
            this.radioEightAngry.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.radioTenAfraid);
            this.groupBox9.Controls.Add(this.radioTenAngry);
            this.groupBox9.Location = new System.Drawing.Point(745, 554);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(509, 47);
            this.groupBox9.TabIndex = 9;
            this.groupBox9.TabStop = false;
            // 
            // radioTenAfraid
            // 
            this.radioTenAfraid.AutoSize = true;
            this.radioTenAfraid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTenAfraid.Location = new System.Drawing.Point(261, 16);
            this.radioTenAfraid.Name = "radioTenAfraid";
            this.radioTenAfraid.Size = new System.Drawing.Size(69, 21);
            this.radioTenAfraid.TabIndex = 4;
            this.radioTenAfraid.TabStop = true;
            this.radioTenAfraid.Text = "Afraid";
            this.radioTenAfraid.UseVisualStyleBackColor = true;
            // 
            // radioTenAngry
            // 
            this.radioTenAngry.AutoSize = true;
            this.radioTenAngry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTenAngry.Location = new System.Drawing.Point(13, 16);
            this.radioTenAngry.Name = "radioTenAngry";
            this.radioTenAngry.Size = new System.Drawing.Size(68, 21);
            this.radioTenAngry.TabIndex = 3;
            this.radioTenAngry.TabStop = true;
            this.radioTenAngry.Text = "Angry";
            this.radioTenAngry.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radioNineAfraid);
            this.groupBox10.Controls.Add(this.radioNineAngry);
            this.groupBox10.Location = new System.Drawing.Point(745, 503);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(509, 47);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Visible = false;
            // 
            // radioNineAfraid
            // 
            this.radioNineAfraid.AutoSize = true;
            this.radioNineAfraid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioNineAfraid.Location = new System.Drawing.Point(261, 16);
            this.radioNineAfraid.Name = "radioNineAfraid";
            this.radioNineAfraid.Size = new System.Drawing.Size(69, 21);
            this.radioNineAfraid.TabIndex = 4;
            this.radioNineAfraid.TabStop = true;
            this.radioNineAfraid.Text = "Afraid";
            this.radioNineAfraid.UseVisualStyleBackColor = true;
            // 
            // radioNineAngry
            // 
            this.radioNineAngry.AutoSize = true;
            this.radioNineAngry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioNineAngry.Location = new System.Drawing.Point(13, 16);
            this.radioNineAngry.Name = "radioNineAngry";
            this.radioNineAngry.Size = new System.Drawing.Size(68, 21);
            this.radioNineAngry.TabIndex = 3;
            this.radioNineAngry.TabStop = true;
            this.radioNineAngry.Text = "Angry";
            this.radioNineAngry.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioTwelveAfraid);
            this.groupBox11.Controls.Add(this.radioTwelveSad);
            this.groupBox11.Location = new System.Drawing.Point(745, 656);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(509, 47);
            this.groupBox11.TabIndex = 11;
            this.groupBox11.TabStop = false;
            // 
            // radioTwelveAfraid
            // 
            this.radioTwelveAfraid.AutoSize = true;
            this.radioTwelveAfraid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTwelveAfraid.Location = new System.Drawing.Point(261, 16);
            this.radioTwelveAfraid.Name = "radioTwelveAfraid";
            this.radioTwelveAfraid.Size = new System.Drawing.Size(69, 21);
            this.radioTwelveAfraid.TabIndex = 4;
            this.radioTwelveAfraid.TabStop = true;
            this.radioTwelveAfraid.Text = "Afraid";
            this.radioTwelveAfraid.UseVisualStyleBackColor = true;
            // 
            // radioTwelveSad
            // 
            this.radioTwelveSad.AutoSize = true;
            this.radioTwelveSad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioTwelveSad.Location = new System.Drawing.Point(13, 16);
            this.radioTwelveSad.Name = "radioTwelveSad";
            this.radioTwelveSad.Size = new System.Drawing.Size(54, 21);
            this.radioTwelveSad.TabIndex = 3;
            this.radioTwelveSad.TabStop = true;
            this.radioTwelveSad.Text = "Sad";
            this.radioTwelveSad.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.radioElevenAfraid);
            this.groupBox12.Controls.Add(this.radioElevenSad);
            this.groupBox12.Location = new System.Drawing.Point(745, 605);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(509, 47);
            this.groupBox12.TabIndex = 10;
            this.groupBox12.TabStop = false;
            this.groupBox12.Visible = false;
            // 
            // radioElevenAfraid
            // 
            this.radioElevenAfraid.AutoSize = true;
            this.radioElevenAfraid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioElevenAfraid.Location = new System.Drawing.Point(261, 16);
            this.radioElevenAfraid.Name = "radioElevenAfraid";
            this.radioElevenAfraid.Size = new System.Drawing.Size(69, 21);
            this.radioElevenAfraid.TabIndex = 4;
            this.radioElevenAfraid.TabStop = true;
            this.radioElevenAfraid.Text = "Afraid";
            this.radioElevenAfraid.UseVisualStyleBackColor = true;
            // 
            // radioElevenSad
            // 
            this.radioElevenSad.AutoSize = true;
            this.radioElevenSad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioElevenSad.Location = new System.Drawing.Point(13, 16);
            this.radioElevenSad.Name = "radioElevenSad";
            this.radioElevenSad.Size = new System.Drawing.Size(54, 21);
            this.radioElevenSad.TabIndex = 3;
            this.radioElevenSad.TabStop = true;
            this.radioElevenSad.Text = "Sad";
            this.radioElevenSad.UseVisualStyleBackColor = true;
            // 
            // AKTQuestionnaireB
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.labelQuestions);
            this.Controls.Add(this.labelDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AKTQuestionnaireB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AKT Teacher Questionnaire";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelQuestions;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.RadioButton radioOneHappy;
        private System.Windows.Forms.RadioButton radioOneSad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioTwoSad;
        private System.Windows.Forms.RadioButton radioTwoHappy;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioThreeNo;
        private System.Windows.Forms.RadioButton radioThreeYes;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioFourAngry;
        private System.Windows.Forms.RadioButton radioFourHappy;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioFiveAfraid;
        private System.Windows.Forms.RadioButton radioFiveHappy;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioSixAfraid;
        private System.Windows.Forms.RadioButton radioSixHappy;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioSevenSad;
        private System.Windows.Forms.RadioButton radioSevenAngry;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioEightSad;
        private System.Windows.Forms.RadioButton radioEightAngry;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton radioTenAfraid;
        private System.Windows.Forms.RadioButton radioTenAngry;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton radioNineAfraid;
        private System.Windows.Forms.RadioButton radioNineAngry;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioTwelveAfraid;
        private System.Windows.Forms.RadioButton radioTwelveSad;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton radioElevenAfraid;
        private System.Windows.Forms.RadioButton radioElevenSad;
    }
}
