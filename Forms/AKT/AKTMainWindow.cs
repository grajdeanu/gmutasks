﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Media;
using System.Windows.Forms;
using Microsoft.DirectX.AudioVideoPlayback;
using GMUTasks.Classes;


namespace GMUTasks.Forms.AKT
{
    public partial class AKTMainWindow : Form
    {
        public TaskType GetTaskType()
        {
            return TaskType.AKT;
        }


        // Private
        private AKTTask m_fParent;
        private ArrayList m_aReceptiveData;
        private ArrayList m_aStereotypicalData;
        private ArrayList m_aNonStereotypicalData;
        private Audio m_audio;
        private Constants.AKTVersions m_eVersion = Constants.AKTVersions.VersionA;
        private Gender m_eGender = Gender.Male;
        private Ethnicity m_eEthnicity = Ethnicity.White;
        private Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices> m_dStereotypes =
            new Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices>();

        public Bitmap[] AKTFaces = {Properties.Resources.AKT_Faces_Happy,
                                    Properties.Resources.AKT_Faces_Sad,
                                    Properties.Resources.AKT_Faces_Angry,
                                    Properties.Resources.AKT_Faces_Afraid};
        public Bitmap[] AKTImageTraining = { Properties.Resources.Crayons, Properties.Resources.RubberDuck,
                                             Properties.Resources.Teddybear, Properties.Resources.WoodBocks};

        // To track randomization of faces display
        private int[] m_aCurrentFaceOrder = { -1, -1, -1, -1 };
        private int[] m_aCurrentTrainingOrder = { -1, -1, -1, -1 };         // To track randomization of training display

        public delegate void changeImageInBox(Bitmap imageName, System.Windows.Forms.PictureBox imageBox);
        public delegate void toggleImageVisible(Boolean bVisible, System.Windows.Forms.PictureBox imageBox);


        // Positional variables to generate file names for playback
        private Constants.AKTStages m_eCurrentStage = Constants.AKTStages.ReceptiveQuestions;
        private Constants.AKTFaces m_eCurrentFace = Constants.AKTFaces.Happy;
        private int m_nCurrentStereotypicalQuestion = 0;
        private int m_nCurrentNonStereotypicalQuestion = 0;
        private int m_nCurrentSubStage = 0;
        private bool m_bDidPlayIntro = false;
        private Size m_playerSize, m_frameThumbSize;
        private Point m_playerLocation, m_frameThumbLocation;
        private string m_lastNameVideo;


        // Methods
        public AKTMainWindow()
        {
            InitializeComponent();

#if(DEBUG)
            // allow backgrounding for debugging
            this.TopMost = false;
#endif
            // Initialize the Array of data fields.
            m_aReceptiveData = new ArrayList();
            m_aStereotypicalData = new ArrayList();
            m_aNonStereotypicalData = new ArrayList();

            // Hide cursor
            Cursor.Hide();

            // Set Screen to proper size
            this.SetBounds(0, 0, Constants.DeviceResolutionWidth, Constants.DeviceResolutionHeight);

            // Setup Video Element
            SetupVideoPlayer();
        }

        public void StartTask()
        {
            Console.WriteLine("AKTMainWindow::StartTask");

            if (m_fParent != null)
                m_fParent.PreventMonitorPowerdown();

            // Get the version to show.
            AKTSelectVersion versionSelectBox = new AKTSelectVersion();
            versionSelectBox.ShowDialog();
            System.Console.WriteLine("Selected Version: {0}", versionSelectBox.m_eVersionSelected);
            m_eVersion = versionSelectBox.m_eVersionSelected;

            // Get the gender.
            AKTGenderSelection genderSelectBox = new AKTGenderSelection();
            genderSelectBox.ShowDialog();
            System.Console.WriteLine("Selected Gender: {0}", genderSelectBox.m_eGenderSelected);
            m_eGender = genderSelectBox.m_eGenderSelected;

            // Get the ethnicity.
            AKTEthnicitySelection ethnicitySelectBox = new AKTEthnicitySelection();
            ethnicitySelectBox.ShowDialog();
            System.Console.WriteLine("Selected Ethnicity: {0}", ethnicitySelectBox.m_eEthnicitySelected);
            m_eEthnicity = ethnicitySelectBox.m_eEthnicitySelected;

            // Show the questionnaire.
            switch (m_eVersion)
            {
                case Constants.AKTVersions.VersionA:
                    {
                        AKTQuestionnaireA questionnaire = new AKTQuestionnaireA();
                        questionnaire.ShowDialog();
                        System.Console.WriteLine("Questionnaire Results: {0}", DictionaryToDebugString(questionnaire.m_dStereotypes));
                        m_dStereotypes = questionnaire.m_dStereotypes;
                    }
                    break;
                case Constants.AKTVersions.VersionB:
                    {
                        AKTQuestionnaireB questionnaire = new AKTQuestionnaireB();
                        questionnaire.ShowDialog();
                        System.Console.WriteLine("Questionnaire Results: {0}", DictionaryToDebugString(questionnaire.m_dStereotypes));
                        m_dStereotypes = questionnaire.m_dStereotypes;
                    }
                    break;
            }

            // Validate Questionnaire
            if (!IsQuestionnaireResultsValid())
            {
                Console.WriteLine("AKTMainWindow::StartTask: +++ERROR: Questionnaire results not valid!\n\tm_dStereoTypes={0}", DictionaryToDebugString(m_dStereotypes));
            }

            // Randomize the order of scenarios
            int nNumber;
            Random oRandom = new Random();
            for (int i = 0; i < AKTImageTraining.Count(); i++)
            {
                // Randomize each face choice
                bool bDidSucceed = false;
                while (!bDidSucceed)
                {
                    // Keep going until we find an unused value
                    nNumber = oRandom.Next(0, AKTImageTraining.Count());
                    Console.WriteLine("Found: {0} {1} {2}", nNumber, m_aCurrentTrainingOrder[nNumber], AKTImageTraining.Count());
                    if (m_aCurrentTrainingOrder[nNumber] == -1)
                    {
                        m_aCurrentTrainingOrder[nNumber] = i;
                        bDidSucceed = true;
                    }
                }
            }
            this.Show();

            StartTraining();
        }

        private void ShowImageInBox(Bitmap imageName, System.Windows.Forms.PictureBox imageBox)
        {
            imageBox.Enabled = true;
            // Check for need to scale
            if (imageBox.Width != imageName.Width)
            {
                // Resize/Scale Bitmap
                imageBox.Image = ResizeBitmap(imageName, imageBox.Width, imageBox.Height);
            }
            else
            {
                // Leave image as is
                imageBox.Image = imageName;
            }
            imageBox.Visible = true;
        }
        public Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            return result;
        }
        private void ToggleImageBoxVisibility(Boolean bVisible, System.Windows.Forms.PictureBox imageBox)
        {
            Console.WriteLine("AKTMainWindow:ToggleImageBoxVisibility: {0} -> {1}", imageBox.Visible, bVisible);
            imageBox.Visible = bVisible;
        }

        private void PlaySoundFromFile(string sFileName)
        {
            if (File.Exists(sFileName))
            {
                if (m_audio != null)
                {
                    //m_audio.Stop();
                    m_audio.Dispose();
                }
                m_audio = new Audio(sFileName);
                Console.Out.WriteLine("PlaySoundFromFile: {0} ; length: {1}", sFileName, m_audio.Duration);
                m_audio.Play();
            }
            else
            {
                Console.Out.WriteLine("PlaySoundFromFile: ERROR! File does not exist: {0}", sFileName);
            }
        }

        private void StartTraining()
        {
            DialogResult result = MessageBox.Show("Do you want to start?", "AKT", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.No)
            {
                EndThis();
            }
            else
            {
                m_eCurrentStage = Constants.AKTStages.Training;
                PlaySoundFromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Resources\\Audio\\Volume_Level.wma");
                changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                cellPictureBoxA.Invoke(pictureBoxDelegate, new object[2] { AKTImageTraining[m_aCurrentTrainingOrder[0]], cellPictureBoxA });
                cellPictureBoxB.Invoke(pictureBoxDelegate, new object[2] { AKTImageTraining[m_aCurrentTrainingOrder[1]], cellPictureBoxB });
                cellPictureBoxC.Invoke(pictureBoxDelegate, new object[2] { AKTImageTraining[m_aCurrentTrainingOrder[2]], cellPictureBoxC });
                cellPictureBoxD.Invoke(pictureBoxDelegate, new object[2] { AKTImageTraining[m_aCurrentTrainingOrder[3]], cellPictureBoxD });
            }
        }
        private void ProgressTrainingMaybe(int nIndex)
        {
            if (0 == m_aCurrentTrainingOrder[nIndex])
            {
                toggleImageVisible imageVisible = new toggleImageVisible(ToggleImageBoxVisibility);
                cellPictureBoxA.Invoke(imageVisible, new object[2] { false, cellPictureBoxA });
                cellPictureBoxB.Invoke(imageVisible, new object[2] { false, cellPictureBoxB });
                cellPictureBoxC.Invoke(imageVisible, new object[2] { false, cellPictureBoxC });
                cellPictureBoxD.Invoke(imageVisible, new object[2] { false, cellPictureBoxD });

                changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                introBox.Invoke(pictureBoxDelegate, new object[2] { Properties.Resources.CAPSEL, introBox });
                //introBox.Invoke(imageVisible, new object[2] { true, introBox });

            }
        }
        private void EndTraining()
        {
            m_audio.Dispose();

            toggleImageVisible imageVisible = new toggleImageVisible(ToggleImageBoxVisibility);
            introBox.Invoke(imageVisible, new object[2] { false, introBox });

            m_fParent.StartTimeMeasuring();

            // Start initial Stage
            StartStage(Constants.AKTStages.ReceptiveQuestions);
            //StartStage(Constants.AKTStages.NonStereotypicalScenarios);
        }

        private void StartStage(Constants.AKTStages stage)
        {
            Console.WriteLine("AKTMainWindow::StartStage: Starting stage: {0}", stage);

            switch (stage)
            {
                case Constants.AKTStages.ReceptiveQuestions:
                    {
                        // Start the Receptive Questions phase (Phase 1)
                        m_eCurrentStage = stage;
                        StartReceptivePhase();
                        break;
                    }
                case Constants.AKTStages.TeachingPhase:
                    {
                        // Start the teaching phase (Phase 2)
                        m_eCurrentStage = stage;
                        StartTeachingPhase();
                        break;
                    }
                case Constants.AKTStages.StereotypicalScenarios:
                    {
                        // Start the Stereotypical questions phase (Phase 3)
                        m_eCurrentStage = stage;
                        StartStereotypicalPhase();
                        break;
                    }
                case Constants.AKTStages.NonStereotypicalScenarios:
                    {
                        // Start the NonStereotypical question phase (Phase 4)
                        m_eCurrentStage = stage;
                        StartNonStereotypicalPhase();
                        break;
                    }
                case Constants.AKTStages.Finish:
                    {
                        m_eCurrentStage = stage;
                        // Do finish
                        ExitTask();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("AKTMainWindow::StartStage: +++ERROR: Invalid stage provided: stage={0} : {1}", (int)stage, stage);
                        break;
                    }
            }
        }

        private void StartReceptivePhase()
        {
            Console.WriteLine("AKTMainWindow::StartReceptivePhase");
            m_eCurrentFace = 0;
            m_nCurrentSubStage = 0;
            PlayReceptiveVideo();
        }

        private void PlayReceptiveVideo()
        {
            cellPictureBoxA.Hide();
            cellPictureBoxB.Hide();
            cellPictureBoxC.Hide();
            cellPictureBoxD.Hide();

            Console.WriteLine("AKTMainWindow::PlayReceptiveVideo");
            string sFullPath, sShortPath = "";
            m_nCurrentSubStage = 0;

            switch (m_eCurrentFace)
            {
                case Constants.AKTFaces.Happy:
                    sShortPath = "Receptive_Showme_HappyFace.wmv";
                    break;
                case Constants.AKTFaces.Sad:
                    sShortPath = "Receptive_Showme_SadFace.wmv";
                    break;
                case Constants.AKTFaces.Angry:
                    sShortPath = "Receptive_Showme_MadFace.wmv";
                    break;
                case Constants.AKTFaces.Afraid:
                    sShortPath = "Receptive_Showme_ScaredFace.wmv";
                    break;
                default:
                    break;
            }

            if (GetPathForVideoResourceWithName(sShortPath, out sFullPath))
            {
                axWMPVideo.URL = sFullPath;
                axWMPVideo.Ctlcontrols.play();
            }
        }

        private void SetPlayerAndCorrespondingFaceVisibility()
        {
            cellPictureBoxA.Hide();
            cellPictureBoxB.Hide();
            cellPictureBoxC.Hide();
            cellPictureBoxD.Hide();

            if ((int)m_eCurrentFace == m_aCurrentFaceOrder[0])
            {
                cellPictureBoxA.Show();
                axWMPVideo.Size = cellPictureBoxD.Size;
                axWMPVideo.Location = cellPictureBoxD.Location;
            }
                
            if ((int)m_eCurrentFace == m_aCurrentFaceOrder[1])
            {
                cellPictureBoxB.Show();
                axWMPVideo.Size = cellPictureBoxC.Size;
                axWMPVideo.Location = cellPictureBoxC.Location;
            }
            if ((int)m_eCurrentFace == m_aCurrentFaceOrder[2])
            {
                cellPictureBoxC.Show();
                axWMPVideo.Size = cellPictureBoxB.Size;
                axWMPVideo.Location = cellPictureBoxB.Location;
            }
            if ((int)m_eCurrentFace == m_aCurrentFaceOrder[3])
            {
                cellPictureBoxD.Show();
                axWMPVideo.Size = cellPictureBoxA.Size;
                axWMPVideo.Location = cellPictureBoxA.Location;
            }
        }

        private void PlayTeachingVideo()
        {
            Console.WriteLine("AKTMainWindow::PlayTeachingVideo");
            string sFullPath, sShortPath = "";

            this.Invoke(new Action(SetPlayerAndCorrespondingFaceVisibility), null);

            switch (m_eCurrentFace)
            {
                case Constants.AKTFaces.Happy:
                    sShortPath = "Teaching_HappyFace.wmv";
                    break;
                case Constants.AKTFaces.Sad:
                    sShortPath = "Teaching_SadFace.wmv";
                    break;
                case Constants.AKTFaces.Angry:
                    sShortPath = "Teaching_AngryFace.wmv";
                    break;
                case Constants.AKTFaces.Afraid:
                    sShortPath = "Teaching_ScaredFace.wmv";
                    break;
                default:
                    break;
            }

            if (GetPathForVideoResourceWithName(sShortPath, out sFullPath))
            {
                axWMPVideo.URL = sFullPath;
                axWMPVideo.Ctlcontrols.play();
            }
        }

        private void ResetPlayerPosition()
        {
            cellPictureBoxA.Hide();
            cellPictureBoxB.Hide();
            cellPictureBoxC.Hide();
            cellPictureBoxD.Hide();
            cellPictureBoxFrame.Hide();

            axWMPVideo.Size = m_playerSize;
            axWMPVideo.Location = m_playerLocation;
        }

        private void PlayStereotypicalVideo()
        {
            this.Invoke(new Action(ResetPlayerPosition), null);

            Console.WriteLine("AKTMainWindow::PlayStereotypicalVideo");
            string sFullPath, sShortPath = "";
            m_nCurrentSubStage = 0;

            if (!m_bDidPlayIntro)
            {
                sShortPath = "AKT Puppet intro_" + m_eEthnicity.ToString() + ".wmv";
            }
            else
            {
                switch (m_eVersion)
                {
                    case Constants.AKTVersions.VersionA:
                        Constants.AKTStereotypicalVersionA curPosA = (Constants.AKTStereotypicalVersionA)m_nCurrentStereotypicalQuestion;
                        sShortPath = GetFilenameForStereotypicalVideoWithVersion(curPosA.ToString());
                        break;
                    case Constants.AKTVersions.VersionB:
                        Constants.AKTStereotypicalVersionB curPosB = (Constants.AKTStereotypicalVersionB)m_nCurrentStereotypicalQuestion;
                        sShortPath = GetFilenameForStereotypicalVideoWithVersion(curPosB.ToString());
                        break;
                }

            }

            if (GetPathForVideoResourceWithName(sShortPath, out sFullPath))
            {
                axWMPVideo.URL = sFullPath;
                axWMPVideo.Ctlcontrols.play();
            }
        }

        private string GetFilenameForStereotypicalVideoWithVersion(string version)
        {
            string sRet = "AKT Stereo_" + m_eEthnicity.ToString() + "_" + m_eGender.ToString()[0] + "_" + (m_eVersion == Constants.AKTVersions.VersionA ? (m_nCurrentStereotypicalQuestion + 2) : (m_nCurrentStereotypicalQuestion + 6)).ToString() + "_" + version + ".wmv";
            Console.WriteLine("AKTMainWindow::GetFilenameForStereotypicalVideoWithVersion: sRet = {0}", sRet);
            return sRet;
        }

        private string GetFilenameForNonStereotypicalVideoWithVersion(string version)
        {
            string sRet = "AKT NonStereo_" + m_eEthnicity.ToString() + "_" + m_eGender.ToString()[0] + "_" + (m_nCurrentNonStereotypicalQuestion + 1).ToString() + (m_dStereotypes[(Constants.AKTNonStereotypicalQuestions)m_nCurrentNonStereotypicalQuestion] == Constants.AKTQuestionnaireChoices.ChoiceA ? "A" : "B") + "_" + version + ".wmv";
            Console.WriteLine("AKTMainWindow::GetFilenameForlVideoWithVersion: sRet = {0}", sRet);
            return sRet;
        }

        private void PlayNonStereotypicalVideo()
        {
            this.Invoke(new Action(ResetPlayerPosition), null);

            Console.WriteLine("AKTMainWindow::PlayNonStereotypicalVideo");
            string sFullPath, sShortPath = "";
            m_nCurrentSubStage = 0;
            Constants.AKTNonStereotypicalQuestions curPos = (Constants.AKTNonStereotypicalQuestions)m_nCurrentNonStereotypicalQuestion;
            sShortPath = GetFilenameForNonStereotypicalVideoWithVersion(curPos.ToString());

            if (GetPathForVideoResourceWithName(sShortPath, out sFullPath))
            {
                axWMPVideo.URL = sFullPath;
                axWMPVideo.Ctlcontrols.play();
            }
        }

        private void HandleNotification()
        {
            Console.WriteLine("AKTMainWindow::HandleNotification m_eCurrentStage={0} m_nCurrentFace={1} m_nCurrentSubStage={2}", m_eCurrentStage, m_eCurrentFace, m_nCurrentSubStage);
            switch (m_eCurrentStage)
            {
                case Constants.AKTStages.ReceptiveQuestions:
                    switch (m_nCurrentSubStage)
                    {
                        default:
                        case 0:
                            // Finished showing video for face - show face selection now
                            PerformFaceSelection();
                            break;
                        case 1:
                            // Finished showing Face selection - 
                            if ((int)m_eCurrentFace < 3)
                            {
                                // show next face video
                                m_eCurrentFace++;
                                PlayReceptiveVideo();
                            }
                            else
                            {
                                // move to next stage
                                Console.WriteLine("AKTMainWindow::HandleNotification: Receptive Phase Data: {0}", string.Join("|", (string[])m_aReceptiveData.ToArray(Type.GetType("System.String"))));
                                StartStage(Constants.AKTStages.TeachingPhase);
                            }
                            break;
                    }
                    break;
                case Constants.AKTStages.TeachingPhase:
                    // Finished showing face
                    if ((int)m_eCurrentFace < 3)
                    {
                        // show next face video
                        m_eCurrentFace++;
                        System.Timers.Timer timer = new System.Timers.Timer();
                        timer.Elapsed += new System.Timers.ElapsedEventHandler(TimedPlayTeachingVideo);
                        timer.Interval = Constants.AKTVideoTimerDuration;
                        timer.Enabled = true;
                    }
                    else
                    {
                        // move to next stage
                        Console.WriteLine("AKTMainWindow::HandleNotification: Finished TeachingPhase");
                        StartStage(Constants.AKTStages.StereotypicalScenarios);
                    }
                    break;
                case Constants.AKTStages.StereotypicalScenarios:
                    switch (m_nCurrentSubStage)
                    {
                        default:
                        case 0:
                            // Finished showing video for stereotypical - show face selection now
                            if (!m_bDidPlayIntro)
                            {
                                m_bDidPlayIntro = true;
                                System.Timers.Timer timer = new System.Timers.Timer();
                                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimedPlayStereotypicalVideo);
                                timer.Interval = Constants.AKTVideoTimerDuration;
                                timer.Enabled = true;
                            }
                            else
                            {
                                PerformFaceSelection();
                            }
                            break;
                        case 1:
                            // Finished showing Face selection
                            if (m_nCurrentStereotypicalQuestion < 2)
                            {
                                // Show next video
                                m_nCurrentStereotypicalQuestion++;
                                System.Timers.Timer timer = new System.Timers.Timer();
                                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimedPlayStereotypicalVideo);
                                timer.Interval = Constants.AKTVideoTimerDuration;
                                timer.Enabled = true;
                            }
                            else
                            {
                                // move to next stage
                                Console.WriteLine("AKTMainWindow::HandleNotification: Stereotypical Phase Data: {0}", string.Join("|", (string[])m_aStereotypicalData.ToArray(Type.GetType("System.String"))));
                                StartStage(Constants.AKTStages.NonStereotypicalScenarios);
                            }
                            break;
                    }
                    break;
                case Constants.AKTStages.NonStereotypicalScenarios:
                    switch (m_nCurrentSubStage)
                    {
                        default:
                        case 0:
                            // Finished showing video for stereotypical - show face selection now
                            PerformFaceSelection();
                            break;
                        case 1:
                            // Finished showing Face selectio
                            if (m_nCurrentNonStereotypicalQuestion < 10)
                            {
                                // Show next video
                                m_nCurrentNonStereotypicalQuestion += 2;
                                System.Timers.Timer timer = new System.Timers.Timer();
                                timer.Elapsed += new System.Timers.ElapsedEventHandler(TimedPlayNonStereotypicalVideo);
                                timer.Interval = Constants.AKTVideoTimerDuration;
                                timer.Enabled = true;
                            }
                            else
                            {
                                // move to next stage
                                Console.WriteLine("AKTMainWindow::HandleNotification: NonStereotypical Phase Data: {0}", string.Join("|", (string[])m_aNonStereotypicalData.ToArray(Type.GetType("System.String"))));
                                StartStage(Constants.AKTStages.Finish);
                            }
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        private void TimedPlayTeachingVideo(object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("AKTMainWindow::TimedPlayTeachingVideo");
            ((System.Timers.Timer)source).Stop();
            PlayTeachingVideo();
        }

        private void TimedPlayStereotypicalVideo(object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("AKTMainWindow::TimedPlayStereotypicalVideo");
            ((System.Timers.Timer)source).Stop();
            PlayStereotypicalVideo();
        }

        private void TimedPlayNonStereotypicalVideo(object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("AKTMainWindow::TimedPlayNonStereotypicalVideo");
            ((System.Timers.Timer)source).Stop();
            PlayNonStereotypicalVideo();
        }

        private void PerformFaceSelection()
        {
            Console.WriteLine("AKTMainWindow::PerformFaceSelection");
            GenerateRandomFaceOrder();
            m_nCurrentSubStage++;

            cellPictureBoxA.Image = AKTFaces[m_aCurrentFaceOrder[0]];
            cellPictureBoxB.Image = AKTFaces[m_aCurrentFaceOrder[1]];
            cellPictureBoxC.Image = AKTFaces[m_aCurrentFaceOrder[2]];
            cellPictureBoxD.Image = AKTFaces[m_aCurrentFaceOrder[3]];

            cellPictureBoxA.Show();
            cellPictureBoxB.Show();
            cellPictureBoxC.Show();
            cellPictureBoxD.Show();
        }

        private void StartTeachingPhase()
        {
            Console.WriteLine("AKTMainWindow::StartTeachingPhase");
            m_eCurrentFace = 0;
            m_nCurrentSubStage = 0;

            cellPictureBoxA.Hide();
            cellPictureBoxB.Hide();
            cellPictureBoxC.Hide();
            cellPictureBoxD.Hide();

            PlayTeachingVideo();
        }

        private void RepositionUIElements()
        {
            int available, spanBefore, gapBefore, spanAfter, gapAfter;
            spanBefore = cellPictureBoxA.Size.Width;
            gapBefore = cellPictureBoxB.Location.X - cellPictureBoxA.Location.X - spanBefore;
            available = spanBefore + spanBefore + gapBefore;
            double alpha = (double)(2 * spanBefore + gapBefore) / (3 * spanBefore + 2 * gapBefore);
            spanAfter = (int)(alpha * spanBefore);
            gapAfter = (int)(alpha * gapBefore);
            cellPictureBoxFrame.Location = cellPictureBoxA.Location;
            cellPictureBoxFrame.Size = new Size(spanAfter, cellPictureBoxA.Size.Height + cellPictureBoxC.Location.Y - cellPictureBoxA.Location.Y);
            int [] x = new int[6];
            x[0] = cellPictureBoxA.Location.X;
            x[5] = cellPictureBoxB.Location.X + cellPictureBoxB.Size.Width;
            x[2] = x[0] + spanAfter + gapAfter;
            x[4] = x[5] - spanAfter;
            x[1] = x[0] + spanAfter;
            x[3] = x[2] + spanAfter;
            cellPictureBoxA.Hide();
            cellPictureBoxB.Hide();
            cellPictureBoxC.Hide();
            cellPictureBoxD.Hide();
            cellPictureBoxA.Location = new Point(x[2], cellPictureBoxA.Location.Y);
            cellPictureBoxA.Size = new Size(spanAfter, cellPictureBoxA.Height);
            cellPictureBoxB.Location = new Point(x[4], cellPictureBoxB.Location.Y);
            cellPictureBoxB.Size = new Size(spanAfter, cellPictureBoxB.Height);
            cellPictureBoxC.Location = new Point(x[2], cellPictureBoxC.Location.Y);
            cellPictureBoxC.Size = new Size(spanAfter, cellPictureBoxC.Height);
            cellPictureBoxD.Location = new Point(x[4], cellPictureBoxD.Location.Y);
            cellPictureBoxD.Size = new Size(spanAfter, cellPictureBoxD.Height);

            List<Bitmap> bmps = new List<Bitmap>();
            foreach (Bitmap bmp in AKTFaces)
            {
                Bitmap bmpNew = new Bitmap(spanAfter, bmp.Height);
                int baseX = (bmp.Width - spanAfter) / 2;
                for (int xx=0; xx < spanAfter; ++ xx)
                    for (int yy = 0; yy < bmp.Height; ++ yy)
                        bmpNew.SetPixel(xx, yy, bmp.GetPixel(xx + baseX, yy));
                bmps.Add(bmpNew);
            }
            AKTFaces = bmps.ToArray();
        }

        private void StartStereotypicalPhase()
        {
            Console.WriteLine("AKTMainWindow::StartStereotypicalPhase");
            this.Invoke(new Action(RepositionUIElements), null);
            m_eCurrentFace = 0;
            m_nCurrentSubStage = 0;
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(TimedPlayStereotypicalVideo);
            timer.Interval = Constants.AKTVideoTimerDuration;
            timer.Enabled = true;
        }

        private void StartNonStereotypicalPhase()
        {
            Console.WriteLine("AKTMainWindow::NonStartStereotypicalPhase");
            m_eCurrentFace = 0;
            m_nCurrentSubStage = 0;
            if (m_eVersion == Constants.AKTVersions.VersionB)
                m_nCurrentNonStereotypicalQuestion = 1;
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(TimedPlayNonStereotypicalVideo);
            timer.Interval = Constants.AKTVideoTimerDuration;
            timer.Enabled = true;
        }

        private void GenerateRandomFaceOrder()
        {
            Console.WriteLine("AKTMainWindow::GenerateRandomFaceOrder");
            int nNumber;
            Random oRandom = new Random();

            m_aCurrentFaceOrder[0] = -1;
            m_aCurrentFaceOrder[1] = -1;
            m_aCurrentFaceOrder[2] = -1;
            m_aCurrentFaceOrder[3] = -1;

            for (int i = 0; i < AKTFaces.Count(); i++)
            {
                // Randomize each face choice
                bool bDidSucceed = false;
                while (!bDidSucceed)
                {
                    // Keep going until we find an unused value
                    nNumber = oRandom.Next(0, AKTFaces.Count());
                    Console.WriteLine("Found: {0} {1} {2}", nNumber, m_aCurrentFaceOrder[nNumber], AKTFaces.Count());
                    if (m_aCurrentFaceOrder[nNumber] == -1)
                    {
                        m_aCurrentFaceOrder[nNumber] = i;
                        bDidSucceed = true;
                    }
                }
            }
        }

        private void SetupVideoPlayer()
        {
            //axWMPVideo = new AxWMPLib.AxWindowsMediaPlayer();
            axWMPVideo.uiMode = "none";
            //axWMPVideo.Size = new Size(360, 272);
            //axWMPVideo.Location = new Point(460, 20);
            m_playerSize = axWMPVideo.Size;
            m_playerLocation = axWMPVideo.Location;



            axWMPVideo.enableContextMenu = false;
            axWMPVideo.settings.autoStart = false;
            axWMPVideo.settings.setMode("AutoRewind", false);
            axWMPVideo.settings.setMode("showFrame", true);
            axWMPVideo.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(axWMPVideo_StateChange);
            axWMPVideo.Hide();
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            bool bPlaying = false;
            if (m_audio != null && !m_audio.Disposed)
            {
                bPlaying = m_audio.Playing;
                if (bPlaying)
                    m_audio.Pause();
            }
            m_fParent.EndTimeMeasuring();
            if (MessageBox.Show("Are you sure you are finished with this assessment for student " + m_fParent.m_oStudent.UUID + "?",
                                "Are you finished?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (MessageBox.Show("Would you like to record the observations for this student?",
                                "Record Observations?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Console.WriteLine("AKTMainWindow:FinishedAndSave()");
                    RecordResults();
                }
                else
                {
                    Console.WriteLine("AKTMainWindow:FinishedAndDiscardObservations()");
                }

                EndThis();
            }
            else
            {
                if (bPlaying)
                    m_audio.Play();
            }
        }

        private void EndThis()
        {
            // stop if still running
            if (m_audio != null)
                m_audio.Dispose();
            if (axWMPVideo.playState != WMPLib.WMPPlayState.wmppsStopped)
                axWMPVideo.Ctlcontrols.stop();

            m_fParent.AllowMonitorPowerdown();
            Cursor.Show();
            this.Close();
        }

        public AKTTask Parent
        {
            get
            {
                return m_fParent;
            }
            set
            {
                m_fParent = value;
            }
        }

        private void ExitTask()
        {
            // Exit Task - Write results
            MessageBox.Show("The AKT (Version " + (m_eVersion == Constants.AKTVersions.VersionA ? "A" : "B") + ") for Student " + m_fParent.m_oStudent.UUID + " is complete.", "Task Complete");
            RecordResults();

            Cursor.Show();
            m_fParent.AllowMonitorPowerdown();
            m_fParent.CloseWindow();
        }

        public void RecordResults()
        {
            // TODO: Save Results
            Console.Out.WriteLine("Writing: {0}", GetStringForWrite());
            System.IO.File.AppendAllText(GetFileNameForResultOutput(), GetStringForWrite());
        }

        public string[] GetArrayForCheckbox(PictureBox pb, int nIndex)
        {
            string sIndex = nIndex.ToString();
            string sValue = pb.Tag.Equals(Constants.Disabled) ? "0" : "1";
            string[] arr = { sIndex, sValue };
            Console.Out.WriteLine("GetArrayForCheckbox: ret={0}", string.Join(",", arr));
            return arr;
        }

        public string GetStringForWrite()
        {
            string sSep = ",";
            string sRet = m_fParent.m_oStudent.UUID + sSep + Parent.m_oTeacher.UUID + sSep + Parent.m_oTeacher.LastName + sSep;
            sRet += DateTime.Now + sSep;
            sRet += m_fParent.GetTimeMeasureInSeconds() + sSep;
            sRet += m_eVersion + sSep;
            sRet += m_eGender + sSep;
            sRet += m_eEthnicity + sSep;
            sRet += GetStringFromDictionary(m_dStereotypes) + sSep;
            sRet += GetStringFromDataArray(m_aReceptiveData) + sSep;
            sRet += GetStringFromDataArray(m_aStereotypicalData) + sSep;
            sRet += GetStringFromDataArray(m_aNonStereotypicalData) + "\n";
            return sRet;
        }

        private string GetStringFromDataArray(ArrayList ar)
        {
            string sReturn = string.Join(":", (string[])ar.ToArray(Type.GetType("System.String")));
            Console.Out.WriteLine("AKTMainWindow::GetStringFromDataArray: ret={0}", sReturn);
            return sReturn;
        }

        private string GetStringFromDictionary(Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices> dict)
        {
            string sRet = "";

            foreach (KeyValuePair<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices> kvpPair in dict)
            {
                sRet += ((int)kvpPair.Key).ToString() + "," + ((int)kvpPair.Value).ToString();
                dict.Last();
                if (dict.Keys.Last() != kvpPair.Key)
                    sRet += ",";
            }

            return sRet;
        }

        private string GetFileNameForResultOutput()
        {
            //string result = System.Environment.MachineName.ToLower() + "_" + DateTime.Now.ToString("yyyyMMdd") + "_AKT.csv";
            string result = Parent.m_oStudent.UUID + "_AKT_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            bool finishloop;
            do
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Results files (*.csv)|*.csv|All files|*.*";
                sfd.FileName = result;
                sfd.Title = "Choose the file where these results will be stored";
                //sfd.CheckFileExists = true;
                finishloop = sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                if (finishloop)
                    result = sfd.FileName;
            } while (!finishloop);

            return result;
        }

        private void axWMPVideo_StateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            AxWMPLib.AxWindowsMediaPlayer player = (AxWMPLib.AxWindowsMediaPlayer)sender;
            WMPLib.IWMPControls2 controls = (WMPLib.IWMPControls2)axWMPVideo.Ctlcontrols;
            System.Console.WriteLine("State: {0} - Position: {1}", (WMPLib.WMPPlayState)e.newState, player.Ctlcontrols.currentPosition);

            if ((WMPLib.WMPPlayState)e.newState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                axWMPVideo.Show();
                //player.fullScreen = false;
                //player.stretchToFit = false;
                //System.Threading.Thread.Sleep(300);
                //player.fullScreen = true;
                player.stretchToFit = true;
            }
            else if ((WMPLib.WMPPlayState)e.newState == WMPLib.WMPPlayState.wmppsStopped)
            {
                if (m_eCurrentStage == Constants.AKTStages.StereotypicalScenarios ||
                    m_eCurrentStage == Constants.AKTStages.NonStereotypicalScenarios)
                {
                    string sName;
                    if (GetPathForLastFrameResource(out sName))
                    {
                        Bitmap bmpFrame = new Bitmap(sName);
                        Size sz = new Size(cellPictureBoxFrame.Width, (int)((double)cellPictureBoxFrame.Width * bmpFrame.Height / bmpFrame.Width));
                        cellPictureBoxFrame.Image = new Bitmap(bmpFrame, sz);
                        cellPictureBoxFrame.Show();
                    }
                }

                axWMPVideo.Hide();
                HandleNotification();
            }
        }

        private bool GetPathForVideoResourceWithName(string sName, out string sPath)
        {
            sPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + Constants.AKTVideoPath + sName;
            bool rc = System.IO.File.Exists(sPath);
            Console.Out.WriteLine("AKTMainWindow::GetPathForVideoResourceWithName: sName={0} sPath={1} rc={2}", sName, sPath, rc);
            m_lastNameVideo = sName;

            if (!rc)
                m_lastNameVideo = sPath = "";

            return rc;
        }

        private bool GetPathForLastFrameResource(out string sPath)
        {
            sPath = "";
            if (m_lastNameVideo == "")
                return false;
            sPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + Constants.AKTFramePath + m_lastNameVideo.Replace(".wmv", ".jpg");
            bool rc = System.IO.File.Exists(sPath);
            if (!rc)
                sPath = "";

            return rc;
        }

        private string DictionaryToDebugString(Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices> dict)
        {
            string returnString = "{" + Environment.NewLine;

            foreach (KeyValuePair<Constants.AKTNonStereotypicalQuestions, Constants.AKTQuestionnaireChoices> kvpPair in dict)
            {
                returnString += "\t" + ((int)kvpPair.Key + 1) + ". " + kvpPair.Key + " => " + kvpPair.Value + Environment.NewLine;
            }

            returnString += "}";

            return returnString;
        }

        private bool IsQuestionnaireResultsValid()
        {
            bool bRet = true;

            for (Constants.AKTNonStereotypicalQuestions ques = 0; (int)ques < 12; ques++)
            {
                if (m_fParent.mapStereotypicalQuestionsToVersion[ques] == m_eVersion && !m_dStereotypes.ContainsKey(ques))
                    bRet = false;
            }

            return bRet;
        }

        private void cellPictureBoxA_Click(object sender, EventArgs e)
        {
            HandlePictureBoxClick(0);
        }

        private void cellPictureBoxB_Click(object sender, EventArgs e)
        {
            HandlePictureBoxClick(1);
        }

        private void cellPictureBoxC_Click(object sender, EventArgs e)
        {
            HandlePictureBoxClick(2);
        }

        private void cellPictureBoxD_Click(object sender, EventArgs e)
        {
            HandlePictureBoxClick(3);
        }

        private void HandlePictureBoxClick(int box)
        {
            switch (m_eCurrentStage)
            {
                case Constants.AKTStages.Training:
                    ProgressTrainingMaybe(box);
                    break;
                case Constants.AKTStages.ReceptiveQuestions:
                    Console.WriteLine("AKTMainWindow::HandlePictureBoxClick: m_eCurrentFace={0} selectedFace={1}", m_eCurrentFace, (Constants.AKTFaces)m_aCurrentFaceOrder[box]);

                    // Record Click
                    m_aReceptiveData.Add((int)m_eCurrentFace + "," + m_aCurrentFaceOrder[box]);

                    // Send notification
                    HandleNotification();

                    break;

                case Constants.AKTStages.StereotypicalScenarios:

                    Console.WriteLine("AKTMainWindow::HandlePictureBoxClick: m_nCurrentStereotypicalQuestion={0} selectedFace={1}", (m_eVersion == Constants.AKTVersions.VersionA ? (m_nCurrentStereotypicalQuestion + 2) : (m_nCurrentStereotypicalQuestion + 6)), (Constants.AKTFaces)m_aCurrentFaceOrder[box]);

                    // Record Click
                    m_aStereotypicalData.Add((int)(m_eVersion == Constants.AKTVersions.VersionA ? (m_nCurrentStereotypicalQuestion + 2) : (m_nCurrentStereotypicalQuestion + 6)) + "," + m_aCurrentFaceOrder[box]);

                    // Send notification
                    HandleNotification();
                    break;

                case Constants.AKTStages.NonStereotypicalScenarios:
                    Console.WriteLine("AKTMainWindow::HandlePictureBoxClick: m_nCurrentNonStereotypicalQuestion={0} selectedFace={1}", m_nCurrentNonStereotypicalQuestion, (Constants.AKTFaces)m_aCurrentFaceOrder[box]);

                    // Record Click
                    m_aNonStereotypicalData.Add((int)m_nCurrentNonStereotypicalQuestion + "," + m_aCurrentFaceOrder[box]);

                    // Send notification
                    HandleNotification();
                    break;

                case Constants.AKTStages.TeachingPhase:
                default:
                    break;
            }
        }

        private void introBox_Click(object sender, EventArgs e)
        {
            EndTraining();
        }
    }
}
