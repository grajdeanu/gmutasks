﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Forms.AKT
{
    public partial class AKTSelectVersion : Form
    {
        public Constants.AKTVersions m_eVersionSelected;

        public AKTSelectVersion()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();

            m_eVersionSelected = Constants.AKTVersions.VersionA;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_eVersionSelected = Constants.AKTVersions.VersionA;
            Dismiss();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            m_eVersionSelected = Constants.AKTVersions.VersionB;
            Dismiss();
        }
        
        private void Dismiss()
        {
            Cursor.Show();
            this.Close();
        }

    }
}
