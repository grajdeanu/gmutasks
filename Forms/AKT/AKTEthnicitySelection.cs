﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMUTasks.Classes;

namespace GMUTasks.Forms.AKT
{
    public partial class AKTEthnicitySelection : Form
    {
        public Ethnicity m_eEthnicitySelected;

        public AKTEthnicitySelection()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();

            m_eEthnicitySelected = Ethnicity.White;
        }

        private void buttonCaucasian_Click(object sender, EventArgs e)
        {
            m_eEthnicitySelected = Ethnicity.White;
            Dismiss();
        }


        private void buttonAsian_Click(object sender, EventArgs e)
        {
            m_eEthnicitySelected = Ethnicity.Asian;
            Dismiss();
        }

        private void buttonHispanic_Click(object sender, EventArgs e)
        {
            m_eEthnicitySelected = Ethnicity.Hispanic;
            Dismiss();
        }

        private void buttonAfricanAmerican_Click(object sender, EventArgs e)
        {
            m_eEthnicitySelected = Ethnicity.Black;
            Dismiss();
        }

        private void Dismiss()
        {
            Cursor.Show();
            this.Close();
        }

    }
}
