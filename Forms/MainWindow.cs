﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMUTasks.Classes;

namespace GMUTasks
{

    public enum TaskType
    {
        None = 0,
        CST, 
        PSRA,
        AKT,
        MPAC
    }

    public partial class MainWindow : Form
    {
        public MainLogin m_fParentForm;
        private CUser m_oTeacher;
        //private ArrayList m_aStudents;

        public MainWindow()
        {
            Console.Out.WriteLine("MainWindow() - Create");
            InitializeComponent();
        }

        public MainWindow(MainLogin fParentForm)
        {
            Console.Out.WriteLine("MainWindow(Form) - Create");
            m_fParentForm = fParentForm;
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

            // Get Students list to add to Combo Box
            //m_aStudents = m_fParentForm.GetStudentsForTeacherID(m_oTeacher.UUID);
            
            //IEnumerator studentEnumerator = m_aStudents.GetEnumerator();
            //StudentIdComboBox.Items.Clear();

            //while (studentEnumerator.MoveNext())
            //{
            //    Console.Out.WriteLine("Adding Student: {0}",studentEnumerator.Current);
            //    StudentIdComboBox.Items.Add(studentEnumerator.Current);
            //}
            
            // Enable combo box if users are available
            //StudentIdComboBox.Enabled = (StudentIdComboBox.Items.Count > 0);
         
            // Set Teacher ID
            TeacherIDBox.Text = m_oTeacher.UUID;
            StudentIdComboBox.Enabled = true;

            // Set available Tasks, if any.
            UpdateTaskButtons(false);
   
        }

        private void startButtonCST_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("GMUTasks::MainWindow::startButtonCST_Click");
            if (MessageBox.Show("Would you like to start the Challenging Skills Task with Student " + StudentIdComboBox.Text + "?",
                                "Start CST?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                CSTTask newCSTTask = new CSTTask();
                newCSTTask.m_oStudent = m_fParentForm.GetUserById(StudentIdComboBox.Text);
                newCSTTask.m_oTeacher = m_oTeacher;
                newCSTTask.LoadWindow();
            }

        }

        private void startButtonAKT_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("MainWindow::startButtonAKT_Click");
            if (MessageBox.Show("Would you like to start the Affect Knowledge Task (AKT) with Student " + StudentIdComboBox.Text + "?",
                                "Start MPAC?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                AKTTask newAKTTask = new AKTTask();
                newAKTTask.m_oStudent = m_fParentForm.GetUserById(StudentIdComboBox.Text);
                newAKTTask.m_oTeacher = m_oTeacher;
                newAKTTask.LoadWindow();
            }
        }

        private void startButtonMPAC_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("MainWindow::startButtonMPAC_Click");
            if (MessageBox.Show("Would you like to start the Minnesota Preschool Affect Checklist (MPAC) with Student " + StudentIdComboBox.Text + "?",
                                "Start MPAC?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                MPACTask newMPACTask = new MPACTask();
                newMPACTask.m_oStudent = m_fParentForm.GetUserById(StudentIdComboBox.Text);
                newMPACTask.m_oTeacher = m_oTeacher;
                newMPACTask.LoadWindow();
            }
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("MainWindow::LogoutButton_Click");
            m_fParentForm.LogoutUser();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Console.Out.WriteLine("MainWindow::MainWindow_FormClosing");
            m_fParentForm.LogoutUser();
        }

        public CUser Teacher
        {
            get
            {
                return m_oTeacher;
            }
            set
            {
                m_oTeacher = value;
            }
        }

        private void StudentIdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTaskButtons((StudentIdComboBox.Text.Length > 0));
        }

        private void StudentIdComboBox_OnTextChanged(object sender, EventArgs e)
        {
            UpdateTaskButtons((StudentIdComboBox.Text.Length > 0));
        }

        private void UpdateTaskButtons(Boolean bEnabled)
        {
            startButtonCST.Enabled  = bEnabled;
            startButtonMPAC.Enabled = bEnabled;
            startButtonAKT.Enabled  = bEnabled;
        }

    }
}
