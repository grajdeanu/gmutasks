﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMUTasks.Classes;


namespace GMUTasks.Forms.MPAC
{
    public partial class MPACMainWindow : Form
    {
        public TaskType GetTaskType()
        {
            return TaskType.CST;
        }


        // Private
        private int nAttempts;
        private MPACTask m_fParent;
        private ArrayList m_aRecordedData;
        private string m_sFocalData = "";
        private string m_sComments  = "";
        private Timer taskTimer;
        private DateTime taskTimerEnd;
        private int m_nPausedSeconds = 0;
        private Boolean m_bPaused = false;
        private int m_nCurrentTabIndex = 0;

        // Methods

        public MPACMainWindow()
        {
            InitializeComponent();

            // Initialize the Array of data fields.
            m_aRecordedData = new ArrayList();

            // Positive Emotion
            check_0_0.Click += new EventHandler(checkClick);
            check_0_1.Click += new EventHandler(checkClick);
            check_0_2.Click += new EventHandler(checkClick);
            // Negative Emotion
            check_1_0.Click += new EventHandler(checkClick);
            check_1_1.Click += new EventHandler(checkClick);
            // Productive
            check_2_0.Click += new EventHandler(checkClick);
            check_2_1.Click += new EventHandler(checkClick);
            // Unproductive
            check_3_0.Click += new EventHandler(checkClick);
            check_3_1.Click += new EventHandler(checkClick);
            // Negative to Frustration
            check_4_0.Click += new EventHandler(checkClick);
            check_4_1.Click += new EventHandler(checkClick);
            // Positive to Frustration
            check_5_0.Click += new EventHandler(checkClick);
            check_5_1.Click += new EventHandler(checkClick);
            // Empathy
            check_8_0.Click += new EventHandler(checkClick);
            check_8_1.Click += new EventHandler(checkClick);
            check_8_2.Click += new EventHandler(checkClick);
            // Unusual
            check_6_0.Click += new EventHandler(checkClick);
            check_6_1.Click += new EventHandler(checkClick);
            // Peer Skills
            check_7_0.Click += new EventHandler(checkClick);
            check_7_1.Click += new EventHandler(checkClick);

            tabControl1.SelectedIndexChanged += new EventHandler(tabControl1_TabIndexChanged);

#if(DEBUG)
            // allow backgrounding for debugging
            this.TopMost = false;
#endif

            taskTimer = new Timer();

            // Set Screen to proper size
            this.SetBounds(0, 0, Constants.DeviceResolutionWidth, Constants.DeviceResolutionHeight);

        }

        public void StartTask()
        {
            if (m_fParent != null)
                m_fParent.PreventMonitorPowerdown();

            // Check for total of prior attempts
            nAttempts = GetNumberOfRecordsInFileForUser(GetFileNameForResultOutput(), m_fParent.m_oStudent.UUID);
            if (nAttempts >= Constants.MPACAllowedAttempts)
            {
                MessageBox.Show("The student " + m_fParent.m_oStudent.UUID + " has already had " + nAttempts.ToString() + "/" + Constants.MPACAllowedAttempts.ToString() + " observations. Please select another student.",
                                "This student has completed the maximum number of observations",
                                MessageBoxButtons.OK);
                this.Close();
                return;
            }
            else
            {
                labelObservations.Text = "Observation: " + (nAttempts + 1).ToString() + "/" + Constants.MPACAllowedAttempts;
            }


            m_fParent.StartTimeMeasuring();

            // Hide cursor
            Cursor.Hide();

            // Setup and Start timer
            taskTimer.Interval = 1000;
            taskTimer.Start();
            taskTimer.Tick += new EventHandler(taskTimerTick);
#if(DEBUG)
            taskTimerEnd = DateTime.Now.AddSeconds(35);
            labelTimer.Text = "0:35";

#else
            taskTimerEnd = DateTime.Now.AddMinutes(Constants.MPACDurationInMinutes);
            labelTimer.Text = Constants.MPACDurationInMinutes.ToString() + ":00";
#endif

            // Set initial Scenario
            this.Show();
        }

        private void Quit()
        {
            m_fParent.EndTimeMeasuring();

            if (MessageBox.Show("Would you like to record the data for this student?",
                            "Record Observations?",
                            MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Console.WriteLine("MPACMainWindow:FinishedAndSave()");
                RecordResults();
            }
            else
            {
                Console.WriteLine("MPACMainWindow:FinishedAndDiscardObservations()");
            }

            if (taskTimer != null)
                taskTimer.Enabled = false;

            Cursor.Show();
            this.Close();
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you are finished with this assessment for student " + m_fParent.m_oStudent.UUID + "?",
                                "Are you finished?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Quit();
            }
        }

        public MPACTask Parent
        {
            get
            {
                return m_fParent;
            }
            set
            {
                m_fParent = value;
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkClick(object sender, EventArgs e)
        {
            toggleCheckbox((sender as PictureBox));
        }

        private void toggleCheckbox(PictureBox box)
        {
            if (box.Tag == null)
                return;

            bool bWasDisabled = box.Tag.Equals(Constants.Disabled);
            Console.Out.WriteLine("MPACMainWindow::toggleCheckbox: {0} ({1} -> {2})", box.Name, (bWasDisabled ? "Enabled" : "Disabled"), (!bWasDisabled ? "Enabled" : "Disabled"));
            if (bWasDisabled)
            {
                box.Image = Properties.Resources.check_full;
                box.Tag = Constants.Enabled;
            }
            else
            {
                box.Image = Properties.Resources.check_empty;
                box.Tag = Constants.Disabled;
            }

            Control[] controls = this.Controls.Find(box.Name.Replace("check", "label"), true);
            if (controls.Length == 1) // 0 means not found, more - there are several controls with the same name
            {
                Label control = controls[0] as Label;
                if (control != null)
                {
                    control.ForeColor = (bWasDisabled ? Color.LightGray : Color.Black);
                }
            }
        }

        public void taskTimerTick(object sender, EventArgs eArgs)
        {
            if (sender == taskTimer && !m_bPaused)
            {
                int elapsedSeconds = (int)taskTimerEnd.Subtract(DateTime.Now).TotalSeconds + m_nPausedSeconds; // subtract start time from end time, and take into account the paused time
                SetTimeInLabel(labelTimer, elapsedSeconds);

                if (elapsedSeconds <= 0)
                {
                    (sender as Timer).Stop();
                    QuitOnTimer();
                }
            }
            else if (m_bPaused)
            {
                m_nPausedSeconds++;
            }
        }

        public void SetTimeInLabel(Label label, int seconds)
        {
            string TimeInString = "";

            if (seconds <= 30)
                label.ForeColor = Color.Red;
            else
                label.ForeColor = Color.Black;

            int min = seconds / 60;
            int sec = seconds % 60;

            TimeInString += (min < 1) ? "0" : min.ToString();
            TimeInString += ":" + ((sec < 10) ? "0" + sec.ToString() : sec.ToString());

            label.Text = TimeInString;
        }

        public void QuitOnTimer()
        {
            if (MessageBox.Show("The observation for " + m_fParent.m_oStudent.UUID + " is now complete. Do you need more time to edit your responses?",
                                "Are you finished?",
                                MessageBoxButtons.YesNo) == DialogResult.No)
            {
                Quit();
            }
        }

        public void RecordResults()
        {
            // === Save Results
            bool bValid = false;
            DialogResult res = DialogResult.Abort;

            // Get the focal input
            MPACFocalQuestion focalDialog = new MPACFocalQuestion();
            while (!bValid)
            {
                // keep showing the same window until we can verify the dialog was closed in an accepted manner.
                res = focalDialog.ShowDialog();
                bValid = (res == DialogResult.OK);
            }
            System.Console.WriteLine("Entered Focal Text: {0}", focalDialog.m_sFocalResponse);
            m_sFocalData = focalDialog.m_sFocalResponse;

            // Get the focal input
            MPACComments commentsDialog = new MPACComments();
            // keep showing the same window until we can verify the dialog was closed in an accepted manor.
            res = commentsDialog.ShowDialog();
            System.Console.WriteLine("Entered Focal Text: {0}", commentsDialog.m_sCommentResponse);
            m_sComments = commentsDialog.m_sCommentResponse;

            m_aRecordedData.Clear();
            m_aRecordedData.Add(GetArrayForCheckbox(check_0_0, 1));
            m_aRecordedData.Add(GetArrayForCheckbox(check_0_1, 2));
            m_aRecordedData.Add(GetArrayForCheckbox(check_0_2, 3));
            m_aRecordedData.Add(GetArrayForCheckbox(check_1_0, 4));
            m_aRecordedData.Add(GetArrayForCheckbox(check_1_1, 5));
            m_aRecordedData.Add(GetArrayForCheckbox(check_2_0, 6));
            m_aRecordedData.Add(GetArrayForCheckbox(check_2_1, 7));
            m_aRecordedData.Add(GetArrayForCheckbox(check_3_0, 8));
            m_aRecordedData.Add(GetArrayForCheckbox(check_3_1, 9));
            m_aRecordedData.Add(GetArrayForCheckbox(check_4_0, 10));
            m_aRecordedData.Add(GetArrayForCheckbox(check_4_1, 11));
            m_aRecordedData.Add(GetArrayForCheckbox(check_5_0, 12));
            m_aRecordedData.Add(GetArrayForCheckbox(check_5_1, 13));
            m_aRecordedData.Add(GetArrayForCheckbox(check_8_0, 14));
            m_aRecordedData.Add(GetArrayForCheckbox(check_8_1, 15));
            m_aRecordedData.Add(GetArrayForCheckbox(check_8_2, 16));
            m_aRecordedData.Add(GetArrayForCheckbox(check_6_0, 17));
            m_aRecordedData.Add(GetArrayForCheckbox(check_6_1, 18));
            m_aRecordedData.Add(GetArrayForCheckbox(check_7_0, 19));
            m_aRecordedData.Add(GetArrayForCheckbox(check_7_1, 20));

            System.IO.File.AppendAllText(GetFileNameForResultOutput(), GetStringForWrite());
        }
        public string[] GetArrayForCheckbox(PictureBox pb, int nIndex)
        {
            string sIndex = nIndex.ToString();
            string sValue = pb.Tag.Equals(Constants.Disabled) ? "0" : "1";
            string[] arr = { sIndex, sValue };
            Console.Out.WriteLine("GetArrayForCheckbox: ret={0}", string.Join(",", arr));
            return arr;
        }

        public string GetStringForWrite()
        {
            return m_fParent.m_oStudent.UUID + "," + DateTime.Now + "," + m_fParent.GetTimeMeasureInSeconds() + "," + GetStringFromDataArray(m_aRecordedData) + "," + m_sFocalData.Replace(Environment.NewLine, " ") + "," + m_sComments.Replace(Environment.NewLine, " ") + "\n";
        }

        private string GetStringFromDataArray(ArrayList ar)
        {
            string sReturn = "";
            string[][] aValues = (string[][])ar.ToArray(typeof(string[]));

            foreach (string[] str in aValues)
            {
                sReturn += string.Join(",", str);
                if (!aValues.Last().Equals(str))
                    sReturn += ",";
            }
            Console.Out.WriteLine("MPACMainWindow::GetStringFromDataArray: ret={0}", sReturn);
            return sReturn;
        }

        private string strFileNameForResults = "";
        private bool bNewFile;
        private string GetFileNameForResultOutput()
        {
            if (strFileNameForResults == "")
            {
                strFileNameForResults = m_fParent.m_oStudent.UUID + "_MPAC.csv";
                bool finishloop;
                do
                {
                    DialogResult res = MessageBox.Show("Is this observation the first for this student?", "Need some information", MessageBoxButtons.YesNo);
                    if (res == System.Windows.Forms.DialogResult.Yes)
                    {
                        bNewFile = true;
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.Filter = "Results files (*.csv)|*.csv|All files|*.*";
                        sfd.FileName = strFileNameForResults;
                        sfd.Title = "Choose the file where this observation will be stored";
                        //sfd.CheckFileExists = true;
                        finishloop = sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                        if (finishloop)
                            strFileNameForResults = sfd.FileName;
                    }
                    else
                    {
                        bNewFile = false;
                        OpenFileDialog ofd = new OpenFileDialog();
                        ofd.Filter = "Results files (*.csv)|*.csv|All files|*.*";
                        ofd.FileName = strFileNameForResults;
                        ofd.Title = "Choose the file where previous observations were stored; this current observation will be appended";
                        ofd.CheckFileExists = true;
                        finishloop = ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                        if (finishloop)
                            strFileNameForResults = ofd.FileName;
                    }
                } while (!finishloop);
            }
            return strFileNameForResults;
        }

        private int GetNumberOfRecordsInFileForUser(string sFileName, string sUser)
        {
            int nRecords = 0;

            try
            {
                if (System.IO.File.Exists(sFileName))
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(sFileName))
                    {
                        while (sr.Peek() >= 0)
                        {
                            string sLine = sr.ReadLine();
                            string[] sLineArr = sLine.Split(',');
                            if (sLineArr.Count() > 1)
                            {
                                if (sLineArr[1].Equals(sUser))
                                    nRecords++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

            return nRecords;
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            // toggle paused
            m_bPaused = !m_bPaused;

            // toggle checks
            check_0_0.Enabled = !m_bPaused;
            check_0_1.Enabled = !m_bPaused;
            check_0_2.Enabled = !m_bPaused;
            check_1_0.Enabled = !m_bPaused;
            check_1_1.Enabled = !m_bPaused;
            check_2_0.Enabled = !m_bPaused;
            check_2_1.Enabled = !m_bPaused;
            check_3_0.Enabled = !m_bPaused;
            check_3_1.Enabled = !m_bPaused;
            check_4_0.Enabled = !m_bPaused;
            check_4_1.Enabled = !m_bPaused;
            check_5_0.Enabled = !m_bPaused;
            check_5_1.Enabled = !m_bPaused;
            check_8_0.Enabled = !m_bPaused;
            check_8_1.Enabled = !m_bPaused;
            check_8_2.Enabled = !m_bPaused;
            check_6_0.Enabled = !m_bPaused;
            check_6_1.Enabled = !m_bPaused;
            check_7_0.Enabled = !m_bPaused;
            check_7_1.Enabled = !m_bPaused;

            // update text label
            if (m_bPaused)
                pauseButton.Text = "Resume";
            else
                pauseButton.Text = "Pause";
        }

        void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {
            TabControl tc = (TabControl)sender;
            if (m_bPaused)
                // if paused, prevents changing tabs
                tc.SelectedIndex = m_nCurrentTabIndex;
            else
                // if not paused, updated 'cur tab'
                m_nCurrentTabIndex = tc.SelectedIndex;
        }

    }
}
