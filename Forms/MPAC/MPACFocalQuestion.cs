﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Forms.MPAC
{
    public partial class MPACFocalQuestion : Form
    {

        public string m_sFocalResponse;

        public MPACFocalQuestion()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();
#if(DEBUG)
#else
            this.TopMost = true;
#endif
            m_sFocalResponse = "";
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textArea.Text))
            {
                // valid entry we assume
                m_sFocalResponse = textArea.Text;
                Dismiss();
            }
            else
            {
                // no entry, which is required to proceed.
                MessageBox.Show("Please provide input to proceed.",
                                "Required Input Missing",
                                MessageBoxButtons.OK);
            }
        }

        private void Dismiss()
        {
            DialogResult = DialogResult.OK;
            Cursor.Show();
            this.Close();
        }

    }
}
