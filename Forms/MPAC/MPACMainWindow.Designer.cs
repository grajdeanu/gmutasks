﻿namespace GMUTasks.Forms.MPAC
{
    partial class MPACMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MPACMainWindow));
            this.ButtonClose = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.check_7_1 = new System.Windows.Forms.PictureBox();
            this.label_7_1 = new System.Windows.Forms.Label();
            this.check_7_0 = new System.Windows.Forms.PictureBox();
            this.label_7_0 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.check_6_1 = new System.Windows.Forms.PictureBox();
            this.label_6_1 = new System.Windows.Forms.Label();
            this.check_6_0 = new System.Windows.Forms.PictureBox();
            this.label_6_0 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.check_5_1 = new System.Windows.Forms.PictureBox();
            this.label_5_1 = new System.Windows.Forms.Label();
            this.check_5_0 = new System.Windows.Forms.PictureBox();
            this.label_5_0 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.check_4_1 = new System.Windows.Forms.PictureBox();
            this.label_4_1 = new System.Windows.Forms.Label();
            this.check_4_0 = new System.Windows.Forms.PictureBox();
            this.label_4_0 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.check_3_1 = new System.Windows.Forms.PictureBox();
            this.label_3_1 = new System.Windows.Forms.Label();
            this.check_3_0 = new System.Windows.Forms.PictureBox();
            this.label_3_0 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.check_2_1 = new System.Windows.Forms.PictureBox();
            this.label_2_1 = new System.Windows.Forms.Label();
            this.check_2_0 = new System.Windows.Forms.PictureBox();
            this.label_2_0 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.check_1_1 = new System.Windows.Forms.PictureBox();
            this.label_1_1 = new System.Windows.Forms.Label();
            this.check_1_0 = new System.Windows.Forms.PictureBox();
            this.label_1_0 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel0 = new System.Windows.Forms.TableLayoutPanel();
            this.check_0_2 = new System.Windows.Forms.PictureBox();
            this.label_0_2 = new System.Windows.Forms.Label();
            this.check_0_1 = new System.Windows.Forms.PictureBox();
            this.label_0_1 = new System.Windows.Forms.Label();
            this.check_0_0 = new System.Windows.Forms.PictureBox();
            this.label_0_0 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.check_8_2 = new System.Windows.Forms.PictureBox();
            this.label_8_2 = new System.Windows.Forms.Label();
            this.check_8_1 = new System.Windows.Forms.PictureBox();
            this.label_8_1 = new System.Windows.Forms.Label();
            this.check_8_0 = new System.Windows.Forms.PictureBox();
            this.label_8_0 = new System.Windows.Forms.Label();
            this.labelTimeRemaining = new System.Windows.Forms.Label();
            this.labelTimer = new System.Windows.Forms.Label();
            this.labelObservations = new System.Windows.Forms.Label();
            this.pauseButton = new System.Windows.Forms.Button();
            this.tabPage8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_7_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_7_0)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_6_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_6_0)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_5_0)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_4_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_4_0)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_3_0)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_2_0)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_1_0)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_0_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_0_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_0_0)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_8_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_8_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_8_0)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonClose
            // 
            this.ButtonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonClose.Location = new System.Drawing.Point(1116, 761);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(152, 27);
            this.ButtonClose.TabIndex = 2;
            this.ButtonClose.Text = "Finished";
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.tableLayoutPanel7);
            this.tabPage8.Location = new System.Drawing.Point(4, 74);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(1247, 668);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Peer Skills";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel7.Controls.Add(this.check_7_1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label_7_1, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.check_7_0, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label_7_0, 1, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel7.TabIndex = 2;
            // 
            // check_7_1
            // 
            this.check_7_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_7_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_7_1.Location = new System.Drawing.Point(42, 439);
            this.check_7_1.Name = "check_7_1";
            this.check_7_1.Size = new System.Drawing.Size(101, 101);
            this.check_7_1.TabIndex = 2;
            this.check_7_1.TabStop = false;
            this.check_7_1.Tag = "disabled";
            // 
            // label_7_1
            // 
            this.label_7_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_7_1.AutoSize = true;
            this.label_7_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_7_1.Location = new System.Drawing.Point(188, 435);
            this.label_7_1.Name = "label_7_1";
            this.label_7_1.Size = new System.Drawing.Size(948, 108);
            this.label_7_1.TabIndex = 3;
            this.label_7_1.Text = "The child smoothly approaches an already ongoing activity and gets actively invol" +
    "ved. The child does not disrupt or antagonize other children as he/she approache" +
    "s the activity.";
            this.label_7_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_7_0
            // 
            this.check_7_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_7_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_7_0.Location = new System.Drawing.Point(42, 112);
            this.check_7_0.Name = "check_7_0";
            this.check_7_0.Size = new System.Drawing.Size(101, 101);
            this.check_7_0.TabIndex = 0;
            this.check_7_0.TabStop = false;
            this.check_7_0.Tag = "disabled";
            // 
            // label_7_0
            // 
            this.label_7_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_7_0.AutoSize = true;
            this.label_7_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_7_0.Location = new System.Drawing.Point(188, 109);
            this.label_7_0.Name = "label_7_0";
            this.label_7_0.Size = new System.Drawing.Size(974, 108);
            this.label_7_0.TabIndex = 1;
            this.label_7_0.Text = resources.GetString("label_7_0.Text");
            this.label_7_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.tableLayoutPanel6);
            this.tabPage7.Location = new System.Drawing.Point(4, 74);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1247, 668);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Unusual";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel6.Controls.Add(this.check_6_1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label_6_1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.check_6_0, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label_6_0, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // check_6_1
            // 
            this.check_6_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_6_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_6_1.Location = new System.Drawing.Point(42, 439);
            this.check_6_1.Name = "check_6_1";
            this.check_6_1.Size = new System.Drawing.Size(101, 101);
            this.check_6_1.TabIndex = 2;
            this.check_6_1.TabStop = false;
            this.check_6_1.Tag = "disabled";
            // 
            // label_6_1
            // 
            this.label_6_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_6_1.AutoSize = true;
            this.label_6_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_6_1.Location = new System.Drawing.Point(188, 453);
            this.label_6_1.Name = "label_6_1";
            this.label_6_1.Size = new System.Drawing.Size(1032, 72);
            this.label_6_1.TabIndex = 3;
            this.label_6_1.Text = "The child displays unprovoked physical interpersonal aggression with no preceding" +
    " provocation by the victim.";
            this.label_6_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_6_0
            // 
            this.check_6_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_6_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_6_0.Location = new System.Drawing.Point(42, 112);
            this.check_6_0.Name = "check_6_0";
            this.check_6_0.Size = new System.Drawing.Size(101, 101);
            this.check_6_0.TabIndex = 0;
            this.check_6_0.TabStop = false;
            this.check_6_0.Tag = "disabled";
            // 
            // label_6_0
            // 
            this.label_6_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_6_0.AutoSize = true;
            this.label_6_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_6_0.Location = new System.Drawing.Point(188, 145);
            this.label_6_0.Name = "label_6_0";
            this.label_6_0.Size = new System.Drawing.Size(964, 36);
            this.label_6_0.TabIndex = 1;
            this.label_6_0.Text = "The child engages in no social interaction continuously for 3 minutes or more.";
            this.label_6_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel5);
            this.tabPage6.Location = new System.Drawing.Point(4, 74);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1247, 668);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Positive to Frust.";
            this.tabPage6.ToolTipText = "Positive to Frustration";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel5.Controls.Add(this.check_5_1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label_5_1, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.check_5_0, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label_5_0, 1, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // check_5_1
            // 
            this.check_5_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_5_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_5_1.Location = new System.Drawing.Point(42, 439);
            this.check_5_1.Name = "check_5_1";
            this.check_5_1.Size = new System.Drawing.Size(101, 101);
            this.check_5_1.TabIndex = 2;
            this.check_5_1.TabStop = false;
            this.check_5_1.Tag = "disabled";
            // 
            // label_5_1
            // 
            this.label_5_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_5_1.AutoSize = true;
            this.label_5_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_5_1.Location = new System.Drawing.Point(188, 471);
            this.label_5_1.Name = "label_5_1";
            this.label_5_1.Size = new System.Drawing.Size(639, 36);
            this.label_5_1.TabIndex = 3;
            this.label_5_1.Text = "The child shows primarily neutral or positive affect.";
            this.label_5_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_5_0
            // 
            this.check_5_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_5_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_5_0.Location = new System.Drawing.Point(42, 112);
            this.check_5_0.Name = "check_5_0";
            this.check_5_0.Size = new System.Drawing.Size(101, 101);
            this.check_5_0.TabIndex = 0;
            this.check_5_0.TabStop = false;
            this.check_5_0.Tag = "disabled";
            // 
            // label_5_0
            // 
            this.label_5_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_5_0.AutoSize = true;
            this.label_5_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_5_0.Location = new System.Drawing.Point(188, 109);
            this.label_5_0.Name = "label_5_0";
            this.label_5_0.Size = new System.Drawing.Size(1006, 108);
            this.label_5_0.TabIndex = 1;
            this.label_5_0.Text = resources.GetString("label_5_0.Text");
            this.label_5_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel4);
            this.tabPage5.Location = new System.Drawing.Point(4, 74);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1247, 668);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Negative to Frust.";
            this.tabPage5.ToolTipText = "Negative to Frustration";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel4.Controls.Add(this.check_4_1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label_4_1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.check_4_0, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label_4_0, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // check_4_1
            // 
            this.check_4_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_4_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_4_1.Location = new System.Drawing.Point(42, 439);
            this.check_4_1.Name = "check_4_1";
            this.check_4_1.Size = new System.Drawing.Size(101, 101);
            this.check_4_1.TabIndex = 2;
            this.check_4_1.TabStop = false;
            this.check_4_1.Tag = "disabled";
            // 
            // label_4_1
            // 
            this.label_4_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_4_1.AutoSize = true;
            this.label_4_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_4_1.Location = new System.Drawing.Point(188, 453);
            this.label_4_1.Name = "label_4_1";
            this.label_4_1.Size = new System.Drawing.Size(1004, 72);
            this.label_4_1.TabIndex = 3;
            this.label_4_1.Text = "Object aggression: The child hits, kicks, shoves, knocks over, or throws objects " +
    "in response to an emotionally arousing problem situation. ";
            this.label_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_4_0
            // 
            this.check_4_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_4_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_4_0.Location = new System.Drawing.Point(42, 112);
            this.check_4_0.Name = "check_4_0";
            this.check_4_0.Size = new System.Drawing.Size(101, 101);
            this.check_4_0.TabIndex = 0;
            this.check_4_0.TabStop = false;
            this.check_4_0.Tag = "disabled";
            // 
            // label_4_0
            // 
            this.label_4_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_4_0.AutoSize = true;
            this.label_4_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_4_0.Location = new System.Drawing.Point(188, 109);
            this.label_4_0.Name = "label_4_0";
            this.label_4_0.Size = new System.Drawing.Size(984, 108);
            this.label_4_0.TabIndex = 1;
            this.label_4_0.Text = resources.GetString("label_4_0.Text");
            this.label_4_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel3);
            this.tabPage4.Location = new System.Drawing.Point(4, 74);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1247, 668);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Unproductive";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel3.Controls.Add(this.check_3_1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_3_1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.check_3_0, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label_3_0, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // check_3_1
            // 
            this.check_3_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_3_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_3_1.Location = new System.Drawing.Point(42, 439);
            this.check_3_1.Name = "check_3_1";
            this.check_3_1.Size = new System.Drawing.Size(101, 101);
            this.check_3_1.TabIndex = 2;
            this.check_3_1.TabStop = false;
            this.check_3_1.Tag = "disabled";
            // 
            // label_3_1
            // 
            this.label_3_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_3_1.AutoSize = true;
            this.label_3_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_3_1.Location = new System.Drawing.Point(188, 435);
            this.label_3_1.Name = "label_3_1";
            this.label_3_1.Size = new System.Drawing.Size(1041, 108);
            this.label_3_1.TabIndex = 3;
            this.label_3_1.Text = "Listless: The child looks fidgety and uninvested in the activity but still “emoti" +
    "onally present;” the child stays in one area but shows little/no involvement in " +
    "activities or social interaction.";
            this.label_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_3_0
            // 
            this.check_3_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_3_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_3_0.Location = new System.Drawing.Point(42, 112);
            this.check_3_0.Name = "check_3_0";
            this.check_3_0.Size = new System.Drawing.Size(101, 101);
            this.check_3_0.TabIndex = 0;
            this.check_3_0.TabStop = false;
            this.check_3_0.Tag = "disabled";
            // 
            // label_3_0
            // 
            this.label_3_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_3_0.AutoSize = true;
            this.label_3_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_3_0.Location = new System.Drawing.Point(188, 127);
            this.label_3_0.Name = "label_3_0";
            this.label_3_0.Size = new System.Drawing.Size(952, 72);
            this.label_3_0.TabIndex = 1;
            this.label_3_0.Text = "Vacant: The child displays a very flat, unexpressive, detached face; shows no inv" +
    "olvement in an activity; and looks \"emotionally absent.\"";
            this.label_3_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 74);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1247, 668);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Productive";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel2.Controls.Add(this.check_2_1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label_2_1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.check_2_0, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_2_0, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // check_2_1
            // 
            this.check_2_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_2_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_2_1.Location = new System.Drawing.Point(42, 439);
            this.check_2_1.Name = "check_2_1";
            this.check_2_1.Size = new System.Drawing.Size(101, 101);
            this.check_2_1.TabIndex = 2;
            this.check_2_1.TabStop = false;
            this.check_2_1.Tag = "disabled";
            // 
            // label_2_1
            // 
            this.label_2_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_2_1.AutoSize = true;
            this.label_2_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_2_1.Location = new System.Drawing.Point(188, 399);
            this.label_2_1.Name = "label_2_1";
            this.label_2_1.Size = new System.Drawing.Size(1000, 180);
            this.label_2_1.TabIndex = 3;
            this.label_2_1.Text = resources.GetString("label_2_1.Text");
            this.label_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_2_0
            // 
            this.check_2_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_2_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_2_0.Location = new System.Drawing.Point(42, 112);
            this.check_2_0.Name = "check_2_0";
            this.check_2_0.Size = new System.Drawing.Size(101, 101);
            this.check_2_0.TabIndex = 0;
            this.check_2_0.TabStop = false;
            this.check_2_0.Tag = "disabled";
            // 
            // label_2_0
            // 
            this.label_2_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_2_0.AutoSize = true;
            this.label_2_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_2_0.Location = new System.Drawing.Point(188, 109);
            this.label_2_0.Name = "label_2_0";
            this.label_2_0.Size = new System.Drawing.Size(964, 108);
            this.label_2_0.TabIndex = 1;
            this.label_2_0.Text = resources.GetString("label_2_0.Text");
            this.label_2_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 74);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1247, 668);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Negative Emotion";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel1.Controls.Add(this.check_1_1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_1_1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.check_1_0, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_1_0, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // check_1_1
            // 
            this.check_1_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_1_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_1_1.Location = new System.Drawing.Point(42, 439);
            this.check_1_1.Name = "check_1_1";
            this.check_1_1.Size = new System.Drawing.Size(101, 101);
            this.check_1_1.TabIndex = 2;
            this.check_1_1.TabStop = false;
            this.check_1_1.Tag = "disabled";
            // 
            // label_1_1
            // 
            this.label_1_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_1_1.AutoSize = true;
            this.label_1_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_1_1.Location = new System.Drawing.Point(188, 417);
            this.label_1_1.Name = "label_1_1";
            this.label_1_1.Size = new System.Drawing.Size(1031, 144);
            this.label_1_1.TabIndex = 3;
            this.label_1_1.Text = resources.GetString("label_1_1.Text");
            this.label_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_1_0
            // 
            this.check_1_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_1_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_1_0.Location = new System.Drawing.Point(42, 112);
            this.check_1_0.Name = "check_1_0";
            this.check_1_0.Size = new System.Drawing.Size(101, 101);
            this.check_1_0.TabIndex = 0;
            this.check_1_0.TabStop = false;
            this.check_1_0.Tag = "disabled";
            // 
            // label_1_0
            // 
            this.label_1_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_1_0.AutoSize = true;
            this.label_1_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_1_0.Location = new System.Drawing.Point(188, 109);
            this.label_1_0.Name = "label_1_0";
            this.label_1_0.Size = new System.Drawing.Size(1037, 108);
            this.label_1_0.TabIndex = 1;
            this.label_1_0.Text = resources.GetString("label_1_0.Text");
            this.label_1_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage1
            // 
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage1.Controls.Add(this.tableLayoutPanel0);
            this.tabPage1.Location = new System.Drawing.Point(4, 74);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1247, 668);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Positive Emotion";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel0
            // 
            this.tableLayoutPanel0.ColumnCount = 2;
            this.tableLayoutPanel0.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel0.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel0.Controls.Add(this.check_0_2, 0, 2);
            this.tableLayoutPanel0.Controls.Add(this.label_0_2, 0, 2);
            this.tableLayoutPanel0.Controls.Add(this.check_0_1, 0, 1);
            this.tableLayoutPanel0.Controls.Add(this.label_0_1, 1, 1);
            this.tableLayoutPanel0.Controls.Add(this.check_0_0, 0, 0);
            this.tableLayoutPanel0.Controls.Add(this.label_0_0, 1, 0);
            this.tableLayoutPanel0.Location = new System.Drawing.Point(7, 7);
            this.tableLayoutPanel0.Name = "tableLayoutPanel0";
            this.tableLayoutPanel0.RowCount = 3;
            this.tableLayoutPanel0.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel0.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel0.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel0.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel0.TabIndex = 0;
            // 
            // check_0_2
            // 
            this.check_0_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_0_2.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_0_2.Location = new System.Drawing.Point(42, 493);
            this.check_0_2.Name = "check_0_2";
            this.check_0_2.Size = new System.Drawing.Size(101, 101);
            this.check_0_2.TabIndex = 4;
            this.check_0_2.TabStop = false;
            this.check_0_2.Tag = "disabled";
            // 
            // label_0_2
            // 
            this.label_0_2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_0_2.AutoSize = true;
            this.label_0_2.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_0_2.Location = new System.Drawing.Point(188, 507);
            this.label_0_2.Name = "label_0_2";
            this.label_0_2.Size = new System.Drawing.Size(1029, 72);
            this.label_0_2.TabIndex = 5;
            this.label_0_2.Text = "The child displays positive affect when in a social situation but does not direct" +
    " it to anyone in particular.";
            this.label_0_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_0_1
            // 
            this.check_0_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_0_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_0_1.Location = new System.Drawing.Point(42, 275);
            this.check_0_1.Name = "check_0_1";
            this.check_0_1.Size = new System.Drawing.Size(101, 101);
            this.check_0_1.TabIndex = 2;
            this.check_0_1.TabStop = false;
            this.check_0_1.Tag = "disabled";
            // 
            // label_0_1
            // 
            this.label_0_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_0_1.AutoSize = true;
            this.label_0_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_0_1.Location = new System.Drawing.Point(188, 253);
            this.label_0_1.Name = "label_0_1";
            this.label_0_1.Size = new System.Drawing.Size(1029, 144);
            this.label_0_1.TabIndex = 3;
            this.label_0_1.Text = resources.GetString("label_0_1.Text");
            this.label_0_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_0_0
            // 
            this.check_0_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_0_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_0_0.Location = new System.Drawing.Point(42, 58);
            this.check_0_0.Name = "check_0_0";
            this.check_0_0.Size = new System.Drawing.Size(101, 101);
            this.check_0_0.TabIndex = 0;
            this.check_0_0.TabStop = false;
            this.check_0_0.Tag = "disabled";
            // 
            // label_0_0
            // 
            this.label_0_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_0_0.AutoSize = true;
            this.label_0_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_0_0.Location = new System.Drawing.Point(188, 36);
            this.label_0_0.Name = "label_0_0";
            this.label_0_0.Size = new System.Drawing.Size(1018, 144);
            this.label_0_0.TabIndex = 1;
            this.label_0_0.Text = resources.GetString("label_0_0.Text");
            this.label_0_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(12, 24);
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1255, 746);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tableLayoutPanel8);
            this.tabPage9.Location = new System.Drawing.Point(4, 74);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(1247, 668);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Empathy";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0974F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.9026F));
            this.tableLayoutPanel8.Controls.Add(this.check_8_2, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label_8_2, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.check_8_1, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label_8_1, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.check_8_0, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label_8_0, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(7, 8);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1232, 653);
            this.tableLayoutPanel8.TabIndex = 3;
            // 
            // check_8_2
            // 
            this.check_8_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_8_2.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_8_2.Location = new System.Drawing.Point(42, 493);
            this.check_8_2.Name = "check_8_2";
            this.check_8_2.Size = new System.Drawing.Size(101, 101);
            this.check_8_2.TabIndex = 4;
            this.check_8_2.TabStop = false;
            this.check_8_2.Tag = "disabled";
            // 
            // label_8_2
            // 
            this.label_8_2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_8_2.AutoSize = true;
            this.label_8_2.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_8_2.Location = new System.Drawing.Point(188, 525);
            this.label_8_2.Name = "label_8_2";
            this.label_8_2.Size = new System.Drawing.Size(964, 36);
            this.label_8_2.TabIndex = 5;
            this.label_8_2.Text = "The child shares toys or other materials (e.g., crayon, pencil, play dough, etc.)" +
    ".";
            this.label_8_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_8_1
            // 
            this.check_8_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_8_1.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_8_1.Location = new System.Drawing.Point(42, 275);
            this.check_8_1.Name = "check_8_1";
            this.check_8_1.Size = new System.Drawing.Size(101, 101);
            this.check_8_1.TabIndex = 2;
            this.check_8_1.TabStop = false;
            this.check_8_1.Tag = "disabled";
            // 
            // label_8_1
            // 
            this.label_8_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_8_1.AutoSize = true;
            this.label_8_1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_8_1.Location = new System.Drawing.Point(188, 289);
            this.label_8_1.Name = "label_8_1";
            this.label_8_1.Size = new System.Drawing.Size(979, 72);
            this.label_8_1.TabIndex = 3;
            this.label_8_1.Text = "Cooperating: The child jointly works with a peer or group of peers to achieve a c" +
    "ommon goal.";
            this.label_8_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // check_8_0
            // 
            this.check_8_0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.check_8_0.Image = global::GMUTasks.Properties.Resources.check_empty;
            this.check_8_0.Location = new System.Drawing.Point(42, 58);
            this.check_8_0.Name = "check_8_0";
            this.check_8_0.Size = new System.Drawing.Size(101, 101);
            this.check_8_0.TabIndex = 0;
            this.check_8_0.TabStop = false;
            this.check_8_0.Tag = "disabled";
            // 
            // label_8_0
            // 
            this.label_8_0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_8_0.AutoSize = true;
            this.label_8_0.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_8_0.Location = new System.Drawing.Point(188, 54);
            this.label_8_0.Name = "label_8_0";
            this.label_8_0.Size = new System.Drawing.Size(1041, 108);
            this.label_8_0.TabIndex = 1;
            this.label_8_0.Text = "Taking turns: The child plays with a toy or participates in an activity and then " +
    "allows another to do the same. A clear beginning and end of each child’s turn du" +
    "ring an activity must be observed.";
            this.label_8_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTimeRemaining
            // 
            this.labelTimeRemaining.AutoSize = true;
            this.labelTimeRemaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeRemaining.Location = new System.Drawing.Point(12, 762);
            this.labelTimeRemaining.Name = "labelTimeRemaining";
            this.labelTimeRemaining.Size = new System.Drawing.Size(212, 29);
            this.labelTimeRemaining.TabIndex = 4;
            this.labelTimeRemaining.Text = "Time Remaining:";
            // 
            // labelTimer
            // 
            this.labelTimer.AutoSize = true;
            this.labelTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimer.Location = new System.Drawing.Point(230, 762);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Size = new System.Drawing.Size(62, 29);
            this.labelTimer.TabIndex = 5;
            this.labelTimer.Text = "0:00";
            // 
            // labelObservations
            // 
            this.labelObservations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelObservations.AutoSize = true;
            this.labelObservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelObservations.Location = new System.Drawing.Point(806, 764);
            this.labelObservations.Name = "labelObservations";
            this.labelObservations.Size = new System.Drawing.Size(146, 24);
            this.labelObservations.TabIndex = 6;
            this.labelObservations.Text = "Observation: 0/5";
            // 
            // pauseButton
            // 
            this.pauseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseButton.Location = new System.Drawing.Point(958, 761);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(152, 27);
            this.pauseButton.TabIndex = 7;
            this.pauseButton.Text = "Pause";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // MPACMainWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.ControlBox = false;
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.labelObservations);
            this.Controls.Add(this.labelTimer);
            this.Controls.Add(this.labelTimeRemaining);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ButtonClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MPACMainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MPACMainWindow";
            this.TopMost = true;
            this.tabPage8.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_7_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_7_0)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_6_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_6_0)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_5_0)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_4_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_4_0)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_3_0)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_2_0)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_1_0)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel0.ResumeLayout(false);
            this.tableLayoutPanel0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_0_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_0_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_0_0)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_8_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_8_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_8_0)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel0;
        private System.Windows.Forms.PictureBox check_0_2;
        private System.Windows.Forms.Label label_0_2;
        private System.Windows.Forms.PictureBox check_0_1;
        private System.Windows.Forms.Label label_0_1;
        private System.Windows.Forms.PictureBox check_0_0;
        private System.Windows.Forms.Label label_0_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox check_1_1;
        private System.Windows.Forms.Label label_1_1;
        private System.Windows.Forms.PictureBox check_1_0;
        private System.Windows.Forms.Label label_1_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox check_2_1;
        private System.Windows.Forms.Label label_2_1;
        private System.Windows.Forms.PictureBox check_2_0;
        private System.Windows.Forms.Label label_2_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox check_3_1;
        private System.Windows.Forms.Label label_3_1;
        private System.Windows.Forms.PictureBox check_3_0;
        private System.Windows.Forms.Label label_3_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox check_4_1;
        private System.Windows.Forms.Label label_4_1;
        private System.Windows.Forms.PictureBox check_4_0;
        private System.Windows.Forms.Label label_4_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.PictureBox check_5_1;
        private System.Windows.Forms.Label label_5_1;
        private System.Windows.Forms.PictureBox check_5_0;
        private System.Windows.Forms.Label label_5_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.PictureBox check_6_0;
        private System.Windows.Forms.Label label_6_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.PictureBox check_7_1;
        private System.Windows.Forms.Label label_7_1;
        private System.Windows.Forms.PictureBox check_7_0;
        private System.Windows.Forms.Label label_7_0;
        private System.Windows.Forms.Label labelTimeRemaining;
        private System.Windows.Forms.Label labelTimer;
        private System.Windows.Forms.Label labelObservations;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.PictureBox check_6_1;
        private System.Windows.Forms.Label label_6_1;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.PictureBox check_8_2;
        private System.Windows.Forms.Label label_8_2;
        private System.Windows.Forms.PictureBox check_8_1;
        private System.Windows.Forms.Label label_8_1;
        private System.Windows.Forms.PictureBox check_8_0;
        private System.Windows.Forms.Label label_8_0;

    }
}