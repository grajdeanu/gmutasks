﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Forms.MPAC
{
    public partial class MPACComments : Form
    {

        public string m_sCommentResponse;

        public MPACComments()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();
#if(DEBUG)
#else
            this.TopMost = true;
#endif

            m_sCommentResponse = "";
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            // we assume any entry is valid, as this is optional
            m_sCommentResponse = textArea.Text;
            Dismiss();
        }

        private void Dismiss()
        {
            DialogResult = DialogResult.OK;
            Cursor.Show();
            this.Close();
        }

    }
}
