﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.DirectX.AudioVideoPlayback;
using GMUTasks.Classes;

namespace GMUTasks.Forms.CST
{

    public partial class CSTMainWindow : Form
    {
        // Public
        public TaskType GetTaskType()
        {
            return TaskType.CST;
        }
        
        private CSTTask   m_fParent;
        private ArrayList m_aRecordedData;
        private ArrayList m_aTempData;
        private Audio     m_audio;

        private int[]     m_aCurrentScenarioOrder = { -1, -1, -1, -1, -1, -1 }; // To track randomization of scenarios
        private int[]     m_aCurrentFaceOrder     = { -1, -1, -1, -1 };         // To track randomization of faces display
        private int[]     m_aCurrentChoiceOrder   = { -1, -1, -1, -1 };         // To track randomization of choices display
        private int[]     m_aCurrentTrainingOrder = { -1, -1, -1, -1 };         // To track randomization of training display

        public delegate void changeImageInBox(Bitmap imageName, System.Windows.Forms.PictureBox imageBox);
        public delegate void toggleImageEnabled(Boolean bEnabled, System.Windows.Forms.PictureBox imageBox);
        public delegate void toggleImageVisible(Boolean bVisible, System.Windows.Forms.PictureBox imageBox);
        public delegate void deselectImageBox(Boolean bEnabled, System.Windows.Forms.PictureBox imageBox);
        public delegate void toggleProgressionVisible(Boolean bEnabled);
        public delegate void updateProgressionLabel(int nCount);
        private delegate void closeMyself();

        public Bitmap[] CSTImageTraining = { Properties.Resources.Crayons, Properties.Resources.RubberDuck,
                                             Properties.Resources.Teddybear, Properties.Resources.WoodBocks};
        public Bitmap[] CSTImageFaces       = {Properties.Resources.CSTTask_Faces_Happy_,
                                               Properties.Resources.CSTTask_Faces_Mad_,
                                               Properties.Resources.CSTTask_Faces_OK_,
                                               Properties.Resources.CSTTask_Faces_Sad_};

        public Bitmap[][] CSTImageCatalysts = new Bitmap[][]
        {
            new Bitmap[] {Properties.Resources.CSTTask_Blocks_Catalyst___Knocks_over_tower,
                          Properties.Resources.CSTTask_Sandbox_Catalyst___Gets_hit,
                          Properties.Resources.CSTTask_SoccerBall_Catalyst___Steals_ball,
                          Properties.Resources.CSTTask_WontPlay_Catalyst___Bobby_says_he_doesnt_want_to_play,
                          Properties.Resources.CSTTask_Picture_Catalyst___Bobby_says_your_picture_is_ugly,
                          Properties.Resources.CSTTask_Doll_Catalyst___Bobby_calls_you_a_baby},
            new Bitmap[] {Properties.Resources.CSTTask_Airplane_Catalyst___Draws_on_picture,
                          Properties.Resources.CSTTask_Swing_Catalyst___Cuts_in_line,
                          Properties.Resources.CSTTask_Car_Catalyst___Bobby_took_the_toy_car,
                          Properties.Resources.CSTTask_Birthday_Catalyst___Bobby_says_you_cant_goto_birthday,
                          Properties.Resources.CSTTask_KickBall_Catalyst___You_fell_and_Bobby_laughed,
                          Properties.Resources.CSTTask_SuckThumb_Catalyst___Bobby_saw_you_suck_your_thumb_during_nap}
        };

        public Bitmap[][][] CSTImageResponses = new Bitmap[][][]
        {
            new Bitmap[][]
            {
                // Version A
                new Bitmap[] {Properties.Resources.CSTTask_Blocks_A__Build_Another_Tower,
                              Properties.Resources.CSTTask_Blocks_B__Hit_Yell_at_him,
                              Properties.Resources.CSTTask_Blocks_C__Cry,
                              Properties.Resources.CSTTask_Blocks_D__Play_with_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_Sandbox_A__Tell_Him_thats_not_nice,
                              Properties.Resources.CSTTask_Sandbox_B__Hit_Him,
                              Properties.Resources.CSTTask_Sandbox_C__Cry,
                              Properties.Resources.CSTTask_Sandbox_D__Go_Play_Somewhere_Else},
                new Bitmap[] {Properties.Resources.CSTTask_SoccerBall_A__Ask_to_play_with_him,
                              Properties.Resources.CSTTask_SoccerBall_B__Steal_ball_back,
                              Properties.Resources.CSTTask_SoccerBall_C__Cry,
                              Properties.Resources.CSTTask_SoccerBall_D__Play_with_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_WontPlay_B_Ask_to_play_with_Tom_too,
                              Properties.Resources.CSTTask_WontPlay_A_Push_Bobby_and_tell_him_youre_not_his_friend,
                              Properties.Resources.CSTTask_WontPlay_C_Cry,
                              Properties.Resources.CSTTask_WontPlay_Stop_drawing_an_do_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_Picture_B_Say_thats_OK_you_like_your_picture,
                              Properties.Resources.CSTTask_Picture_A_Hit_Bobby_and_yell_at_him,
                              Properties.Resources.CSTTask_Picture_C_Cry,
                              Properties.Resources.CSTTask_Picture_D_Do_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_Doll_B_Bring_a_toy_for_Bobby_to_nap_with,
                              Properties.Resources.CSTTask_Doll_A_Call_Bobby_a_baby_or_hit_him,
                              Properties.Resources.CSTTask_Doll_C_Cry,
                              Properties.Resources.CSTTask_Doll_D_Ignore_Bobby}
            },
            new Bitmap[][]
            {
                // Version B
                new Bitmap[] {Properties.Resources.CSTTask_Airplane_B_Ask_to_draw_another_picture,
                              Properties.Resources.CSTTask_Airplane_A_Hit_or_yell_at_him,
                              Properties.Resources.CSTTask_Airplane_C_Cry,
                              Properties.Resources.CSTTask_Airplane_D_Do_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_Swing_B_Tell_Bobby_this_is_your_spot,
                              Properties.Resources.CSTTask_Swing_A_Push_Bobby_back,
                              Properties.Resources.CSTTask_Swing_C_Cry,
                              Properties.Resources.CSTTask_Swing_D_Go_do_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_Car_B_Ask_Bobby_if_he_wants_to_play_together,
                              Properties.Resources.CSTTask_Car_A_Grab_the_car_back_or_yell,
                              Properties.Resources.CSTTask_Car_C_Cry,
                              Properties.Resources.CSTTask_Car_D_Play_with_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_Birthday_B_Tell_Bobby_he_can_come_to_your_birthday_party,
                              Properties.Resources.CSTTask_Birthday_A_Push_Bobby_and_say_fine_then,
                              Properties.Resources.CSTTask_Birthday_C_Cry,
                              Properties.Resources.CSTTask_Birthday_D_Do_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_KickBall_B_Try_Again,
                              Properties.Resources.CSTTask_KickBall_A_Throw_ball_at_Bobbys_face,
                              Properties.Resources.CSTTask_KickBall_C_Cry,
                              Properties.Resources.CSTTask_KickBall_D_Play_something_else},
                new Bitmap[] {Properties.Resources.CSTTask_SuckThumb_B_Say_its_OK_to_suck_your_thunk_during_nap,
                              Properties.Resources.CSTTask_SuckThumb_A_Tell_Bobby_hes_a_baby_or_hit_him,
                              Properties.Resources.CSTTask_SuckThumb_C_Cry,
                              Properties.Resources.CSTTask_SuckThumb_D_Ignore_Bobby}
            }
        };

        // Private

        // Duration marks at which a choice is mentioned in the corrosponding audio
        // [Version][CurrentScenario][ChoiceIndex]
        public int[][][] CSTChoiceDurations = new int[][][] 
        {
            new int[][]
            {
                // Version A
                new int[] {2, 2, 3, 2},   // Blocks
                new int[] {2, 2, 2, 2},   // Sandbox
                new int[] {2, 2, 2, 2},   // SoccerBall
                new int[] {2, 2, 2, 2},   // WontPlay
                new int[] {3, 2, 2, 3},   // Picture
                new int[] {1, 3, 2, 2}    // Doll
            },
            new int[][]
            {
                // Version B
                new int[] {2, 2, 2, 2},   // Airplane
                new int[] {2, 2, 2, 2},   // Swing
                new int[] {2, 2, 2, 2},   // Car
                new int[] {3, 4, 2, 2},   // Birthday
                new int[] {4, 2, 2, 2},   // KickBall
                new int[] {3, 3, 2, 2}    // SuckThumb
            }
        };

        // Duration of the audio files 
        // [Gender][Version][CurrentScenario]
        public int[][][] CSTCatalystDuration = new int[][][]
        {
            // Male
            new int[][]
            {
                // Version A
                new int[] {
                    4,  // Blocks
                    4,  // Sandbox
                    4,  // SoccerBall
                    7,  // WontPlay
                    9,  // Picture
                    5   // Doll
                },
                // Version B
                new int[] {
                    6,  // Airplane
                    7,  // Swing
                    4,  // Car
                    10, // Birthday
                    11, // KickBall
                    7   // SuckThumb
                }
            },
            // Female
            new int[][]
            {
                // Version A
                new int[] {
                    5,  // Blocks
                    4,  // Sandbox
                    4,  // SoccerBall
                    7,  // WontPlay
                    9,  // Picture
                    5   // Doll
                },
                // Version B
                new int[] {
                    6, // Airplane
                    8,  // Swing
                    4,  // Car
                    11, // Birthday
                    11, // KickBall
                    8   // SuckThumb
                }
            },
        };

        private int   CSTFaceIntroDuration   = 4;
        private int[] CSTFaceDurations       = {1, 1, 1, 1};
        private int   CSTWhatDoYouDoDuration = 2;

        private System.Timers.Timer taskTimer;
        private System.Timers.Timer hesitationTimerFaces = new System.Timers.Timer();
        private System.Timers.Timer hesitationTimerChoices = new System.Timers.Timer();
        private double[] introDurations = { 4.2, 4.2, 4.5 };
        private int currentIntroBubble = 0;
        private int currentResponse = -1;
        private int m_eCurrentStage = 0;
        private int m_eCurrentScenario;
        private bool m_bDidShowSegway = true;
        private Constants.CSTVersions m_eVersion = Constants.CSTVersions.VersionA;
        private Gender m_eGender = Gender.Male;

        // Methods

        public CSTMainWindow()
        {
            InitializeComponent();

            // Initialize the Array of data fields.
            m_aRecordedData = new ArrayList();

#if(DEBUG)
            // allow backgrounding for debugging
            this.TopMost = false;
#endif

            // Hide cursor
            Cursor.Hide();

            // Set and hide progression label (0/6)
            UpdateProgressionLabel(Constants.kFirstScenario);
            progressionLabel.Visible = false;

            // Set Screen to proper size
            this.SetBounds(0, 0, Constants.DeviceResolutionWidth, Constants.DeviceResolutionHeight);

            cellPictureBoxA.BorderStyle = BorderStyle.FixedSingle;

            hesitationTimerFaces.Elapsed += new System.Timers.ElapsedEventHandler((sender, elem) => PlayReminderAudio(sender, elem, "HowWouldYouFeel"));
            hesitationTimerFaces.Interval = Constants.CSTHesitationPause * 1000;
            hesitationTimerChoices.Elapsed += new System.Timers.ElapsedEventHandler((sender, elem) => PlayReminderAudio(sender, elem, "WhatWouldYouDo"));
            hesitationTimerChoices.Interval = Constants.CSTHesitationPause * 1000;
        }

        
        public void StartTask()
        {
            if (m_fParent != null)
                m_fParent.PreventMonitorPowerdown();

            // Get the version to show.
            CSTSelectVersion versionSelectBox = new CSTSelectVersion();
            if (versionSelectBox.ShowDialog() == DialogResult.Cancel)
            {
                m_fParent.AllowMonitorPowerdown();
                m_fParent.CloseWindow();
                return;
            }
            System.Console.WriteLine("Selected Version: {0}",versionSelectBox.m_eVersionSelected);
            m_eVersion = versionSelectBox.m_eVersionSelected;

            // Get the gender for audio.
            CSTGenderSelection genderSelectBox = new CSTGenderSelection();
            if (genderSelectBox.ShowDialog() == DialogResult.Cancel)
            {
                m_fParent.AllowMonitorPowerdown();
                m_fParent.CloseWindow();
                return;
            }
            System.Console.WriteLine("Selected Gender: {0}", genderSelectBox.m_eGenderSelected);
            m_eGender = genderSelectBox.m_eGenderSelected;

            // Set initial Scenario
            this.Show();

            // Randomize the order of scenarios
            int nNumber;
            Random oRandom = new Random();
            for (int i = 0; i < CSTImageResponses[(int)m_eVersion].Count(); i++)
            {
                // Randomize each face choice
                bool bDidSucceed = false;
                while (!bDidSucceed)
                {
                    // Keep going until we find an unused value
                    nNumber = oRandom.Next(0, CSTImageResponses[(int)m_eVersion].Count());
                    Console.WriteLine("Found: {0} {1} {2}", nNumber, m_aCurrentScenarioOrder[nNumber], CSTImageResponses[(int)m_eVersion].Count());
                    if (m_aCurrentScenarioOrder[nNumber] == -1)
                    {
                        m_aCurrentScenarioOrder[nNumber] = i;
                        bDidSucceed = true;
                    }
                }
            }
            for (int i = 0; i < CSTImageTraining.Count(); i++)
            {
                // Randomize each face choice
                bool bDidSucceed = false;
                while (!bDidSucceed)
                {
                    // Keep going until we find an unused value
                    nNumber = oRandom.Next(0, CSTImageTraining.Count());
                    Console.WriteLine("Found: {0} {1} {2}", nNumber, m_aCurrentTrainingOrder[nNumber], CSTImageTraining.Count());
                    if (m_aCurrentTrainingOrder[nNumber] == -1)
                    {
                        m_aCurrentTrainingOrder[nNumber] = i;
                        bDidSucceed = true;
                    }
                }
            }
            StartTraining();
        }
        private void StartTraining()
        {
            DialogResult result = MessageBox.Show("Do you want to start?", "CST", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.No)
            {
                EndThis();
            }
            else
            {
                m_eCurrentStage = (int)Constants.CSTStages.Training;
                PlaySoundFromFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Resources\\Audio\\Volume_Level.wma");
                changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                cellPictureBoxA.Invoke(pictureBoxDelegate, new object[2] { CSTImageTraining[m_aCurrentTrainingOrder[0]], cellPictureBoxA });
                cellPictureBoxB.Invoke(pictureBoxDelegate, new object[2] { CSTImageTraining[m_aCurrentTrainingOrder[1]], cellPictureBoxB });
                cellPictureBoxC.Invoke(pictureBoxDelegate, new object[2] { CSTImageTraining[m_aCurrentTrainingOrder[2]], cellPictureBoxC });
                cellPictureBoxD.Invoke(pictureBoxDelegate, new object[2] { CSTImageTraining[m_aCurrentTrainingOrder[3]], cellPictureBoxD });
            }
        }
        private void ProgressTrainingMaybe(int nIndex)
        {
            if (2 == m_aCurrentTrainingOrder[nIndex])
            {
                toggleImageVisible imageVisible = new toggleImageVisible(ToggleImageBoxVisibility);
                cellPictureBoxA.Invoke(imageVisible, new object[2] { false, cellPictureBoxA} );
                cellPictureBoxB.Invoke(imageVisible, new object[2] { false, cellPictureBoxB });
                cellPictureBoxC.Invoke(imageVisible, new object[2] { false, cellPictureBoxC });
                cellPictureBoxD.Invoke(imageVisible, new object[2] { false, cellPictureBoxD });

                changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                introBox.Invoke(pictureBoxDelegate, new object[2] { Properties.Resources.CAPSEL, introBox });
                //introBox.Invoke(imageVisible, new object[2] { true, introBox });

            }
        }
        private void EndTraining()
        {
            m_fParent.StartTimeMeasuring();

            m_eCurrentStage = (int)Constants.CSTStages.Catalyst;
            PlaySoundFromFile(GetStringForAudioFileWithName("Introduction"));
            StartScenarioTimerEvent(null, null);
        }

        private void StartScenarioTimerEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            if (taskTimer != null)
                taskTimer.Enabled = false;

            if (currentIntroBubble <= 2)
            {
                changeImageInBox changeImage = new changeImageInBox(ChangeImageInCenterBox);
                
                Bitmap bmp;
                switch (currentIntroBubble)
                {
                    case 0:
                        bmp = Properties.Resources.chubby_puppy_transparent_msg1;
                        break;
                    case 1:
                        bmp = Properties.Resources.chubby_puppy_transparent_msg2;
                        break;
                    case 2:
                    default:
                        bmp = Properties.Resources.chubby_puppy_transparent_msg3;
                        break;
                }

                object[] tempObject = new object[2] { bmp, introBox };
                introBox.Invoke(changeImage, tempObject);

                taskTimer = new System.Timers.Timer();
                taskTimer.Elapsed += new System.Timers.ElapsedEventHandler(StartScenarioTimerEvent);
                taskTimer.Interval = (introDurations[currentIntroBubble]) * 1000;
                taskTimer.Enabled = true;

                currentIntroBubble++;
            }
            else
            {
                // Hide Introduction puppy
                toggleImageVisible pictureBoxDelegate = new toggleImageVisible(ToggleImageBoxVisibility);
                object[] tempObject = new object[2] { false, introBox };
                introBox.Invoke(pictureBoxDelegate, tempObject);

                toggleProgressionVisible toggleProg = new toggleProgressionVisible(ToggleProgressionVisible);
                object[] tempObject2 = new object[1] { true };
                progressionLabel.Invoke(toggleProg, tempObject2);

                StartScenario(Constants.kFirstScenario);
            }
        }

        private void ContinueScenarioTimerEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            if (taskTimer != null)
                taskTimer.Enabled = false;

            StartScenario(m_eCurrentScenario);
        }

        private void StartScenario(int scenario)
        {
            Console.WriteLine("StartScenario({0}) m_eCurrentScenario={1}, m_eCurrentStage={2}, m_aCurrentScenarioOrder={3}", scenario, m_eCurrentScenario, m_eCurrentStage, m_aCurrentScenarioOrder[m_eCurrentScenario]);

            if (CSTImageCatalysts[(int)m_eVersion].Length <= (int)m_eCurrentScenario)
            {
                ExitTask();
                return;
            }
            
            // Set to current scenario
            m_eCurrentScenario = scenario;

            updateProgressionLabel updateProg = new updateProgressionLabel(UpdateProgressionLabel);
            object[] updateProgObj = new object[1] { m_eCurrentScenario };
            progressionLabel.Invoke(updateProg, updateProgObj);

            // enable 'repeat' control
            if (!ButtonRepeat.Visible)
            {
                toggleImageVisible pictureBoxDelegate = new toggleImageVisible(ToggleImageBoxVisibility);
                object[] tempObject = new object[2] { true, ButtonRepeat };
                introBox.Invoke(pictureBoxDelegate, tempObject);
            }

            taskTimer = new System.Timers.Timer();
            

            int nNumber;
            Random oRandom = new Random();

            if (!m_bDidShowSegway)
            {
                // show intermediary slide with character and play positive audio
                changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                object[] tempObject = new object[2] { (m_eCurrentScenario == 1 ? Properties.Resources.chubby_puppy_transparent_letskeepgoing : 
                                                                                 Properties.Resources.chubby_puppy_transparent_heresanotherstory), segwayBox };
                segwayBox.Invoke(pictureBoxDelegate, tempObject);

                string sAudio = (m_eCurrentScenario == 1 ? "LetsKeepGoing" : "HeresAnotherStory");
                PlaySoundFromFile(GetStringForAudioFileWithName(sAudio));

                taskTimer.Elapsed += new System.Timers.ElapsedEventHandler(ContinueScenarioTimerEvent);
                taskTimer.Interval = 2 * 1000;
                
                m_bDidShowSegway = true;
            }
            else
            {
                taskTimer.Elapsed += new System.Timers.ElapsedEventHandler(ShowResponsesTimerEvent);

                // Generate the random faces display order
                switch (m_eCurrentStage)
                {
                    case (int)Constants.CSTStages.Catalyst:
                        m_aTempData = new ArrayList();
                        changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                        object[] tempObject2 = new object[2] { CSTImageCatalysts[(int)m_eVersion][(int)m_aCurrentScenarioOrder[m_eCurrentScenario]], centerPicBox };
                        centerPicBox.Invoke(pictureBoxDelegate, tempObject2);

                        taskTimer.Interval = (CSTCatalystDuration[(int)m_eGender][(int)m_eVersion][(int)m_aCurrentScenarioOrder[m_eCurrentScenario]] + Constants.CSTDefaultPause) * 1000;
                        break;

                    case (int)Constants.CSTStages.Faces:

                        for (int i = 0; i < CSTImageFaces.Count(); i++)
                        {
                            // Randomize each face choice
                            bool bDidSucceed = false;
                            while (!bDidSucceed)
                            {
                                // Keep going until we find an unused value
                                nNumber = oRandom.Next(0, CSTImageFaces.Count());
                                Console.WriteLine("Found: {0} {1} {2}", nNumber, m_aCurrentFaceOrder[nNumber], CSTImageFaces.Count());
                                if (m_aCurrentFaceOrder[nNumber] == -1)
                                {
                                    m_aCurrentFaceOrder[nNumber] = i;
                                    bDidSucceed = true;
                                }
                            }
                        }

                        taskTimer.Interval = (CSTFaceIntroDuration + 1) * 1000;
                        break;

                    case (int)Constants.CSTStages.Choices:

                        for (int i = 0; i < CSTImageFaces.Count(); i++)
                        {
                            // Randomize each face choice
                            bool bDidSucceed = false;
                            while (!bDidSucceed)
                            {
                                // Keep going until we find an unused value
                                nNumber = oRandom.Next(0, CSTImageFaces.Count());
                                Console.WriteLine("Found: {0} {1} {2}", nNumber, m_aCurrentChoiceOrder[nNumber], CSTImageFaces.Count());
                                if (m_aCurrentChoiceOrder[nNumber] == -1)
                                {
                                    m_aCurrentChoiceOrder[nNumber] = i;
                                    bDidSucceed = true;
                                }
                            }
                        }

                        taskTimer.Interval = (CSTWhatDoYouDoDuration + 1) * 1000;
                        break;
                }

                // Disable 'clicking' imageboxes
                ToggleImagesEnabled(false);

                // Start the audio for the current stage/scenario
                PlayAppropriateAudioFile("");
            }

            // Kick off the timer for N sec
            taskTimer.Enabled = true;

        }

        private void ShowResponsesTimerEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("CSTMainWindow:ShowResponsesTimerEvent");
            taskTimer.Enabled = false;
            changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
            //taskTimer.Stop();
            bool bQuit = false;
            
            if (m_eCurrentStage == (int)Constants.CSTStages.Catalyst)
            {
                m_eCurrentStage++;
                //currentResponse = 99;
                StartScenario(m_eCurrentScenario);
                return;
            }

            if (currentResponse < 0)
            {
                currentResponse = (int)Constants.CSTScenarioResponseChoices.ChoiceA;
            }
            else if (currentResponse < 3)
            {
                currentResponse++;
            }
            else
            {
                currentResponse = -1;
                ToggleImagesEnabled(true);
                bQuit = true;
            }
            if (!bQuit)
            {
                if (m_eCurrentStage == (int)Constants.CSTStages.Faces)
                {
                    // Faces
                    taskTimer.Interval = (CSTFaceDurations[currentResponse] + (Constants.CSTDefaultPause / 2)) * 1000;
                    PlayAppropriateAudioFile(GetAlphaIndexFromInt(m_aCurrentFaceOrder[currentResponse]));
                }
                else
                {
                    // Choices
                    taskTimer.Interval = (CSTChoiceDurations[(int)m_eVersion][(int)m_aCurrentScenarioOrder[m_eCurrentScenario]][currentResponse] + (Constants.CSTDefaultPause / 2)) * 1000;
                    PlayAppropriateAudioFile(GetAlphaIndexFromInt(m_aCurrentChoiceOrder[currentResponse]));
                }
                taskTimer.Enabled = true;

                Console.WriteLine("Showing response: {0}", currentResponse);
                if (currentResponse >= 0)
                {
                    PictureBox currPictureBox = new PictureBox();

                    switch (currentResponse)
                    {
                        case 0:
                            // Choice A
                            currPictureBox = cellPictureBoxA;
                            break;
                        case 1:
                            // Chioce B
                            currPictureBox = cellPictureBoxB;
                            break;
                        case 2:
                            // Choice C
                            currPictureBox = cellPictureBoxC;
                            break;
                        case 3:
                            // Choice D
                            currPictureBox = cellPictureBoxD;
                            break;
                        default:
                            // Error
                            Console.WriteLine("Error: Bad switch case - {0}", currentResponse);
                            break;
                    }

                    object[] tempObject = new object[2];

                    if (m_eCurrentStage == (int)Constants.CSTStages.Faces)
                    {
                        // Emotional Responses
                        tempObject = new object[2] { CSTImageFaces[m_aCurrentFaceOrder[currentResponse]], currPictureBox };
                    }
                    else
                    {
                        // Scenario Responses
                        tempObject = new object[2] { CSTImageResponses[(int)m_eVersion][(int)m_aCurrentScenarioOrder[m_eCurrentScenario]][m_aCurrentChoiceOrder[currentResponse]], currPictureBox };
                    }
                    currPictureBox.Invoke(pictureBoxDelegate, tempObject);
                }
            }
            else
            {
                System.Diagnostics.Debug.Assert(m_eCurrentStage != (int)Constants.CSTStages.Catalyst, "Wrong stage!");
                // last item was shown - start reminder audio
                // enable appropriate timer based on stage
                (m_eCurrentStage == (int)Constants.CSTStages.Faces ? hesitationTimerFaces : hesitationTimerChoices).Enabled = true;
            }
        }

        private void PlayReminderAudio(object source, System.Timers.ElapsedEventArgs e, string sAudioFileName)
        {
            PlaySoundFromFile(GetStringForAudioFileWithName(sAudioFileName));
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            bool bPlaying = false;
            if (m_audio != null && !m_audio.Disposed)
            {
                bPlaying = m_audio.Playing;
                if (bPlaying)
                    m_audio.Pause();
            }
            Console.WriteLine("CSTMainWindow:Close()");
            m_fParent.EndTimeMeasuring();
            if (MessageBox.Show("Are you sure you are finished with this assessment for student " + m_fParent.m_oStudent.UUID + "?",
                                "Are you finished?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (MessageBox.Show("Would you like to record the observations for this student?",
                "Record Observations?",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Console.WriteLine("CSTMainWindow:FinishedAndSave()");
                    RecordResults();
                }
                else
                {
                    Console.WriteLine("CSTMainWindow:FinishedAndDiscardObservations()");
                }

                EndThis();
            }
            else
            {
                if (bPlaying)
                    m_audio.Play();
            }
        }
        private void EndThis()
        {
            if (taskTimer != null)
                taskTimer.Enabled = false;
            hesitationTimerChoices.Enabled = hesitationTimerFaces.Enabled = false;
            if (m_audio != null && m_audio.Playing)
                m_audio.Stop();
            m_fParent.AllowMonitorPowerdown();
            Cursor.Show();
            this.Close();
        }

        private void ToggleProgressionVisible(Boolean bVisible)
        {
            progressionLabel.Visible = bVisible;
        }

        private void UpdateProgressionLabel(int nCurrentStep)
        {
            string sLabel = (nCurrentStep+1).ToString() + " / " + (Constants.kLastScenario+1).ToString();
            progressionLabel.Text = sLabel;
        }

        private void ChangeImageInCenterBox(Bitmap imageName, System.Windows.Forms.PictureBox imageBox)
        {
            imageBox.Visible = true;
            imageBox.Image = imageName;
        }

        private void ShowImageInBox(Bitmap imageName, System.Windows.Forms.PictureBox imageBox)
        {
            
            int nTag = Convert.ToInt16(imageBox.Tag);
            Console.WriteLine("CSTMainWindow:ShowImageInBox({0} - {1})", imageName, nTag);

            deselectImageBox deselectIB = new deselectImageBox(DeselectImageBox);
            object[] tempObject; 
            tempObject = new object[2] { false, cellPictureBoxA };
            cellPictureBoxA.Invoke(deselectIB, tempObject);
            tempObject = new object[2] { false, cellPictureBoxB };
            cellPictureBoxB.Invoke(deselectIB, tempObject);
            tempObject = new object[2] { false, cellPictureBoxC };
            cellPictureBoxC.Invoke(deselectIB, tempObject);
            tempObject = new object[2] { false, cellPictureBoxD };
            cellPictureBoxD.Invoke(deselectIB, tempObject);

            // Toggle all pictureboxes based on which one is being assigned a new image.
            switch (nTag)
            {
                case (int)Constants.CSTPictureBoxTags.mainCharacterBox:
                    introBox.Visible            = true;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = false;
                    cellPictureBoxB.Visible     = false;
                    cellPictureBoxC.Visible     = false;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.bottomRightPictureBox:
                    introBox.Visible            = false;
                    segwayBox.Visible           = true;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = false;
                    cellPictureBoxB.Visible     = false;
                    cellPictureBoxC.Visible     = false;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.centerPictureBox:
                    introBox.Visible            = false;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = true;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = false;
                    cellPictureBoxB.Visible     = false;
                    cellPictureBoxC.Visible     = false;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.topCenterPictureBox:
                    introBox.Visible            = false;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = true;
                    cellPictureBoxA.Visible     = false;
                    cellPictureBoxB.Visible     = false;
                    cellPictureBoxC.Visible     = false;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.cellPictureBoxA:
                    introBox.Visible            = false;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = true;
                    cellPictureBoxB.Visible     = false;
                    cellPictureBoxC.Visible     = false;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.cellPictureBoxB:
                    introBox.Visible            = false;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = true;
                    cellPictureBoxB.Visible     = true;
                    cellPictureBoxC.Visible     = false;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.cellPictureBoxC:
                    introBox.Visible            = false;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = true;
                    cellPictureBoxB.Visible     = true;
                    cellPictureBoxC.Visible     = true;
                    cellPictureBoxD.Visible     = false;
                    break;
                case (int)Constants.CSTPictureBoxTags.cellPictureBoxD:
                    introBox.Visible            = false;
                    segwayBox.Visible           = false;
                    centerPicBox.Visible        = false;
                    topCenterPictureBox.Visible = false;
                    cellPictureBoxA.Visible     = true;
                    cellPictureBoxB.Visible     = true;
                    cellPictureBoxC.Visible     = true;
                    cellPictureBoxD.Visible     = true;
                    break;
                default:
                    Console.WriteLine("Error: Uncaught case statement {0}", (int)imageBox.Tag);
                    break;
            }

            // Check for need to scale
            if (imageBox.Width != imageName.Width)
            {
                // Resize/Scale Bitmap
                imageBox.Image = ResizeBitmap(imageName, imageBox.Width, imageBox.Height);
            }
            else
            {
                // Leave image as is
                imageBox.Image = imageName;
            }

        }

        private void ToggleImagesEnabled(Boolean bEnabled)
        {
            toggleImageEnabled toggleImage = new toggleImageEnabled(ToggleImageEnabled);
            object[] tmpObjectA = { bEnabled, cellPictureBoxA };
            object[] tmpObjectB = { bEnabled, cellPictureBoxB };
            object[] tmpObjectC = { bEnabled, cellPictureBoxC };
            object[] tmpObjectD = { bEnabled, cellPictureBoxD };
            cellPictureBoxA.Invoke(toggleImage, tmpObjectA);
            cellPictureBoxB.Invoke(toggleImage, tmpObjectB);
            cellPictureBoxC.Invoke(toggleImage, tmpObjectC);
            cellPictureBoxD.Invoke(toggleImage, tmpObjectD);
        }

        private void ToggleImageEnabled(Boolean bEnabled, System.Windows.Forms.PictureBox imageBox)
        {
            imageBox.Enabled = bEnabled;
            //imageBox.Cursor = (bEnabled ? System.Windows.Forms.Cursors.Hand : System.Windows.Forms.Cursors.Default);
        }

        private void ToggleImageBoxVisibility(System.Windows.Forms.PictureBox imageBox)
        {
            Console.WriteLine("CSTMainWindow:ToggleImageBoxVisibility: {0} -> {1}", imageBox.Visible, !imageBox.Visible);
            imageBox.Visible = !imageBox.Visible;
        }

        private void ToggleImageBoxVisibility(Boolean bVisible, System.Windows.Forms.PictureBox imageBox)
        {
            Console.WriteLine("CSTMainWindow:ToggleImageBoxVisibility: {0} -> {1}", imageBox.Visible, bVisible);
            imageBox.Visible = bVisible;
        }

        private void DeselectImageBox(Boolean bEnabled, System.Windows.Forms.PictureBox imageBox)
        {
            imageBox.BorderStyle = BorderStyle.None;
        }

        public Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            return result;
        }

        private void MakeSelectionWithIndex(int nIndex)
        {
            if (m_eCurrentStage == (int)Constants.CSTStages.Training)
            {
                ProgressTrainingMaybe(nIndex);
                return;
            }
            // stop hesitation timers
            hesitationTimerFaces.Enabled = hesitationTimerChoices.Enabled = false;

            // Generate array data fields for storage
            string[] aData = { "", "", "" };
            // Scenario
            aData[0] = ((int)m_aCurrentScenarioOrder[m_eCurrentScenario]).ToString();
            // Stage (scenarios or faces)
            aData[1] = m_eCurrentStage.ToString();
            // Selected Index
            if (m_eCurrentStage == (int)Constants.CSTStages.Faces)
            {
                // Emotional Response Selection - Randomized Order
                aData[2] = m_aCurrentFaceOrder[nIndex].ToString();
                string result = m_aCurrentFaceOrder.Aggregate(string.Empty, (s, i) => s + i.ToString());
                Console.WriteLine("CurrentFaceOrder: {0}", result);
                for (int i = 0; i < CSTImageFaces.Count(); i++)
                {
                    // Clean up randomization variable
                    m_aCurrentFaceOrder[i] = -1;
                }
                m_aTempData.Add(aData);
            }
            else
            {
                // Scenario Response Selection - Randomized Order
                aData[2] = m_aCurrentChoiceOrder[nIndex].ToString();
                string result = m_aCurrentChoiceOrder.Aggregate(string.Empty, (s, i) => s + i.ToString());
                Console.WriteLine("CurrentFaceOrder: {0}", result);
                for (int i = 0; i < CSTImageFaces.Count(); i++)
                {
                    // Clean up randomization variable
                    m_aCurrentChoiceOrder[i] = -1;
                }
                m_aTempData.Add(aData);
                m_aRecordedData.AddRange(m_aTempData);
            }


            // these two lines make the whole thing exit after just one scenario (used during debug!)
            //if (m_eCurrentStage == (int)Constants.CSTStages.Choices)
                //m_eCurrentScenario = Constants.kLastScenario;
            Console.Out.WriteLine("CSTMainWindow::MakeSelectionWithIndex({0} - {1})", m_eCurrentScenario, nIndex);
            if (m_eCurrentScenario < Constants.kLastScenario || m_eCurrentStage < (int)Constants.CSTStages.Choices)
            {
                // Start Next Scenario
                if (m_eCurrentStage < (int)Constants.CSTStages.Choices)
                {
                    m_eCurrentStage++;
                }
                else
                {
                    m_eCurrentStage = 0;
                    m_bDidShowSegway = false;
                    m_eCurrentScenario++;
                }
                StartScenario(m_eCurrentScenario);
            }
            else
            {
                // show you did it dog with audio
                changeImageInBox pictureBoxDelegate = new changeImageInBox(ShowImageInBox);
                object[] tempObject = new object[2] { Properties.Resources.chubby_puppy_transparent, introBox };
                introBox.Invoke(pictureBoxDelegate, tempObject);

                PlaySoundFromFile(GetStringForAudioFileWithName("AllDoneYouDidIt"));

                taskTimer = new System.Timers.Timer();
                taskTimer.Elapsed += new System.Timers.ElapsedEventHandler(FinishTask);
                taskTimer.Interval = 2 * 1000;
                taskTimer.Enabled = true;
            }
        }

        private void FinishTask(object source, System.Timers.ElapsedEventArgs e)
        {
            if (taskTimer != null)
                taskTimer.Enabled = false;
            this.Invoke(new closeMyself(ExitTask));
        }
        private void RecordResults()
        {
            Console.Out.WriteLine("Writing: {0}", GetStringForWrite());
            System.IO.File.AppendAllText(GetFileNameForResultOutput(), GetStringForWrite());
        }
        private void ExitTask()
        {
            // Exit Task - Write results
            MessageBox.Show("The Challenging Situations Task (Version " + (m_eVersion == Constants.CSTVersions.VersionA ? "A" : "B") + ") for Student " + m_fParent.m_oStudent.UUID + " is complete.", "Task Complete");
            m_fParent.EndTimeMeasuring();
            RecordResults();
            taskTimer.Enabled = false;
            hesitationTimerFaces.Enabled = hesitationTimerChoices.Enabled = false;
            if (m_audio != null && m_audio.Playing)
                m_audio.Stop();
            // Show cursor
            Cursor.Show();
            m_fParent.AllowMonitorPowerdown();
            //m_fParent.CloseWindow();
            this.Close();
        }

        private void cellPictureBoxA_Click(object sender, EventArgs e)
        {
            cellPictureBoxA.BorderStyle = BorderStyle.FixedSingle;
            MakeSelectionWithIndex(0);
        }

        private void cellPictureBoxB_Click(object sender, EventArgs e)
        {
            cellPictureBoxB.BorderStyle = BorderStyle.FixedSingle;
            MakeSelectionWithIndex(1);
        }

        private void cellPictureBoxC_Click(object sender, EventArgs e)
        {
            cellPictureBoxC.BorderStyle = BorderStyle.FixedSingle;
            MakeSelectionWithIndex(2);
        }

        private void cellPictureBoxD_Click(object sender, EventArgs e)
        {
            cellPictureBoxD.BorderStyle = BorderStyle.FixedSingle;
            MakeSelectionWithIndex(3);
        }

        private string GetStringForWrite()
        {
            String result = m_fParent.m_oStudent.UUID + "," + Parent.m_oTeacher.UUID + "," + Parent.m_oTeacher.LastName + "," + DateTime.Now + "," + m_fParent.GetTimeMeasureInSeconds() + "," + m_eVersion + "," + GetStringFromDataArray(m_aRecordedData) + "\n";
            return result;
        }

        private string GetFileNameForResultOutput()
        {
            //string result = System.Environment.MachineName.ToLower() + "_" + DateTime.Now.ToString("yyyyMMdd") + "_CST.csv";
            string result = Parent.m_oStudent.UUID + "_CST_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            bool finishloop;
            do
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Results files (*.csv)|*.csv|All files|*.*";
                sfd.FileName = result;
                sfd.Title = "Choose the file where these results will be stored";
                //sfd.CheckFileExists = true;
                finishloop = sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                if (finishloop)
                    result = sfd.FileName;
            } while (!finishloop);

            return result;
        }

        private string GetStringFromDataArray(ArrayList ar)
        {
            // Find out which questions haven't been answered.
            HashSet<string> answered = new HashSet<string>();
            foreach (string[] a in ar)
                answered.Add(a[0]);
            for (int i = 0; i < 6; ++ i)
                if (!answered.Contains(i.ToString()))
                {
                    ar.Add(new string[] { i.ToString(), "1", "99" });
                    ar.Add(new string[] { i.ToString(), "2", "99" });
                }
            string sReturn = "";
            string[][] aValues = (string[][])ar.ToArray(typeof(string[]));
            Array.Sort<string[]>(aValues, delegate(string[] s1, string[] s2) {
                return (string.Join("",s1)).CompareTo(string.Join("",s2));
            });

            foreach (string[] str in aValues)
            {
                sReturn += string.Join(",", str) + ",";
            }
            return sReturn;
        }

        private void PlaySoundFromFile(string sFileName)
        {
            if (File.Exists(sFileName))
            {
                if (m_audio != null)
                {
                    //m_audio.Stop();
                    m_audio.Dispose();
                }
                m_audio = new Audio(sFileName);
                Console.Out.WriteLine("PlaySoundFromFile: {0} ; length: {1}", sFileName, m_audio.Duration);
                m_audio.Play();
            }
            else
            {
                Console.Out.WriteLine("PlaySoundFromFile: ERROR! File does not exist: {0}", sFileName);
            }
        }

        private void PlayAppropriateAudioFile(string sIndex)
        {
            PlaySoundFromFile(GetStringForAudioFileWithName(GetAudioFileStringForCurrentStage(sIndex)));
        }

        private string GetStringForAudioFileWithName(string sName)
        {
            return Path.GetDirectoryName(Application.ExecutablePath) + "\\Resources\\Audio\\CST_" + sName + ".wma";
        }

        private string GetAudioFileStringForCurrentStage(string sIndex)
        {
            string sAudio = "";
            switch (m_eVersion)
            {
                case Constants.CSTVersions.VersionA:
                    {
                        // Version A
                        switch (m_aCurrentScenarioOrder[m_eCurrentScenario])
                        {
                            case (int)Constants.CSTScenarioNamesVersionA.Blocks:
                                sAudio = "Blocks";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionA.Doll:
                                sAudio = "Doll";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionA.Picture:
                                sAudio = "Picture";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionA.Sandbox:
                                sAudio = "Sandbox";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionA.SoccerBall:
                                sAudio = "SoccerBall";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionA.WontPlay:
                                sAudio = "WontPlay";
                                break;
                        }
                        break;
                    }
                case Constants.CSTVersions.VersionB:
                    {
                        switch (m_aCurrentScenarioOrder[m_eCurrentScenario])
                        {
                            case (int)Constants.CSTScenarioNamesVersionB.Airplane:
                                sAudio = "Airplane";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionB.Birthday:
                                sAudio = "Birthday";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionB.Car:
                                sAudio = "Car";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionB.KickBall:
                                sAudio = "KickBall";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionB.SuckThumb:
                                sAudio = "SuckThumb";
                                break;
                            case (int)Constants.CSTScenarioNamesVersionB.Swing:
                                sAudio = "Swing";
                                break;
                        }
                        break;
                    }
            }

            switch (m_eCurrentStage)
            {
                case (int)Constants.CSTStages.Catalyst:
                    return sAudio + "_" + (m_eGender == 0 ? "Male" : "Female");
                case (int)Constants.CSTStages.Faces:
                    if (sIndex.Equals(""))
                    {
                        return "HowDoYouFeel";
                    }
                    else
                    {
                        return "HowDoYouFeel_" + sIndex;
                    }
                case (int)Constants.CSTStages.Choices:
                    if (sIndex.Equals(""))
                    {
                        return "WhatDoYouDo";
                    }
                    else 
                    {
                        return sAudio + "_Choices_" + sIndex;
                    }
            }

            Console.WriteLine("GetAudioFileStringForCurrentAspect: ERROR invalid stage: {0}",m_eCurrentStage);
            return "";
        }

        private string GetAlphaIndexFromInt(int nIndex)
        {
            char c = Convert.ToChar(nIndex + 65);
            return c.ToString().ToUpper();
        }
        
        public CSTTask Parent
        {
            get
            {
                return m_fParent;
            }
            set
            {
                m_fParent = value;
            }
        }

        // stop current scenario and repeat
        private void repeatButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine("CSTMainWindow::repeatButton_Click");
            
            // stop timer
            taskTimer.Enabled = false;
            hesitationTimerFaces.Enabled = hesitationTimerChoices.Enabled = false;
            
            // stop audio
            if (m_audio != null && m_audio.Playing)
                m_audio.Stop();
            
            
            // Clean up randomization variable
            for (int i = 0; i < CSTImageFaces.Count(); i++)
            {
                m_aCurrentFaceOrder[i] = -1;
                m_aCurrentChoiceOrder[i] = -1;
            }

            // clean up position variables
            m_eCurrentStage = 0;
            currentResponse = -1;

            // hide boxes
            toggleImageVisible imageVisible = new toggleImageVisible(ToggleImageBoxVisibility);
            object[] tempObject;
            tempObject = new object[2] { false, cellPictureBoxA };
            cellPictureBoxA.Invoke(imageVisible, tempObject);
            tempObject = new object[2] { false, cellPictureBoxB };
            cellPictureBoxB.Invoke(imageVisible, tempObject);
            tempObject = new object[2] { false, cellPictureBoxC };
            cellPictureBoxC.Invoke(imageVisible, tempObject);
            tempObject = new object[2] { false, cellPictureBoxD };
            cellPictureBoxD.Invoke(imageVisible, tempObject);

            // disable image boxes
            ToggleImagesEnabled(false);

            // restart current scenario
            StartScenario(m_eCurrentScenario);
        }

        private void introBox_Click(object sender, EventArgs e)
        {
            if (m_eCurrentStage == (int)Constants.CSTStages.Training)
                EndTraining();
        }
    }
}
