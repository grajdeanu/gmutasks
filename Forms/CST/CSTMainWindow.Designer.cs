﻿namespace GMUTasks.Forms.CST
{
    partial class CSTMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CSTMainWindow));
            this.ButtonClose = new System.Windows.Forms.PictureBox();
            this.centerPicBox = new System.Windows.Forms.PictureBox();
            this.topCenterPictureBox = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxA = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxB = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxC = new System.Windows.Forms.PictureBox();
            this.cellPictureBoxD = new System.Windows.Forms.PictureBox();
            this.introBox = new System.Windows.Forms.PictureBox();
            this.ButtonRepeat = new System.Windows.Forms.PictureBox();
            this.progressionLabel = new System.Windows.Forms.Label();
            this.segwayBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.centerPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topCenterPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.introBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRepeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.segwayBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonClose
            // 
            this.ButtonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonClose.Image = ((System.Drawing.Image)(resources.GetObject("ButtonClose.Image")));
            this.ButtonClose.Location = new System.Drawing.Point(1236, 12);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(32, 32);
            this.ButtonClose.TabIndex = 0;
            this.ButtonClose.TabStop = false;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // centerPicBox
            // 
            this.centerPicBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.centerPicBox.Location = new System.Drawing.Point(376, 262);
            this.centerPicBox.Name = "centerPicBox";
            this.centerPicBox.Size = new System.Drawing.Size(528, 276);
            this.centerPicBox.TabIndex = 1;
            this.centerPicBox.TabStop = false;
            this.centerPicBox.Tag = "0";
            this.centerPicBox.Visible = false;
            // 
            // topCenterPictureBox
            // 
            this.topCenterPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.topCenterPictureBox.Location = new System.Drawing.Point(376, 203);
            this.topCenterPictureBox.Name = "topCenterPictureBox";
            this.topCenterPictureBox.Size = new System.Drawing.Size(528, 276);
            this.topCenterPictureBox.TabIndex = 2;
            this.topCenterPictureBox.TabStop = false;
            this.topCenterPictureBox.Tag = "1";
            this.topCenterPictureBox.Visible = false;
            // 
            // cellPictureBoxA
            // 
            this.cellPictureBoxA.Location = new System.Drawing.Point(66, 85);
            this.cellPictureBoxA.Name = "cellPictureBoxA";
            this.cellPictureBoxA.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxA.TabIndex = 3;
            this.cellPictureBoxA.TabStop = false;
            this.cellPictureBoxA.Tag = "2";
            this.cellPictureBoxA.Visible = false;
            this.cellPictureBoxA.Click += new System.EventHandler(this.cellPictureBoxA_Click);
            // 
            // cellPictureBoxB
            // 
            this.cellPictureBoxB.Location = new System.Drawing.Point(687, 85);
            this.cellPictureBoxB.Name = "cellPictureBoxB";
            this.cellPictureBoxB.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxB.TabIndex = 4;
            this.cellPictureBoxB.TabStop = false;
            this.cellPictureBoxB.Tag = "3";
            this.cellPictureBoxB.Visible = false;
            this.cellPictureBoxB.Click += new System.EventHandler(this.cellPictureBoxB_Click);
            // 
            // cellPictureBoxC
            // 
            this.cellPictureBoxC.Location = new System.Drawing.Point(66, 444);
            this.cellPictureBoxC.Name = "cellPictureBoxC";
            this.cellPictureBoxC.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxC.TabIndex = 5;
            this.cellPictureBoxC.TabStop = false;
            this.cellPictureBoxC.Tag = "4";
            this.cellPictureBoxC.Visible = false;
            this.cellPictureBoxC.Click += new System.EventHandler(this.cellPictureBoxC_Click);
            // 
            // cellPictureBoxD
            // 
            this.cellPictureBoxD.Location = new System.Drawing.Point(687, 444);
            this.cellPictureBoxD.Name = "cellPictureBoxD";
            this.cellPictureBoxD.Size = new System.Drawing.Size(528, 276);
            this.cellPictureBoxD.TabIndex = 6;
            this.cellPictureBoxD.TabStop = false;
            this.cellPictureBoxD.Tag = "5";
            this.cellPictureBoxD.Visible = false;
            this.cellPictureBoxD.Click += new System.EventHandler(this.cellPictureBoxD_Click);
            // 
            // introBox
            // 
            this.introBox.BackgroundImage = global::GMUTasks.Properties.Resources.chubby_puppy_transparent;
            this.introBox.Location = new System.Drawing.Point(318, 177);
            this.introBox.Name = "introBox";
            this.introBox.Size = new System.Drawing.Size(643, 447);
            this.introBox.TabIndex = 7;
            this.introBox.TabStop = false;
            this.introBox.Tag = "7";
            this.introBox.Visible = false;
            this.introBox.Click += new System.EventHandler(this.introBox_Click);
            // 
            // ButtonRepeat
            // 
            this.ButtonRepeat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRepeat.Image = global::GMUTasks.Properties.Resources.repeat_32x32;
            this.ButtonRepeat.Location = new System.Drawing.Point(12, 12);
            this.ButtonRepeat.Name = "ButtonRepeat";
            this.ButtonRepeat.Size = new System.Drawing.Size(32, 32);
            this.ButtonRepeat.TabIndex = 8;
            this.ButtonRepeat.TabStop = false;
            this.ButtonRepeat.Visible = false;
            this.ButtonRepeat.Click += new System.EventHandler(this.repeatButton_Click);
            // 
            // progressionLabel
            // 
            this.progressionLabel.AutoSize = true;
            this.progressionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressionLabel.Location = new System.Drawing.Point(12, 776);
            this.progressionLabel.Name = "progressionLabel";
            this.progressionLabel.Size = new System.Drawing.Size(35, 15);
            this.progressionLabel.TabIndex = 9;
            this.progressionLabel.Text = "0/10";
            // 
            // segwayBox
            // 
            this.segwayBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.segwayBox.Image = global::GMUTasks.Properties.Resources.chubby_puppy_transparent_letskeepgoing;
            this.segwayBox.Location = new System.Drawing.Point(1018, 550);
            this.segwayBox.Name = "segwayBox";
            this.segwayBox.Size = new System.Drawing.Size(250, 238);
            this.segwayBox.TabIndex = 10;
            this.segwayBox.TabStop = false;
            this.segwayBox.Tag = "6";
            this.segwayBox.Visible = false;
            // 
            // CSTMainWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.ControlBox = false;
            this.Controls.Add(this.segwayBox);
            this.Controls.Add(this.progressionLabel);
            this.Controls.Add(this.ButtonRepeat);
            this.Controls.Add(this.introBox);
            this.Controls.Add(this.cellPictureBoxD);
            this.Controls.Add(this.cellPictureBoxC);
            this.Controls.Add(this.cellPictureBoxB);
            this.Controls.Add(this.cellPictureBoxA);
            this.Controls.Add(this.topCenterPictureBox);
            this.Controls.Add(this.centerPicBox);
            this.Controls.Add(this.ButtonClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CSTMainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CSTMainWindow";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.ButtonClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.centerPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topCenterPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPictureBoxD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.introBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRepeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.segwayBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ButtonClose;
        private System.Windows.Forms.PictureBox topCenterPictureBox;
        private System.Windows.Forms.PictureBox centerPicBox;
        private System.Windows.Forms.PictureBox cellPictureBoxA;
        private System.Windows.Forms.PictureBox cellPictureBoxB;
        private System.Windows.Forms.PictureBox cellPictureBoxC;
        private System.Windows.Forms.PictureBox cellPictureBoxD;
        private System.Windows.Forms.PictureBox introBox;
        private System.Windows.Forms.PictureBox ButtonRepeat;
        private System.Windows.Forms.Label progressionLabel;
        private System.Windows.Forms.PictureBox segwayBox;

    }
}