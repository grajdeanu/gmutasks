﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Forms.CST
{
    public partial class CSTSelectVersion : Form
    {
        public Constants.CSTVersions m_eVersionSelected;

        public CSTSelectVersion()
        {
            InitializeComponent();

            // Hide cursor
            Cursor.Hide();

            m_eVersionSelected = Constants.CSTVersions.VersionA;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_eVersionSelected = Constants.CSTVersions.VersionA;
            Dismiss();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            m_eVersionSelected = Constants.CSTVersions.VersionB;
            Dismiss();
        }
        
        private void Dismiss()
        {
            DialogResult = DialogResult.OK;
            Cursor.Show();
            this.Close();
        }

    }
}
