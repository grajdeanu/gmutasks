﻿namespace GMUTasks
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.startButtonCST = new System.Windows.Forms.Button();
            this.startButtonAKT = new System.Windows.Forms.Button();
            this.startButtonMPAC = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TeacherIDLabel = new System.Windows.Forms.Label();
            this.TeacherIDBox = new System.Windows.Forms.Label();
            this.StudentIdLabel = new System.Windows.Forms.Label();
            this.StudentIdComboBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // startButtonCST
            // 
            this.startButtonCST.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButtonCST.Location = new System.Drawing.Point(12, 97);
            this.startButtonCST.Name = "startButtonCST";
            this.startButtonCST.Size = new System.Drawing.Size(470, 45);
            this.startButtonCST.TabIndex = 3;
            this.startButtonCST.Text = "Challenging Situations Task (CST)";
            this.startButtonCST.UseVisualStyleBackColor = true;
            this.startButtonCST.Click += new System.EventHandler(this.startButtonCST_Click);
            // 
            // startButtonAKT
            // 
            this.startButtonAKT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButtonAKT.Location = new System.Drawing.Point(12, 148);
            this.startButtonAKT.Name = "startButtonAKT";
            this.startButtonAKT.Size = new System.Drawing.Size(470, 45);
            this.startButtonAKT.TabIndex = 5;
            this.startButtonAKT.Text = "Affect Knowledge Test (AKT)";
            this.startButtonAKT.UseVisualStyleBackColor = true;
            this.startButtonAKT.Click += new System.EventHandler(this.startButtonAKT_Click);
            // 
            // startButtonMPAC
            // 
            this.startButtonMPAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButtonMPAC.Location = new System.Drawing.Point(12, 199);
            this.startButtonMPAC.Name = "startButtonMPAC";
            this.startButtonMPAC.Size = new System.Drawing.Size(470, 45);
            this.startButtonMPAC.TabIndex = 6;
            this.startButtonMPAC.Text = "Minnesota Preschool Affect Checklist (MPAC)";
            this.startButtonMPAC.UseVisualStyleBackColor = true;
            this.startButtonMPAC.Click += new System.EventHandler(this.startButtonMPAC_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(13, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(95, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(135, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(331, 34);
            this.label1.TabIndex = 8;
            this.label1.Text = "Welcome to the George Mason University Task\r\nSelection Menu. Please select a task" +
    " below to start.\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TeacherIDLabel
            // 
            this.TeacherIDLabel.AutoSize = true;
            this.TeacherIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeacherIDLabel.Location = new System.Drawing.Point(121, 69);
            this.TeacherIDLabel.Name = "TeacherIDLabel";
            this.TeacherIDLabel.Size = new System.Drawing.Size(88, 17);
            this.TeacherIDLabel.TabIndex = 9;
            this.TeacherIDLabel.Text = "Teacher ID";
            // 
            // TeacherIDBox
            // 
            this.TeacherIDBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeacherIDBox.Location = new System.Drawing.Point(215, 63);
            this.TeacherIDBox.Name = "TeacherIDBox";
            this.TeacherIDBox.Size = new System.Drawing.Size(57, 28);
            this.TeacherIDBox.TabIndex = 10;
            this.TeacherIDBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StudentIdLabel
            // 
            this.StudentIdLabel.AutoSize = true;
            this.StudentIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentIdLabel.Location = new System.Drawing.Point(278, 69);
            this.StudentIdLabel.Name = "StudentIdLabel";
            this.StudentIdLabel.Size = new System.Drawing.Size(84, 17);
            this.StudentIdLabel.TabIndex = 11;
            this.StudentIdLabel.Text = "Student ID";
            // 
            // StudentIdComboBox
            // 
            this.StudentIdComboBox.Enabled = false;
            this.StudentIdComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //this.StudentIdComboBox.FormattingEnabled = true;
            //this.StudentIdComboBox.Items.AddRange(new object[] {
            //"(None)"});
            this.StudentIdComboBox.Location = new System.Drawing.Point(368, 63);
            this.StudentIdComboBox.Name = "StudentIdComboBox";
            this.StudentIdComboBox.Size = new System.Drawing.Size(114, 28);
            this.StudentIdComboBox.TabIndex = 12;
            //this.StudentIdComboBox.SelectedIndexChanged += new System.EventHandler(this.StudentIdComboBox_SelectedIndexChanged);
            this.StudentIdComboBox.TextChanged += new System.EventHandler(this.StudentIdComboBox_OnTextChanged);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 254);
            this.Controls.Add(this.StudentIdComboBox);
            this.Controls.Add(this.StudentIdLabel);
            this.Controls.Add(this.TeacherIDBox);
            this.Controls.Add(this.TeacherIDLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.startButtonMPAC);
            this.Controls.Add(this.startButtonAKT);
            this.Controls.Add(this.startButtonCST);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(500, 282);
            this.MinimumSize = new System.Drawing.Size(500, 282);
            this.Name = "MainWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GMU Task Selection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButtonCST;
        private System.Windows.Forms.Button startButtonAKT;
        private System.Windows.Forms.Button startButtonMPAC;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label TeacherIDLabel;
        private System.Windows.Forms.Label TeacherIDBox;
        private System.Windows.Forms.Label StudentIdLabel;
        private System.Windows.Forms.TextBox StudentIdComboBox;
    }
}

