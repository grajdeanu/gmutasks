﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMUTasks.Classes
{
    class CData
    {
        private String m_sTableName;
        private String[] m_aFields;

        public CData()
        {
            m_sTableName = null;
            m_aFields = null;
        }

        public CData(String sTableName)
        {
            m_sTableName = sTableName;
            m_aFields = null;
        }

        public CData(String sTableName, String[] aFields)
        {
            m_sTableName = sTableName;
            m_aFields = aFields;
        }

        public bool InsertData(Dictionary<String, String> oData)
        {
            // Construct SQL Statement
            return db.Insert(m_sTableName, oData);
        }

        public bool UpdateData(int nId, Dictionary<String, String> oData)
        {
            return db.Update(m_sTableName, oData, "id = \"" + nId + "\"");
        }

        public DataTable GetData(String sSql)
        {
            return db.GetDataTable(sSql);
        }

    }
}
