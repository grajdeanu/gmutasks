﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Classes
{

    public enum Gender
    {
        Male,
        Female,
        NotSpecified
    }

    class User
    {
        private SQLiteDatabase db;
        private String userTable = "user";

        private String m_strFirstName;          // First name of the subject (if available)
        private String m_strLastName;           // Last name of the subject (if available)
        private String m_strUUID;               // UUID of the subject (if available)
        private Gender m_Gender;                // Gender of the subject based on 'enum Gender'


        public void Init()
        {
            if (String.IsNullOrEmpty(m_strFirstName)) { m_strLastName = ""; }
            if (String.IsNullOrEmpty(m_strLastName)) { m_strLastName = ""; }
            if (String.IsNullOrEmpty(m_strUUID)) { m_strUUID = System.Guid.NewGuid().ToString(); }
            if (m_Gender <= 0) { m_Gender = Gender.NotSpecified; }
        }

        private void CreateUser()
        {

        }

        #region SQL Record Operations

        private void createUserTableIfNotExist()
        {
            db = new SQLiteDatabase();
            String strQuery = String.Format("CREATE TABLE IF NOT EXISTS {0}(UUID TEXT PRIMARY KEY, FirstName TEXT, LastName TEXT, Gender INT, Ethnicity INT, created TEXT, modified TEXT)", userTable);
            try
            {
                db.ExecuteNonQuery(strQuery);
            }
            catch (Exception fail)
            {
                String strError = "Could not perform table creation/checker.\nThe following error occured:\n\n";
                strError += fail.Message.ToString() + "\n\n";
                MessageBox.Show(strError);
            }
        }

        private Boolean doesUserExist()
        {
            Boolean bExists = false;
            DataTable dtTasks;
            try
            {
                db = new SQLiteDatabase();
                String strQuery = String.Format("SELECT * FROM \"{0}\" WHERE \"UUID\" = \"{1}\"", userTable, m_strUUID);
                dtTasks = db.GetDataTable(strQuery);
                bExists = (dtTasks.Rows.Count > 0);
            }
            catch (Exception fail)
            {
                String strError = "Could not perform lookup.\nThe following error occured:\n\n";
                strError += fail.Message.ToString() + "\n\n";
                MessageBox.Show(strError);
            }
            return bExists;
        }

        private Boolean createUserRecord()
        {
            // Setup Vars
            Boolean bCreated = false;
            db = new SQLiteDatabase();
            DateTime now = DateTime.Now;
            Dictionary<String, String> data = new Dictionary<String, String>();
            
            // Created Dictionary contents for insertion
            data.Add("FirstName", m_strFirstName);
            data.Add("LastName", m_strLastName);
            data.Add("UUID", m_strUUID);
            data.Add("Gender", m_Gender.ToString());
            data.Add("created", String.Format("{0:yyyy'-'MM'-'dd hh:mm:ss.fff}", now));
            data.Add("modified", String.Format("{0:yyyy'-'MM'-'dd hh:mm:ss.fff}", now));

            // Perform insertion
            try
            {
                bCreated = db.Insert(userTable, data);
            }
            catch (Exception fail)
            {
                String strError = "Could not insert record.\nThe following error occured:\n\n";
                strError += fail.Message.ToString() + "\n\n";
                MessageBox.Show(strError);
            }
            return bCreated;
        }

        #endregion
        #region Getters and Setters

        public String FirstName
        {
            get
            {
                return m_strFirstName;
            }
            set
            {
                m_strFirstName = value;
            }
        }
        public String LastName
        {
            get
            {
                return m_strLastName;
            }
            set
            {
                m_strLastName = value;
            }
        }
        public String UUID
        {
            get
            {
                return m_strUUID;
            }
            set
            {
                m_strUUID = value;
            }
        }
        public Gender Gender
        {
            get
            {
                return m_Gender;
            }
            set
            {
                m_Gender = value;
            }
        }
        #endregion
    }
}
