﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMUTasks.Forms.CST;

namespace GMUTasks.Classes
{

    public class CSTTask : CTask
    {
        
        private CSTMainWindow mainWindow;
        public  CUser         m_oStudent;
        public CUser          m_oTeacher;
        public  int[,]        m_aResponses = new int[3,2];

        public CSTTask()
            : base(CTaskType.CTaskTypeCST)
        {
            Console.WriteLine("CSTTask::CSTTask()");
        }
        
        // Challenging Skills Task
        public void LoadWindow()
        {
            mainWindow = new CSTMainWindow();
            mainWindow.Parent = this;
            mainWindow.StartTask();
            // Simple Message relay
            //mainWindow.textRelay = new RelayText(this.DisplayRelayedText);
        }

        public void CloseWindow()
        {
            mainWindow.Close();
        }
    }
}
