﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GMUTasks.Classes
{

    public enum Gender
    {
        Male = 0,
        Female,
        NotSpecified
    }

    public enum Ethnicity
    {
        NotSpecified = 0,
        White,
        Asian,
        Hispanic,
        Black
    }

    public enum AccessLevel
    {
        None = 0,
        Student,
        Teacher
    }

    public class CUser
    {
        //private SQLiteDatabase db;
        //private String userTable = "user";

        private String m_sFirstName;          // First name of the subject (if available)
        private String m_sLastName;           // Last name of the subject (if available)
        private String m_sUUID;               // UUID of the subject (if available)
        private Gender m_eGender;             // Gender of the subject based on 'enum Gender'
        private Ethnicity m_eEthnicity;       // Ethnicity of subject
        private AccessLevel m_eAccess;        // Role of user, user or teacher


        public CUser()
        {
            m_sFirstName = "";
            m_sLastName = "";
            m_sUUID = "";
            m_eGender = Gender.NotSpecified;
            m_eEthnicity = Ethnicity.NotSpecified;
            m_eAccess = AccessLevel.None;
        }

        public CUser(String sFirstName, String sLastName, String sUUID, Gender eGender, Ethnicity eEthnicity, AccessLevel eAccess)
        {
            m_sFirstName = sFirstName;
            m_sLastName  = sLastName;
            m_sUUID      = sUUID;
            m_eGender    = eGender;
            m_eEthnicity = eEthnicity;
            m_eAccess    = eAccess;
        }
#region Getters and Setters

        public String FirstName
        {
            get
            {
                return m_sFirstName;
            }
            set
            {
                m_sFirstName = value;
            }
        }
        public String LastName
        {
            get
            {
                return m_sLastName;
            }
            set
            {
                m_sLastName = value;
            }
        }
        public String UUID
        {
            get
            {
                return m_sUUID;
            }
            set
            {
                m_sUUID = value;
            }
        }
        public Gender Gender
        {
            get
            {
                return m_eGender;
            }
            set
            {
                m_eGender = value;
            }
        }
        public Ethnicity Ethnicity
        {
            get
            {
                return m_eEthnicity;
            }
            set
            {
                m_eEthnicity = value;
            }
        }
        public AccessLevel AccessLevel
        {
            get
            {
                return m_eAccess;
            }
            set
            {
                m_eAccess = value;
            }
        }
        #endregion
    }

}
