﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Text;
using GMUTasks.Classes;

namespace GMUTasks
{
    [Serializable()]
    class Task
    {
        private String taskTable = "task";

        private String strTaskName;
        private String strTaskDescription;
        private List<Object> arrTaskComponents;
        private SQLiteDatabase db;

        public Task(String taskName, String taskDescription, List<Object> taskComponents)
        {
            strTaskName = (String.IsNullOrEmpty(taskName) ? "Task" : taskName);
            strTaskDescription = (String.IsNullOrEmpty(taskDescription) ? "Task Description" : taskDescription);
            arrTaskComponents = ((taskComponents==null || taskComponents.Count<=0) ? new List<Object>() : taskComponents);
            
            createTaskTableIfNotExist();
            if (doesTaskExist())
            {
                MessageBox.Show(String.Format("Task already exists with name:\n{0}\n", taskName));
            }
            else
            {
                if (!createTaskRecord())
                {
                    MessageBox.Show("Could not create task!");
                }
            }
        }

        public String TaskName
        {
            get 
            {
                return strTaskName;
            }
            set 
            {
                strTaskName = value;
            }
        }

        public List<Object> GetComponents()
        {
            return arrTaskComponents;
        }

        public String Name()
        {
            return strTaskName;
        }

        private void createTaskTableIfNotExist()
        {
            db = new SQLiteDatabase();
            String strQuery = String.Format("CREATE TABLE IF NOT EXISTS {0}(id INTEGER PRIMARY KEY ASC, name TEXT, description TEXT, components, created TEXT, modified TEXT)",taskTable);
            try
            {
                db.ExecuteNonQuery(strQuery);
            }
            catch (Exception fail)
            {
                MessageBox.Show(String.Format("Could not perform \"{0}\" table creation/checker.\nThe following error occured:\n\n{1}\n\n", taskTable, fail.Message.ToString()));
            }
        }

        private Boolean doesTaskExist()
        {
            Boolean bExists = false;
            DataTable dtTasks;
            try
            {
                db = new SQLiteDatabase();
                String strQuery = String.Format("SELECT * FROM \"{0}\" WHERE \"name\" = \"{1}\"", taskTable, strTaskName);
                dtTasks = db.GetDataTable(strQuery);
                bExists = (dtTasks.Rows.Count > 0);
            }
            catch (Exception fail)
            {
                MessageBox.Show(String.Format("Could not perform lookup.\nThe following error occured:\n\n{0}\n\n", fail.Message.ToString()));
            }
            return bExists;
        }

        private Boolean createTaskRecord()
        {
            // Setup Vars
            Boolean bCreated = false;
            db = new SQLiteDatabase();
            DateTime now = DateTime.Now;
            Dictionary<String, String> data = new Dictionary<String, String>();

            // Need to serialize Array/Dictionary data to save to SQLite DB
            BinaryFormatter bf = new BinaryFormatter();
            System.IO.MemoryStream mem = new System.IO.MemoryStream();
            bf.Serialize(mem, arrTaskComponents);

            // Created Dictionary contents for insertion
            data.Add("name", strTaskName);
            data.Add("description", strTaskDescription);
            data.Add("components", Convert.ToBase64String(mem.ToArray()));
            data.Add("created", String.Format("{0:yyyy'-'MM'-'dd hh:mm:ss.fff}", now));
            data.Add("modified", String.Format("{0:yyyy'-'MM'-'dd hh:mm:ss.fff}", now));
            
            // Perform insertion
            try
            {
                bCreated = db.Insert(taskTable, data);
            }
            catch (Exception fail)
            {
                MessageBox.Show(String.Format("Could not insert record.\nThe following error occured:\n\n{0}\n\n", fail.Message.ToString()));
            }
            return bCreated;
        }
    }
}
