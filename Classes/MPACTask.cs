﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMUTasks.Forms.MPAC;

namespace GMUTasks.Classes
{
    public class MPACTask : CTask
    {
        private MPACMainWindow mainWindow;
        public  CUser         m_oStudent;
        public  CUser         m_oTeacher;
        public  int[,]        m_aResponses = new int[3,2];

        public MPACTask()
            : base(CTaskType.CTaskTypeMPAC)
        {
            Console.WriteLine("MPACTask::MPACTask()");
        }
        
        // Challenging Skills Task
        public void LoadWindow()
        {
            mainWindow = new MPACMainWindow();
            mainWindow.Parent = this;
            mainWindow.StartTask();
            // Simple Message relay
            //mainWindow.textRelay = new RelayText(this.DisplayRelayedText);
        }

        public void CloseWindow()
        {
            mainWindow.Close();
        }   
    }
}
