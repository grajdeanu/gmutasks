﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMUTasks.Forms.AKT;
   


namespace GMUTasks.Classes
{
    public class AKTTask : CTask
    {
        private AKTMainWindow mainWindow;
        public  CUser         m_oStudent;
        public  CUser         m_oTeacher;
        public  int[,]        m_aResponses = new int[3,2];

        public Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTVersions> mapStereotypicalQuestionsToVersion =
            new Dictionary<Constants.AKTNonStereotypicalQuestions, Constants.AKTVersions>()
            {
                {Constants.AKTNonStereotypicalQuestions.GoSchool, Constants.AKTVersions.VersionA},
                {Constants.AKTNonStereotypicalQuestions.ClassTrip, Constants.AKTVersions.VersionB},
                {Constants.AKTNonStereotypicalQuestions.Pizza, Constants.AKTVersions.VersionA},
                {Constants.AKTNonStereotypicalQuestions.Swing, Constants.AKTVersions.VersionB},
                {Constants.AKTNonStereotypicalQuestions.Clown, Constants.AKTVersions.VersionA},
                {Constants.AKTNonStereotypicalQuestions.JungleGym, Constants.AKTVersions.VersionB},
                {Constants.AKTNonStereotypicalQuestions.Block, Constants.AKTVersions.VersionA},
                {Constants.AKTNonStereotypicalQuestions.StayInside, Constants.AKTVersions.VersionB},
                {Constants.AKTNonStereotypicalQuestions.Hit, Constants.AKTVersions.VersionA},
                {Constants.AKTNonStereotypicalQuestions.Disappointed, Constants.AKTVersions.VersionB},
                {Constants.AKTNonStereotypicalQuestions.CleanToy, Constants.AKTVersions.VersionA},
                {Constants.AKTNonStereotypicalQuestions.Hamster, Constants.AKTVersions.VersionB},
            };

        public AKTTask()
            : base(CTaskType.CTaskTypeAKT)
        {
            Console.WriteLine("AKTTask::AKTTask()");
        }
        
        // Challenging Skills Task
        public void LoadWindow()
        {
            mainWindow = new AKTMainWindow();
            mainWindow.Parent = this;
            mainWindow.StartTask();
            // Simple Message relay
            //mainWindow.textRelay = new RelayText(this.DisplayRelayedText);
        }

        public void CloseWindow()
        {
            mainWindow.Close();
        }   
    }
}
