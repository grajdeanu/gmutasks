﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Text;
using GMUTasks.Classes;

namespace GMUTasks
{
    [Serializable()]
    public class CTask
    {
        public enum CTaskType
        {
            CTaskTypeCST = 0,
            CTaskTypeMPAC,
            //CTaskTypePSRA,
            CTaskTypeAKT,
            CTaskTypeUnknown
        }

        private CTaskType eTaskType = CTaskType.CTaskTypeUnknown;
        public void StartTimeMeasuring()
        {
            m_dttmStart = DateTime.Now;
        }
        /// <summary>
        /// Returns the time measurements in seconds
        /// </summary>
        /// <returns></returns>
        public double EndTimeMeasuring()
        {
            m_dttmEnd = DateTime.Now;
            return GetTimeMeasureInSeconds();
        }

        public double GetTimeMeasureInSeconds()
        {
            TimeSpan ts = m_dttmEnd - m_dttmStart;
            return ts.TotalSeconds;
        }
        private DateTime m_dttmStart;
        private DateTime m_dttmEnd;


        public CTask()
        {
            Console.WriteLine("CTask:CTask()");
            eTaskType = CTaskType.CTaskTypeUnknown;
        }

        public CTask(CTaskType type)
        {
            Console.WriteLine("CTask:CTask({0})", type);
            eTaskType = type;
        }

        public string ArrayListToString(System.Collections.ArrayList ar)
        {
            return ArrayListToString(ar, ',');
        }
        public string ArrayListToString(System.Collections.ArrayList ar, char delim)
        {
            return ArrayListToString(ar, delim.ToString());
        }
        public string ArrayListToString(System.Collections.ArrayList ar, string delim)
        {
            return string.Join(delim, (string[])ar.ToArray(typeof(string)));
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(
        EXECUTION_STATE flags);

        [Flags]
        public enum EXECUTION_STATE : uint
        {
            ES_SYSTEM_REQUIRED = 0x00000001,
            ES_DISPLAY_REQUIRED = 0x00000002,
            // ES_USER_PRESENT = 0x00000004,
            ES_CONTINUOUS = 0x80000000
        }

        public void PreventMonitorPowerdown()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED | EXECUTION_STATE.ES_CONTINUOUS);
        }

        public void AllowMonitorPowerdown()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

    }
    
    public class Constants
    {
        public const int DeviceResolutionWidth   = 1280;
        public const int DeviceResolutionHeight  = 800;
        public const int kFirstScenario          = 0;
        public const int kLastScenario           = 5;
        public const int MPACDurationInMinutes   = 5;
        public const int MPACAllowedAttempts     = 4;
        public const int AKTVideoTimerDuration   = 100;
        public const double CSTDefaultPause      = 3.0;
        public const double CSTSelectionDuration = 2.0;
        public const double CSTHesitationPause   = 5.0;
        public const String AKTVideoPath = "Resources\\Videos\\";
        public const String AKTFramePath = "Resources\\Frames\\";

        public const String Enabled  = "enabled";
        public const String Disabled = "disabled";

        public enum CSTVersions
        {
            VersionA = 0,
            VersionB
        }

        public enum CSTStages
        {
            Catalyst = 0,
            Faces,
            Choices,

            Training
        }

        public enum CSTScenarioNamesVersionA
        {
            Blocks = 0,
            Sandbox,
            SoccerBall,
            WontPlay,
            Picture,
            Doll
        }

        public enum CSTScenarioNamesVersionB
        {
            Airplane,
            Swing,
            Car,
            Birthday,
            KickBall,
            SuckThumb
        }
        public enum CSTScenarioBallResponses
        {
            AskToPlayWithSoccerWithYou = 0,
            GrabTheBallBackOrYell,
            Cry,
            GoPlaySomethingElse
        }

        public enum CSTScenarioSandboxResponses
        {
            TellHimThatsNotANiceThingToDo = 0,
            HitHim,
            Cry,
            GoPlaySomewhereElse
        }

        public enum CSTScenarioBlocksResponses
        {
            BuildAnotherTower = 0,
            HitBobbyOrYellAtHim,
            Cry,
            GoFindSomethingElseToPlayWith
        }

        public enum CSTScenarioWontPlayResponses
        {
            AskBobbyToPlayWithTomToo = 0,
            PushBobbyAndTellHimYoureNotHisFriend, 
            Cry,
            StopDrawingAndDoSomethingElse
        }
        public enum CSTScenarioPictureResponses
        {
            SayThatsOKYouLikeYourPicture = 0,
            HitBobbyAndYellAtHim,
            Cry,
            DoSomethingElse
        }
        public enum CSTScenarioDollResponses
        {
            BringAToyForBobbyToNapWith = 0,
            CallBobbyABabyOrHitHim,
            Cry,
            IgnoreBobby
        }
        public enum CSTScenarioAirplaneResponses
        {
            AskToDrawAnotherPicture = 0,
            HitOrYellAtHim,
            Cry,
            DoSomethingElse
        }
        public enum CSTScenarioSwingResponses
        {
            TellBobbyThisIsYourSpot = 0,
            PushBobbyBack,
            Cry,
            IgnoreBobby
        }
        public enum CSTScenarioCarResponses
        {
            AskBobbyIfHeWantsToPlayTogether = 0,
            GrabTheCarBackOrYell,
            Cry,
            IgnoreBobby
        }
        public enum CSTScenarioBirthdayResponses
        {
            TellBobbyHeCanComeToYourBirthday = 0,
            PushBobbyAndSayFineThen,
            Cry,
            DoSomethingElse
        }
        public enum CSTScenarioKickBallResponses
        {
            TryAgain = 0, 
            ThrowBallAtBobbysFace,
            Cry,
            PlaySomethingElse
        }
        public enum CSTScenarioSuckThumbResponses
        {
            SayItsOKToSuckYourThumbDuringNap = 0,
            TellBobbyHesABabyOrHitHim,
            Cry,
            IgnoreBobby
        }

        public enum CSTExpressionFaces
        {
            OK = 0,
            Angry,
            Sad,
            Happy
        }

        public enum CSTScenarioResponseChoices
        {
            ChoiceA = 0,
            ChoiceB,
            ChoiceC,
            ChoiceD
        }

        public enum CSTPictureBoxTags
        {
            centerPictureBox = 0,
            topCenterPictureBox,
            cellPictureBoxA,
            cellPictureBoxB,
            cellPictureBoxC,
            cellPictureBoxD,
            bottomRightPictureBox,
            mainCharacterBox
        }

        public enum AKTVersions
        {
            VersionA = 0,
            VersionB
        }

        public enum AKTStages
        {
            ReceptiveQuestions = 0,
            TeachingPhase,
            StereotypicalScenarios,
            NonStereotypicalScenarios,
            Finish,

            Training
        }

        //public enum AKTQuestionnaireQuestions
        //{
        //    ComingToSchool = 0,
        //    GoingOnAClassTrip,
        //    DoTheyLikePizza,
        //    LeavingAFavoriteActivity,
        //    SeeingAClownWalkIntoTheClassroom,
        //    ClimbingOnTheJungleGym,
        //    OtherKidsWontLetYouPlay,
        //    HasToStayInside,
        //    DontTellThatSomeoneHitThem,
        //    BeingReprimanded,
        //    ReprimandedForBreakingClassRules,
        //    DeathOfTheClassPet
        //}

        //public enum AKTQuestionnaireAnswers
        //{
        //    Happy = 0,
        //    Sad,
        //    Afraid,
        //    Angry,
        //    Yes,
        //    No
        //}

        public enum AKTNonStereotypicalQuestions
        {
            GoSchool = 0,
            ClassTrip,
            Pizza,
            Swing,
            Clown,
            JungleGym,
            Block,
            StayInside,
            Hit,
            Disappointed,
            CleanToy,
            Hamster
        }

        public enum AKTStereotypicalVersionA
        {
            Push = 0,
            Tower,
            Dreaming
        }

        public enum AKTStereotypicalVersionB
        {
            BigWheel = 0,
            Alone,
            Cabbage
        }

        public string ChoiceA = "A";
        public string ChoiceB = "B";

        public enum AKTQuestionnaireChoices
        {
            ChoiceA = 0,
            ChoiceB
        }

        public enum AKTFaces
        {
            Happy = 0,
            Sad,
            Angry,
            Afraid
        }


    }
}
