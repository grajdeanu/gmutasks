﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GMUTasks
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.Out.WriteLine("----------------------------------\nGMUTasks::Main - ApplicationStart");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainLogin());
        }
    }
}
